package input.FCC.BlockCoil_v2ari194;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableHF_FCC_block_v2ari194 extends Cable {

    public CableHF_FCC_block_v2ari194() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        // ROXIE cable ECC110HF

        this.label = "cable_HF_FCC";

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];
        //Filament
        this.dFilament = 50e-6; // [m];
        //Strand
        this.dstrand = 1.1e-3; // [m]; previous 1.155e-3;
        double CuScRatio = 0.8;
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];
        //Cable
        this.wBare = 12.6e-3; // [m]; previous 13.05e-3
        this.hInBare = 2e-3; // [m]; previous 2.1e-3
        this.hOutBare = 2e-3; // [m]; previous 2.1e-3
        this.noOfStrands = 21;
        this.noOfStrandsPerLayer = 10;
        this.noOfLayers = 2;
        this.lTpStrand = 0.180; // [m]; strand transposition pitch. prev 0.127
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }


}