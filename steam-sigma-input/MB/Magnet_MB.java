package input.LHC.MB;

import input.LHC.MB.CableIL_MB;
import input.LHC.MB.CableOL_MB;
import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/01/2018.
 */

public class Magnet_MB extends UtilsUserInput {

//        public int[] quadrantsRequired = {1};
//        public BoundaryConditions xAxisBC = BoundaryConditions.Neumann;
//        public BoundaryConditions yAxisBC = BoundaryConditions.Neumann;

    private final Domain[] domains;

    // VARIABLES

    // include-file of the variables for modified MB dipole
    // Christine Voellinger,Martin Aleksa, Bernhard Auchmann - 31.08.2000
    // Hole added by Martin Aleksa                           - 15.11.2000
    // Correction of the Nose and Nick by Nikolai Schwerg    - 25.10.2004
    // Checked by Bernhard Auchmann and Per Hagen            - 31.03.2011


    // referring to:
    // yoke			    LHCMB__A0136|0|C
    // inner yoke		LHCMB__A0139|0|B
    // outer yoke		LHCMB__A0140|0|B
    // insert		    LHCMB__A0148|1|E
    // collar		    LHCMB__A0099|0|C
    // coil sheet		LHCMB__A0021|0|B (design study by Christine Voellinger, not production)

    // GENERAL VARIABLES

    // Design variables added by Per HAGEN (for easier to deal with warm measurements)
    double mm = 0.001;
    double deg = Math.PI / 180;
    //shrink  = 0.998;			// yoke/insert shrink
    double DCONT = 0.0;
    double shrink = 1.0 * (1 - DCONT);
    //cshrink = 0.9973;			// collar shrink
    double DCCONT = 0.0;
    double cshrink = 1.0 * (1 - DCCONT);
    //csshrink = 0.996;			// coil sheet shrink
    double DCSCONT = 0.0;
    double csshrink = 1.0 * (1 - DCSCONT);
    double BEAMD = 97.00;
    double beamd = BEAMD * mm;

    // YOKE SPECIFIC VARIABLES

    // outer shape

    double RYOKE = 275.00;
    double ryoke = RYOKE * mm * shrink;

    double ALEL1 = 83.1;
    double alel1 = ALEL1 * mm * shrink;
    double BLEL1 = 100.00;
    double blel1 = BLEL1 * mm * shrink;
    double ALEL2 = 86.85;
    double alel2 = ALEL2 * mm * shrink;
    double D1 = 17.8;
    double d1 = D1 * mm * shrink;
    double D2 = 14.5;
    double d2 = D2 * mm * shrink;

    // houd

    double HOUDW = 27.00;
    double houdw = HOUDW * mm * shrink;
    double HOUDH = 6.00;
    double houdh = HOUDH * mm * shrink;
    double INSWI = 65.00;
    double inswi = INSWI * mm * shrink;

    double houdsh = beamd - houdw - inswi;


    // nose
    // defining the given values from drawing:

    // ___o<//////HX1//////>       o : keypoint
    //    /                :
    //   /_____o<////HX2//->
    //         /           : ^
    //        /            : |
    //       /             : HY
    //      /______________o v
    //                       ^
    // 		      | RNOSE
    //
    // the variable PHINOSEU has been dumped!

    double RNOSE = 256;
    double rnose = RNOSE * mm * shrink;
    double PHINOS = 77;
    double phinos = PHINOS * deg;
    double HX1 = 25.00;
    double hx1 = HX1 * mm * shrink;
    double HX2 = 19.04;
    double hx2 = HX2 * mm * shrink;
    double HY = 270.89 - RNOSE;
    double hy = HY * mm * shrink;


    // nick
    // buildDefinitions from drawing:
    // Given are the Coordinates of the boar in the middle
    // of the nick, the radius of the boar and the angle.

    //           /
    //         /
    //    /\   |
    //  /   \  |
    // /      0<//- XNICK
    //        ^
    //        | YNICK

    double XNICK = 202.95;
    double xnick = XNICK * mm * shrink;
    double YNICK = 342.00 / 2;
    double ynick = YNICK * mm * shrink;
    double PHINICK = 60;
    double phinick = PHINICK * deg;
    double RNICK = 3;
    double rnick = RNICK * mm * shrink;

    // the point in the corner of the nick is given by:
    //   [xnick-rnick, ynick - c]
    // with c the hypothenuse of the triangle R,c, phinick/2:
    //   c = rnick * Math.tan(beta)
    // beta the opposite angle
    //   beta = 90 - phinick/2
    Point kpTEST = Point.ofCartesian(xnick - rnick, ynick - rnick * Math.tan((Math.PI - phinick) / 2));
    // The x-coorinate of this point gives with the circle
    // equation the coordinates of kpnick_2
    //   kpnick_2 = [xnick-rnick, Math.sqrt(ryoke**2 - (xnick-rnick)**2)];

    // The coordinates of kpnick_1 are given by a line and the circle
    // by solving the equations:
    //   y = m * x + n
    //   y = Math.sqrt(R**2 - x**2)
    // With:
    //   R = ryoke
    //   m = 1/Math.tan(phinick)
    double nick_m = 1 / Math.tan(phinick);
    //   n = y0 - m * x0
    double nick_n = ynick - rnick * Math.tan((Math.PI - phinick) / 2) - nick_m * (xnick - rnick);
    //   with:
    //     x0 = xnick-rnick
    //     y0 = ynick - c
    // Equation:
    //   x**2 + 2*m*n/(1+m**2) * x + (n**2 - R**2)/(1+m**2) == 0
    // Solution:
    //   x = - p/2 +- Math.sqrt((p/2)**2-q)
    //   with:
    //     p = 2*m*n/(1+m**2)
    double nick_p = 2 * nick_n * nick_m / (1 + nick_m * nick_m);
    //     q = (n**2 - R**2)/(1+m**2)
    double nick_q = (nick_n * nick_n - ryoke * ryoke) / (1 + nick_m * nick_m);
    //   The Solution with positive sign is used:
    double nick_x = -nick_p / 2 + Math.sqrt((nick_p / 2) * (nick_p / 2) - nick_q);

    // busbar

    double BBR = 244.00;
    double bbr = BBR * mm;
    double BBW = 51.00;
    double bbw = BBW * mm;
    double PHIM = 53.6;
    double phim = PHIM * deg;

    double bush = Math.sqrt(ryoke * ryoke - bbw * bbw / 4) - bbr;
    double gammah = Math.atan(bbw / (2 * ryoke));

    // holesYoke
    // middlepoints of holesYoke
    double LH1X = 17.00;
    double lh1x = LH1X * mm;
    double LH1Y = 90.00;
    double lh1y = LH1Y * mm;
    double LH2X = 57.00;
    double lh2x = LH2X * mm;
    double LH2Y = 220.00;
    double lh2y = LH2Y * mm;
    double LH3X = 90.32;
    double lh3x = LH3X * mm;
    double LH3Y = 176.00;
    double lh3y = LH3Y * mm;
    double LH4X = 180.2;
    double lh4x = LH4X * mm;
    double LH4Y = 120.2;
    double lh4y = LH4Y * mm;
    double LHHX = 0.00;
    double lhhx = LHHX * mm;
    double LHHY = 179.6;
    double lhhy = LHHY * mm;

    // radius of holesYoke
    double LH1R = 2.5;
    double lh1r = LH1R * mm * shrink;
    double LH2R = 15.00;
    double lh2r = LH2R * mm * shrink;
    double LH3R = 8.8;
    double lh3r = LH3R * mm * shrink;
    double LH4R = 8.8;
    double lh4r = LH4R * mm * shrink;
    double LHHR = 30.00;
    double lhhr = LHHR * mm * shrink;

    // other parameters of holesYoke
    double PHIHCO = 30.00;
    double phihco = PHIHCO * deg;
    double linsh1 = Math.sqrt(1 - ((houdw + houdsh - d1) / alel1) * ((houdw + houdsh - d1) / alel1)) * blel1;    // point on ellipse

    // MODIFIED INSERT SPECIFIC VARIABLES

    double LH5X = 32.1;
    double lh5x = LH5X * mm * shrink;
    // double LH5DY = 22.7;
    double LH5R = 8.1;
    double lh5r = LH5R * mm * shrink;

    double LINSPHI = 9.00;
    double linsphi = LINSPHI * deg;
    double LINSPARANG = 41.8;
    double linsparang = LINSPARANG * deg;
    double LINSNH = 8.2;
    double linsnh = LINSNH * mm * shrink;
    double LINSCUT = 56.;
    double linscut = LINSCUT * mm * shrink;

    double linsh = blel1 * Math.sin(linsparang) - linsnh;
    double linsw = beamd - d2 - alel2 * Math.cos(linsparang);

    double yinscut1 = Math.sqrt(1 - ((beamd - d2 - linscut) / alel2) * ((beamd - d2 - linscut) / alel2)) * blel1;    // point on ellipse
    double yinscut2 = (beamd - houdw - linscut) * Math.tan(linsphi) + blel1 + houdh;

    double linsh2 = Math.sqrt(1 - ((houdw + houdsh - d2) / alel2) * ((houdw + houdsh - d2) / alel2)) * blel1;

    // point on ellipse

    double du1a = (d2 - beamd + lh5x);
    double du1b = alel2 * alel2 * alel2 * alel2 * (blel1 * blel1 - 2 * lh5r * lh5r);
    double du1 = -blel1 * blel1 * blel1 * blel1 * du1a * du1a - du1b;
    double du2 = alel2 * alel2 * blel1 * blel1 * (du1a * du1a - lh5r * lh5r);
    double du3a = (alel2 * alel2 * alel2 * alel2 - alel2 * alel2 * du1a * du1a + blel1 * blel1 * du1a * du1a);
    double du3 = du3a * du3a;
    double du4 = (alel2 * alel2 * alel2 * alel2 + alel2 * alel2 * du1a * du1a - blel1 * blel1 * du1a * du1a);
    double du5 = (alel2 * alel2 * (alel2 - blel1) * (alel2 + blel1));

    // du1 = -(blel1**4*(-beamd+d2+lh5x)**2)-alel2**4*(blel1**2-2*lh5r**2);
    // du2 = alel2**2*blel1**2*((-beamd+d2+lh5x)**2-lh5r**2);
    // du3 = (alel2**4-alel2**2*(-beamd+d2+lh5x)**2+blel1**2*(-beamd+d2+lh5x)**2)**2;
    // du4 = (alel2**4+alel2**2*(-beamd+d2+lh5x)**2-blel1**2*(-beamd+d2+lh5x)**2);
    // du5 = (alel2**2*(alel2-blel1)*(alel2+blel1));

    double dxvar = Math.sqrt((du1 + du2 + Math.sqrt(blel1 * blel1 * blel1 * blel1 * (du3 - 2 * alel2 * alel2 * du4 * lh5r * lh5r + alel2 * alel2 * alel2 * alel2 * lh5r * lh5r * lh5r * lh5r))) / du5) / Math.sqrt(2);

    double lh5y = (blel1 * blel1 * dxvar * (beamd - d2 - lh5x)) / (alel2 * alel2 * Math.sqrt(-dxvar * dxvar + lh5r * lh5r));

    double dyvar = Math.sqrt(lh5r * lh5r - dxvar * dxvar);


    // COLLAR SPECIFIC VARIABLES

    // outer shape

    double CUSH1 = 92.00;
    double cush1 = CUSH1 * mm * cshrink;
    double CUSH2 = 47.5;
    double cush2 = CUSH2 * mm * cshrink;
    double CN1X = 179.95;
    double cn1x = CN1X * mm * cshrink;

    // upper structure (us)

    double USHOUT = 92.00;
    double ushout = USHOUT * mm * cshrink;
    double USHIN = 93.1;
    double ushin = USHIN * mm * cshrink;
    double USHTOP = 98.9;
    double ushtop = USHTOP * mm * cshrink;
    double USPHI = 30.00;
    double usphi = USPHI * deg;
    double USDOUT = 52.00;
    double usdout = USDOUT * mm * cshrink;
    double USDIN = 21.00;
    double usdin = USDIN * mm * cshrink;


    // lower structure (ls)

    double LSR1 = 15.00;
    double lsr1 = LSR1 * mm * cshrink;
    double LSR2 = 44.88;
    double lsr2 = LSR2 * mm * cshrink;
    double LSR3 = 60.98;
    double lsr3 = LSR3 * mm * cshrink;
    double LSPHI1 = 52.5;
    double lsphi1 = LSPHI1 * deg;
    double LSPHI2 = 19.01;
    double lsphi2 = LSPHI2 * deg;
    double LSPHI3 = 48.67;
    double lsphi3 = LSPHI3 * deg;
    double LSHHELP1 = 12.44;
    double lshhelp1 = LSHHELP1 * mm * cshrink;
    double LSHHELP2 = 13.2;
    double lshhelp2 = LSHHELP2 * mm * cshrink;
    double LSHHELP3 = 18.12;
    double lshhelp3 = LSHHELP3 * mm * cshrink;
    double LSHMID = 30.3;
    double lshmid = LSHMID * mm * cshrink;

    double delta = lshhelp2 - lshhelp1;
    double lsdI = delta * Math.cos(lsphi2) + Math.sqrt(delta * Math.cos(lsphi2) * delta * Math.cos(lsphi2) + lsr1 * lsr1 - delta * delta);    // Cosine-theorem

    double lsdIIa = lshhelp1 * Math.cos(Math.PI - lsphi2) + Math.sqrt(lshhelp1 * Math.cos(Math.PI - lsphi2) * lshhelp1 * Math.cos(Math.PI - lsphi2) + lsr2 * lsr2 - lshhelp1 * lshhelp1);    // Cosine-theorem

    double lsdIIb = lshhelp3 * Math.cos(Math.PI - lsphi3) + Math.sqrt(lshhelp3 * Math.cos(Math.PI - lsphi3) * lshhelp3 * Math.cos(Math.PI - lsphi3) + lsr2 * lsr2 - lshhelp3 * lshhelp3);

    double lsdIII = lshhelp3 * Math.cos(Math.PI - lsphi3) + Math.sqrt(lshhelp3 * Math.cos(Math.PI - lsphi3) * lshhelp3 * Math.cos(Math.PI - lsphi3) + lsr3 * lsr3 - lshhelp3 * lshhelp3);

    // arcs and holesYoke

    double XARC1 = 178.00;
    double xarc1 = XARC1 * mm * cshrink;
    double RARC1 = 7.01;
    double rarc1 = RARC1 * mm; // shrinked scale
    double RARC2 = 11.01;
    double rarc2 = RARC2 * mm; // shrinked scale

    double XHOLE1 = 156.00;
    double xhole1 = XHOLE1 * mm * cshrink;
    double XHOLE2 = 39.00;
    double xhole2 = XHOLE2 * mm * cshrink;
    double YHOLE = 57.00;
    double yhole = YHOLE * mm * cshrink;

    double DHOLE1 = 10.00;
    double dhole1 = DHOLE1 * mm * cshrink;
    double DHOLE2 = 10.02;
    double dhole2 = DHOLE2 * mm * cshrink;
    double rhole1 = dhole1 / 2;
    double rhole2 = dhole2 / 2;


//    // COIL SHEET (cs) SPECIFIC VARIABLES
//
//    // PARAMETER SETTING
//
//    double ILALPHA = 23.00;
//    double ilalpha = ILALPHA*deg;		// parameter for cut off of cs
//    double OLH = 13.00;
//    double olh = OLH*mm*csshrink;		// height parameter for cut off of cs
//    double IRONALPHA = 20.00;
//    double ironalpha = IRONALPHA*deg;	// cut off angle for inner iron inlay
//
//    // general
//
//    double POLY = 0.5;
//    double poly = POLY*mm*csshrink;	// polyamid layer thickness
//    double RCIN = 28.00;
//    double rcin = RCIN*mm*csshrink;	// inner radius of the coil
//    double RCMID = 43.4;
//    double rcmid = RCMID*mm*csshrink;	// mid radius
//    double RCOUT = 59.3;
//    double rcout = RCOUT*mm*csshrink;	// outer radius
//
//    // inner layer (IL)
//
//    double ILQH = 0.5;
//    double ilqh = ILQH*mm*csshrink;	// inner layer quensh heater thickness
//    double ILCS = 0.5;
//    double ilcs = ILCS*mm*csshrink;	// inner layer coil sheet thickness
//    double ILHELP = 5.69;
//    double ilhelp = ILHELP*mm*csshrink;	// construction help line (see drawings)
//    double ILSHIM = 0.2;
//    double ilshim = ILSHIM*mm*csshrink;	// shim inner layer
//    double ILPHI = 19.01;
//    double ilphi = ILPHI*deg;		// construction help angle (see drawings)
//
//    // outer layer (OL)
//
//    double OLQH = 0.2;
//    double olqh = OLQH*mm*csshrink;	// outer layer quensh heater
//    double OLSHIM = 0.8;
//    double olshim = OLSHIM*mm*csshrink;	// shim outer layer
//    double OLCSIN = 0.5;
//    double olcsin = OLCSIN*mm*csshrink;	// outer layer inner coil sheet
//    double OLCSOUT = 0.5;
//    double olcsout = OLCSOUT*mm*csshrink;	// outer layer outer coil sheet
//    double OLHELP = 14.79;
//    double olhelp = OLHELP*mm*csshrink;	// construction help line (see drawings)
//    double OLPHI = 48.67;
//    double olphi = OLPHI*deg;		// construction help angle (see drawings)
//
//    // inner layer calculations
//
//    // Cosine theorem
//    double ang = Math.PI-ilphi;
//    double d = ilhelp;
//    double r = rcin;
//
//    double ilrh1 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    // cosine theorem result + shifting in orthogonal direction
//    double il1x = ilrh1*Math.sin(ilphi) - (ilqh+poly)*Math.cos(ilphi);
//    double il1y = ilhelp + ilrh1*Math.cos(ilphi) + (ilqh+poly)*Math.sin(ilphi);
//
//    double il2x = ilrh1*Math.sin(ilphi) - (ilqh+poly+2*ilcs+ilshim)*Math.cos(ilphi);
//    double il2y = ilhelp + ilrh1*Math.cos(ilphi) + (ilqh+poly+2*ilcs+ilshim)*Math.sin(ilphi);
//
//    ang = Math.PI-ilphi;
//    d = ilhelp + (ilqh+poly)/Math.sin(ilphi);
//    r = rcmid+ilqh+poly;
//
//    double ilrh2 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double il3x = ilrh2*Math.sin(ilphi);
//    double il3y = d + ilrh2*Math.cos(ilphi);
//
//    double il7phi = Math.acos((d*d+r*r-ilrh2*ilrh2)/(2*r*d));
//
//    ang = Math.PI-ilphi;
//    d = ilhelp + (ilqh+poly+2*ilcs+ilshim)/Math.sin(ilphi);
//    r = rcmid+ilqh+poly+ilcs;
//
//    double ilrh3 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double il4x = ilrh3*Math.sin(ilphi);
//    double il4y = d + ilrh3*Math.cos(ilphi);
//
//    ang = Math.PI-(olphi-ilalpha);
//    d = olhelp;
//    r = rcmid+ilqh+poly;
//
//    double ilrh4 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double il5x = ilrh4*Math.sin(olphi-ilalpha);
//    double il5y = d + ilrh4*Math.cos(olphi-ilalpha);
//
//    double il6phi = Math.acos((d*d+r*r-ilrh4*ilrh4)/(2*r*d));
//
//
//    // outer layer calculations
//
//    ang = Math.PI-(olphi);
//    d = olhelp + (2*poly+olshim)/Math.sin(olphi);
//    r = rcmid+ilqh+poly+ilcs+olh;
//
//    double olrh1 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double ol1x = olrh1*Math.sin(olphi);
//    double ol1y = d + olrh1*Math.cos(olphi);
//
//    ang = Math.PI-(olphi);
//    d = olhelp + (2*poly+olshim)/Math.sin(olphi);
//    r = rcmid+ilqh+poly+ilcs+olh;
//
//    double olrh2 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double ol2x = olrh2*Math.sin(olphi) - olcsout*Math.cos(olphi);
//    double ol2y = d + olrh2*Math.cos(olphi) + olcsout*Math.sin(olphi);
//
//    ang = Math.PI-(olphi);
//    d = olhelp + (2*poly+olshim)/Math.sin(olphi);
//    r = rcout+olqh+poly;
//
//    olrh2 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double ol3x = olrh2*Math.sin(olphi);
//    double ol3y = d + olrh2*Math.cos(olphi);
//
//    double ol11phi = Math.acos((d*d+r*r-olrh2*olrh2)/(2*r*d));
//
//    ang = Math.PI-(olphi);
//    d = olhelp + (2*poly+olshim+olcsout)/Math.sin(olphi);
//    r = rcout+olqh+poly+olcsin+olcsout;
//
//    double olrh3 = d*Math.cos(ang) + Math.sqrt(d*Math.cos(ang)*d*Math.cos(ang) + r*r - d*d);
//
//    double ol4x = olrh3*Math.sin(olphi);
//    double ol4y = d + olrh3*Math.cos(olphi);

    public Magnet_MB() {

        domains = new Domain[]{

                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
//                    new BoundaryConditionsDomain("BC", null, this.xAxisBC, this.yAxisBC, BoundaryConditions()),

                new CoilDomain("CR", MatDatabase.MAT_COIL, coil().translate(97e-3, 0)),
//                    new CoilDomain("CL", MatDatabase.MAT_COIL, coil().translate(-97e-3, 0)),

//                new WedgeDomain("wedgesCR", MatDatabase.MAT_COPPER, wedgesCoil("CR", 97e-3, 0)),
//                    new WedgeDomain("wedgesCL", MatDatabase.MAT_COPPER, wedgesCoil("CL", -97e-3, 0)),

//                new InsulationDomain("foilCoil1", "poly1", fishBone(coil_ApR, coil_ApR_Name, 97e-3, 0)),
//                new InsulationDomain("foilCoil2", "poly2", fishBone(coil_ApL, coil_ApL_Name, -97e-3, 0)),

                new IronDomain("IY", MatDatabase.MAT_IRON1, iron_yoke()),
                new IronDomain("I", MatDatabase.MAT_IRON2, iron_insert()),
                new HoleDomain("HY", MatDatabase.MAT_AIR, holes_yoke()),
                new HoleDomain("HI", MatDatabase.MAT_AIR, holes_insert()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1};
        Element[] quad2 = {el2};
        Element[] quad3 = {el3};
        Element[] quad4 = {el4};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
        return new Element[]{el1, el4};
    }

    public Element[] airFarField() {
        // POINTS

        double r = 1;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
        Area ar2_far = ar1_far.mirrorY();
        Area ar3_far = ar2_far.mirrorX();
        Area ar4_far = ar1_far.mirrorX();

        Element el1_far = new Element("FAR_El1", ar1_far);
        Element el2_far = new Element("FAR_El2", ar2_far);
        Element el3_far = new Element("FAR_El3", ar3_far);
        Element el4_far = new Element("FAR_El4", ar4_far);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_far};
        Element[] quad2 = {el2_far};
        Element[] quad3 = {el3_far};
        Element[] quad4 = {el4_far};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
        return new Element[]{el1_far,el4_far};
    }

    public Coil coil() {
        Point kp11 = Point.ofCartesian(41.6499e-3, 14.5216e-3);
        Point kp12 = Point.ofCartesian(43.8126e-3, 0.1249e-3);
        Point kp13 = Point.ofCartesian(59.1822e-3, 0.1249e-3);
        Point kp14 = Point.ofCartesian(56.8678e-3, 16.6764e-3);

        Point kp21 = Point.ofCartesian(24.8213e-3, 36.5743e-3);
        Point kp22 = Point.ofCartesian(40.7667e-3, 16.3918e-3);
        Point kp23 = Point.ofCartesian(54.46512e-3, 23.3695e-3);
        Point kp24 = Point.ofCartesian(36.3631e-3, 46.7239e-3);

        Point kp31 = Point.ofCartesian(26.6013e-3, 9.8954e-3);
        Point kp32 = Point.ofCartesian(27.9443e-3, 0.1251e-3);
        Point kp33 = Point.ofCartesian(43.3144e-3, 0.1251e-3);
        Point kp34 = Point.ofCartesian(41.8808e-3, 11.5614e-3);

        Point kp41 = Point.ofCartesian(20.9482e-3, 18.9638e-3);
        Point kp42 = Point.ofCartesian(25.9695e-3, 10.5038e-3);
        Point kp43 = Point.ofCartesian(39.8504e-3, 17.1038e-3);
        Point kp44 = Point.ofCartesian(34.0320e-3, 27.0296e-3);
//
        Point kp51 = Point.ofCartesian(14.1078e-3, 24.4838e-3);
        Point kp52 = Point.ofCartesian(18.7038e-3, 20.7613e-3);
        Point kp53 = Point.ofCartesian(29.4194e-3, 31.7803e-3);
        Point kp54 = Point.ofCartesian(24.0830e-3, 36.1771e-3);
//
        Point kp61 = Point.ofCartesian(7.3662e-3, 27.0405e-3);
        Point kp62 = Point.ofCartesian(11.0639e-3, 25.6877e-3);
        Point kp63 = Point.ofCartesian(16.6971e-3, 39.9883e-3);
        Point kp64 = Point.ofCartesian(12.3730e-3, 41.5722e-3);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);
//
        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);
//
        Arc ln51 = Arc.ofEndPointsCenter(kp52, kp51, kp0);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Arc ln53 = Arc.ofEndPointsCenter(kp53, kp54, kp0);
        Line ln54 = Line.ofEndPoints(kp51, kp54);
//
        Arc ln61 = Arc.ofEndPointsCenter(kp62, kp61, kp0);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Arc ln63 = Arc.ofEndPointsCenter(kp63, kp64, kp0);
        Line ln64 = Line.ofEndPoints(kp61, kp64);
//
        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
//
        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();

        Winding w11_R = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1,-1}, 9, 9, new input.LHC.MB.CableOL_MB());
        Winding w12_R = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1,-1}, 16, 16, new CableOL_MB());
        Winding w13_R = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1,-1}, 5, 5, new CableIL_MB());
        Winding w14_R = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1,-1}, 5, 5, new CableIL_MB());
        Winding w15_R = Winding.ofAreas(new Area[]{ha15p, ha15n}, new int[]{+1,-1}, 3, 3, new CableIL_MB());
        Winding w16_R = Winding.ofAreas(new Area[]{ha16p, ha16n}, new int[]{+1,-1}, 2, 2, new CableIL_MB());

        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w11_R,w12_R,w13_R,w14_R,w15_R,w16_R});
        Pole p2 = p1.mirrorX().reverseWindingDirection();
        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1,p2});
        return c1;
    }


    public Element[] wedgesCoil(String coilName, double dx, double dy){

        //Wedge 1
        Point kp11_wedge = Point.ofCartesian(40.6498e-3, 16.3448e-3);
        Point kp14_wedge = Point.ofCartesian(54.46512e-3, 23.3695e-3);
        double r_in = Math.sqrt(Math.pow(kp11_wedge.getX(),2)+Math.pow(kp11_wedge.getY(),2));
        double r_out = Math.sqrt(Math.pow(kp14_wedge.getX(),2)+Math.pow(kp14_wedge.getY(),2));

        Point kp12_wedge_aux = Point.ofCartesian(41.6499e-3, 14.5216e-3);
//        Point kp13_wedge_aux = Point.ofCartesian(56.8678e-3, 16.6764e-3);

        double phi_kp12 = kp12_wedge_aux.getPhi();
//        double phi_kp13 = kp13_wedge_aux.getPhi();

        Point kp12_wedge = Point.ofCartesian(r_in*Math.cos(phi_kp12), r_in*Math.sin(phi_kp12));
//        Point kp13_wedge = Point.ofCartesian(r_out*Math.cos(phi_kp13), r_out*Math.sin(phi_kp13));
        Point kp13_wedge = Point.ofCartesian(56.7908e-3,16.6538e-3);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11_wedge = Arc.ofEndPointsCenter(kp12_wedge, kp11_wedge, kp0);
        Line ln12_wedge = Line.ofEndPoints(kp12_wedge, kp13_wedge);
        Arc ln13_wedge = Arc.ofEndPointsCenter(kp13_wedge, kp14_wedge, kp0);
        Line ln14_wedge = Line.ofEndPoints(kp11_wedge, kp14_wedge);

        //Wedge 2
        Point kp21_wedge = Point.ofCartesian(25.9057e-3, 10.4780e-3);
        Point kp24_wedge = Point.ofCartesian(39.8033e-3, 17.0835e-3);
        double r_in2 = Math.sqrt(Math.pow(kp21_wedge.getX(),2)+Math.pow(kp21_wedge.getY(),2));
        double r_out2 = Math.sqrt(Math.pow(kp24_wedge.getX(),2)+Math.pow(kp24_wedge.getY(),2));

        Point kp22_wedge_aux = Point.ofCartesian(26.6013e-3, 9.8954e-3);
        Point kp23_wedge_aux = Point.ofCartesian(41.8808e-3, 11.5614e-3);

        double phi_kp22 = kp22_wedge_aux.getPhi();
        double phi_kp23 = kp23_wedge_aux.getPhi();

        Point kp22_wedge = Point.ofCartesian(r_in2*Math.cos(phi_kp22), r_in2*Math.sin(phi_kp22));
        Point kp23_wedge = Point.ofCartesian(r_out2*Math.cos(phi_kp23), r_out2*Math.sin(phi_kp23));

        Arc ln21_wedge = Arc.ofEndPointsCenter(kp22_wedge, kp21_wedge, kp0);
        Line ln22_wedge = Line.ofEndPoints(kp22_wedge, kp23_wedge);
        Arc ln23_wedge = Arc.ofEndPointsCenter(kp23_wedge, kp24_wedge, kp0);
        Line ln24_wedge = Line.ofEndPoints(kp21_wedge, kp24_wedge);

        //Wedge 3
        Point kp31_wedge = Point.ofCartesian(18.7038e-3, 20.7613e-3);
        Point kp34_wedge = Point.ofCartesian(29.4194e-3, 31.7803e-3);
        double r_in3 = Math.sqrt(Math.pow(kp31_wedge.getX(),2)+Math.pow(kp31_wedge.getY(),2));
        double r_out3 = Math.sqrt(Math.pow(kp34_wedge.getX(),2)+Math.pow(kp34_wedge.getY(),2));

        Point kp32_wedge_aux = Point.ofCartesian(20.9482e-3, 18.9638e-3);
        Point kp33_wedge_aux = Point.ofCartesian(34.0320e-3, 27.0296e-3);

        double phi_kp32 = kp32_wedge_aux.getPhi();
        double phi_kp33 = kp33_wedge_aux.getPhi();

        Point kp32_wedge = Point.ofCartesian(r_in3*Math.cos(phi_kp32), r_in3*Math.sin(phi_kp32));
        Point kp33_wedge = Point.ofCartesian(r_out3*Math.cos(phi_kp33), r_out3*Math.sin(phi_kp33));

        Arc ln31_wedge = Arc.ofEndPointsCenter(kp32_wedge, kp31_wedge, kp0);
        Line ln32_wedge = Line.ofEndPoints(kp32_wedge, kp33_wedge);
        Arc ln33_wedge = Arc.ofEndPointsCenter(kp33_wedge, kp34_wedge, kp0);
        Line ln34_wedge = Line.ofEndPoints(kp31_wedge, kp34_wedge);



        //Wedge 4
        Point kp41_wedge = Point.ofCartesian(11.0639e-3, 25.6877e-3);
        Point kp44_wedge = Point.ofCartesian(16.6971e-3, 39.9883e-3);
        double r_in4 = Math.sqrt(Math.pow(kp41_wedge.getX(),2)+Math.pow(kp41_wedge.getY(),2));
        double r_out4 = Math.sqrt(Math.pow(kp44_wedge.getX(),2)+Math.pow(kp44_wedge.getY(),2));

        Point kp42_wedge_aux = Point.ofCartesian(14.1078e-3, 24.4838e-3);
//        Point kp43_wedge_aux = Point.ofCartesian(24.0830e-3, 36.1771e-3);

        double phi_kp42 = kp42_wedge_aux.getPhi();
//        double phi_kp43 = kp43_wedge_aux.getPhi();

        Point kp42_wedge = Point.ofCartesian(r_in4*Math.cos(phi_kp42), r_in4*Math.sin(phi_kp42));
//        Point kp43_wedge = Point.ofCartesian(r_out4*Math.cos(phi_kp43), r_out4*Math.sin(phi_kp43));
        Point kp43_wedge = Point.ofCartesian(23.9982e-3,36.0496e-3);

        Arc ln41_wedge = Arc.ofEndPointsCenter(kp42_wedge, kp41_wedge, kp0);
        Line ln42_wedge = Line.ofEndPoints(kp42_wedge, kp43_wedge);
        Arc ln43_wedge = Arc.ofEndPointsCenter(kp43_wedge, kp44_wedge, kp0);
        Line ln44_wedge = Line.ofEndPoints(kp41_wedge, kp44_wedge);



        Area ar1_wedgeCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln11_wedge, ln12_wedge, ln13_wedge, ln14_wedge});
        Area ar1_wedgeCoil1Q2 = ar1_wedgeCoil1Q1.mirrorY();
        Area ar1_wedgeCoil1Q3 = ar1_wedgeCoil1Q2.mirrorX();
        Area ar1_wedgeCoil1Q4 = ar1_wedgeCoil1Q1.mirrorX();
//        ar1_wedgeCoil1Q1.translate(dx,dy);
//        ar1_wedgeCoil1Q2.translate(dx,dy);
//        ar1_wedgeCoil1Q3.translate(dx,dy);
//        ar1_wedgeCoil1Q4.translate(dx,dy);
        Element el1_wedgeCoil1Q1 = new Element(coilName+"Q1_El1_wedge", ar1_wedgeCoil1Q1.translate(dx,dy));
        Element el1_wedgeCoil1Q2 = new Element(coilName+"Q2_El1_wedge", ar1_wedgeCoil1Q2.translate(dx,dy));
        Element el1_wedgeCoil1Q3 = new Element(coilName+"Q3_El1_wedge", ar1_wedgeCoil1Q3.translate(dx,dy));
        Element el1_wedgeCoil1Q4 = new Element(coilName+"Q4_El1_wedge", ar1_wedgeCoil1Q4.translate(dx,dy));

        Area ar2_wedgeCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln21_wedge, ln22_wedge, ln23_wedge, ln24_wedge});
        Area ar2_wedgeCoil1Q2 = ar2_wedgeCoil1Q1.mirrorY();
        Area ar2_wedgeCoil1Q3 = ar2_wedgeCoil1Q2.mirrorX();
        Area ar2_wedgeCoil1Q4 = ar2_wedgeCoil1Q1.mirrorX();
        ar2_wedgeCoil1Q1.translate(dx,dy);
        ar2_wedgeCoil1Q2.translate(dx,dy);
        ar2_wedgeCoil1Q3.translate(dx,dy);
        ar2_wedgeCoil1Q4.translate(dx,dy);
        Element el2_wedgeCoil1Q1 = new Element(coilName+"Q1_El2_wedge", ar2_wedgeCoil1Q1.translate(dx,dy));
        Element el2_wedgeCoil1Q2 = new Element(coilName+"Q2_El2_wedge", ar2_wedgeCoil1Q2.translate(dx,dy));
        Element el2_wedgeCoil1Q3 = new Element(coilName+"Q3_El2_wedge", ar2_wedgeCoil1Q3.translate(dx,dy));
        Element el2_wedgeCoil1Q4 = new Element(coilName+"Q4_El2_wedge", ar2_wedgeCoil1Q4.translate(dx,dy));

        Area ar3_wedgeCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln31_wedge, ln32_wedge, ln33_wedge, ln34_wedge});
        Area ar3_wedgeCoil1Q2 = ar3_wedgeCoil1Q1.mirrorY();
        Area ar3_wedgeCoil1Q3 = ar3_wedgeCoil1Q2.mirrorX();
        Area ar3_wedgeCoil1Q4 = ar3_wedgeCoil1Q1.mirrorX();
        ar3_wedgeCoil1Q1.translate(dx,dy);
        ar3_wedgeCoil1Q2.translate(dx,dy);
        ar3_wedgeCoil1Q3.translate(dx,dy);
        ar3_wedgeCoil1Q4.translate(dx,dy);
        Element el3_wedgeCoil1Q1 = new Element(coilName+"Q1_El3_wedge", ar3_wedgeCoil1Q1.translate(dx,dy));
        Element el3_wedgeCoil1Q2 = new Element(coilName+"Q2_El3_wedge", ar3_wedgeCoil1Q2.translate(dx,dy));
        Element el3_wedgeCoil1Q3 = new Element(coilName+"Q3_El3_wedge", ar3_wedgeCoil1Q3.translate(dx,dy));
        Element el3_wedgeCoil1Q4 = new Element(coilName+"Q4_El3_wedge", ar3_wedgeCoil1Q4.translate(dx,dy));

        Area ar4_wedgeCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln41_wedge, ln42_wedge, ln43_wedge, ln44_wedge});
        Area ar4_wedgeCoil1Q2 = ar4_wedgeCoil1Q1.mirrorY();
        Area ar4_wedgeCoil1Q3 = ar4_wedgeCoil1Q2.mirrorX();
        Area ar4_wedgeCoil1Q4 = ar4_wedgeCoil1Q1.mirrorX();
        ar4_wedgeCoil1Q1.translate(dx,dy);
        ar4_wedgeCoil1Q2.translate(dx,dy);
        ar4_wedgeCoil1Q3.translate(dx,dy);
        ar4_wedgeCoil1Q4.translate(dx,dy);
        Element el4_wedgeCoil1Q1 = new Element(coilName+"Q1_El4_wedge", ar4_wedgeCoil1Q1.translate(dx,dy));
        Element el4_wedgeCoil1Q2 = new Element(coilName+"Q2_El4_wedge", ar4_wedgeCoil1Q2.translate(dx,dy));
        Element el4_wedgeCoil1Q3 = new Element(coilName+"Q3_El4_wedge", ar4_wedgeCoil1Q3.translate(dx,dy));
        Element el4_wedgeCoil1Q4 = new Element(coilName+"Q4_El4_wedge", ar4_wedgeCoil1Q4.translate(dx,dy));


//        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1_wedgeCoil1Q1,el2_wedgeCoil1Q1,el3_wedgeCoil1Q1,el4_wedgeCoil1Q1};
//        Element[] quad2 = {el1_wedgeCoil1Q2,el2_wedgeCoil1Q2,el3_wedgeCoil1Q2,el4_wedgeCoil1Q2};
//        Element[] quad3 = null;
//        Element[] quad4 = null;
//
//        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
//        return elementsToBuild;


        return new Element[] { el1_wedgeCoil1Q1,el1_wedgeCoil1Q2, el1_wedgeCoil1Q3,el1_wedgeCoil1Q4,
                el2_wedgeCoil1Q1,el2_wedgeCoil1Q2, el2_wedgeCoil1Q3,el2_wedgeCoil1Q4,
                el3_wedgeCoil1Q1,el3_wedgeCoil1Q2, el3_wedgeCoil1Q3,el3_wedgeCoil1Q4,
                el4_wedgeCoil1Q1,el4_wedgeCoil1Q2, el4_wedgeCoil1Q3,el4_wedgeCoil1Q4,
        };

//        return new Element[] {
//                el1_wedgeCoil1Q1,el1_wedgeCoil1Q2,el1_wedgeCoil1Q3,el1_wedgeCoil1Q4,
//                el2_wedgeCoil1Q1,el2_wedgeCoil1Q2,el2_wedgeCoil1Q3,el2_wedgeCoil1Q4,
//                el3_wedgeCoil1Q1,el3_wedgeCoil1Q2,el3_wedgeCoil1Q3,el3_wedgeCoil1Q4,
//                el4_wedgeCoil1Q1,el4_wedgeCoil1Q2,el4_wedgeCoil1Q3,el4_wedgeCoil1Q4
//        };

//        return new Element[] {
//                el1_wedgeCoil1Q1,
//                el2_wedgeCoil1Q1,
//                el3_wedgeCoil1Q1,
//                el4_wedgeCoil1Q1,
//        };
    }

//        public Element[] fishBone(Coil coil, String coilName, double dx, double dy){
//
//
//            HyperLine blockEdge = coil.getWindings()[0][0].getBlocks()[0].getHyperLines()[0];
//            int noOfPoints = 2;
//            Point[] coord = blockEdge.extractPoints(noOfPoints);
//            Point kp13_foil = coord[0];
//
//            blockEdge = coil.getWindings()[0][1].getBlocks()[0].getHyperLines()[0];
//            coord = blockEdge.extractPoints(noOfPoints);
//            Point kp14_foil = coord[1];
//
//            blockEdge = coil.getWindings()[0][3].getBlocks()[0].getHyperLines()[2];
//            coord = blockEdge.extractPoints(noOfPoints);
//            Point kp11_foil = coord[0];
//
//            blockEdge = coil.getWindings()[0][5].getBlocks()[0].getHyperLines()[2];
//            coord = blockEdge.extractPoints(noOfPoints);
//            Point kp12_foil = coord[1];
//
//            Point kp0 = Point.ofCartesian(dx,dy);
////        Arc ln11_foil = Arc.ofEndPointsCenter(kp12_foil, kp11_foil, kp0);
//            Line ln12_foil = Line.ofEndPoints(kp12_foil, kp13_foil);
////        Arc ln13_foil = Arc.ofEndPointsCenter(kp13_foil, kp14_foil, kp0);
//            Line ln14_foil = Line.ofEndPoints(kp11_foil, kp14_foil);
//
//
//
////        Area ar1_foilIntCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln11_foil, ln12_foil, ln13_foil, ln14_foil});
//            Area ar1_foilIntCoil1Q1 = Area.ofHyperLines(new HyperLine[] {ln12_foil, ln14_foil});
//
//            Area ar1_foilIntCoil1Q2 = ar1_foilIntCoil1Q1.translate(-dx,-dy).mirrorY().translate(dx,dy);
//            Area ar1_foilIntCoil1Q3 = ar1_foilIntCoil1Q1.translate(-dx,-dy).mirrorX().translate(dx,dy);
//            Area ar1_foilIntCoil1Q4 = ar1_foilIntCoil1Q3.translate(-dx,-dy).mirrorY().translate(dx,dy);
//            Element el1_foilIntCoil1Q1 = new Element(coilName+"_FB_Q1", ar1_foilIntCoil1Q1);
//            Element el1_foilIntCoil1Q2 = new Element(coilName+"_FB_Q2", ar1_foilIntCoil1Q2);
//            Element el1_foilIntCoil1Q3 = new Element(coilName+"_FB_Q3", ar1_foilIntCoil1Q3);
//            Element el1_foilIntCoil1Q4 = new Element(coilName+"_FB_Q4", ar1_foilIntCoil1Q4);
//
//
////        return new Element[] {el1_foilIntCoil1Q1,el1_foilIntCoil1Q2};
//            return new Element[] {el1_foilIntCoil1Q1,el1_foilIntCoil1Q2,el1_foilIntCoil1Q3,el1_foilIntCoil1Q4};
////        return new Element[] {el1_foilIntCoil1Q1};
//
//        }

    public Element[] iron_yoke() {

        // POINTS

        // include-file of the one-piece yoke from modified MB-dipole
        // Christine Voellinger and Martin Aleksa     - 31.08.2000
        // Nose and Nick corrected by Nikolai Schwerg - 25.10.2004
        // Checked by Bernhard Auchmann and Per Hagen - 31.03.2011

        // referring to: LHCMB__A0136|0|C and LHCMB__A0133|0|C

        Point kpyoke_1 = Point.ofCartesian(beamd + d1 + alel1, 0);
        Point kpyoke_2 = Point.ofCartesian(ryoke, 0);

        Point kpnick_1 = Point.ofCartesian(nick_x, Math.sqrt(ryoke * ryoke - nick_x * nick_x));
        Point kpnick_2 = Point.ofCartesian(xnick - rnick, Math.sqrt(ryoke * ryoke - (xnick - rnick) * (xnick - rnick)));

        Point kpbus_1 = Point.ofPolar(ryoke, (phim - gammah));
        Point kpbus_2 = Point.ofPolar(ryoke, (phim + gammah));

        Point kpnose_1 = Point.ofCartesian(hx1, Math.sqrt(ryoke * ryoke - hx1 * hx1));
        Point kpnose_1a = Point.ofCartesian(hx2, rnose + hy);
        Point kpnose_2 = Point.ofCartesian(0, rnose);

        Point kphhole_1 = Point.ofCartesian(lhhx, lhhy + lhhr);
        Point kphhole_2 = Point.ofCartesian(lhhx + lhhr * Math.cos(phihco), lhhy + lhhr * Math.sin(phihco));
        Point kphhole_4 = Point.ofCartesian(lhhx + lhhr * Math.cos(phihco), lhhy - lhhr * Math.sin(phihco));
        Point kphhole_5 = Point.ofCartesian(lhhx, lhhy - lhhr);

        Point kpins_1 = Point.ofCartesian(0, blel1 + houdh + (beamd - houdw) * Math.tan(linsphi));
        Point kpins_7 = Point.ofCartesian(linscut, yinscut2);

        Point kphoud_0 = Point.ofCartesian(beamd - houdw, blel1 + houdh);
        Point kphoud_1 = Point.ofCartesian(beamd + houdw, blel1 + houdh);
        Point kphoud_2 = Point.ofCartesian(beamd + houdw + houdsh, linsh1);

        Point kpyok_col1 = Point.ofCartesian(beamd + d1 + Math.sqrt((1 - (cush1 * cush1) / (blel1 * blel1))) * (alel1), cush1);
        Point kpyok_col2 = Point.ofCartesian(cn1x, Math.sqrt(1 - ((cn1x - beamd - d1) / alel1) * ((cn1x - beamd - d1) / alel1)) * blel1);
        Point kpyok_col3 = Point.ofCartesian(beamd + d1 + Math.sqrt((1 - (cush2 * cush2) / (blel1 * blel1))) * (alel1), cush2);


        // LINES

        Line lnyoke_1 = Line.ofEndPoints(kpyoke_1, kpyoke_2);
        Arc lnyoke_2 = Arc.ofEndPointsRadius(kpnick_1, kpyoke_2, ryoke);

        // lnyoke_3 = HyperLine(kpnick_1,kpnick_2,"Notch",Math.PI/2-phinick,Math.PI/2);
        Line[] lines_lnyoke_3 = RoxieGeometryInferface.Notch(kpnick_1, kpnick_2, Math.PI / 2 - phinick, Math.PI / 2);
        Line lnyoke_3a = lines_lnyoke_3[0];
        Line lnyoke_3b = lines_lnyoke_3[1];

        Arc lnyoke_4 = Arc.ofEndPointsRadius(kpbus_1, kpnick_2, ryoke);

        // lnyoke_5 = HyperLine(kpbus_1,kpbus_2,"Bar",bush);
        Line[] lines_lnyoke_5 = RoxieGeometryInferface.Bar(kpbus_1, kpbus_2, bush);
        Line lnyoke_5a = lines_lnyoke_5[0];
        Line lnyoke_5b = lines_lnyoke_5[1];
        Line lnyoke_5c = lines_lnyoke_5[2];

        Arc lnyoke_6 = Arc.ofEndPointsRadius(kpnose_1, kpbus_2, ryoke);

        // lnyoke_7 = HyperLine(kpnose_1,kpnose_1a,"CornerIn");
        Line[] lines_lnyoke_7 = RoxieGeometryInferface.CornerIn(kpnose_1, kpnose_1a);
        Line lnyoke_7a = lines_lnyoke_7[0];
        Line lnyoke_7b = lines_lnyoke_7[1];

        // lnyoke_8 = HyperLine(kpnose_1a,kpnose_2,"Notch",-phinos,0.);
        Line[] lines_lnyoke_8 = RoxieGeometryInferface.Notch(kpnose_1a, kpnose_2, -phinos, 0);
        Line lnyoke_8a = lines_lnyoke_8[0];
        Line lnyoke_8b = lines_lnyoke_8[1];

        Line lnyoke_9 = Line.ofEndPoints(kpnose_2, kphhole_1);
        Arc lnyoke_10 = Arc.ofEndPointsRadius(kphhole_1, kphhole_2, lhhr);

        // lnyoke_11= HyperLine(kphhole_2,kphhole_4,"Notch",Math.PI/2.+phihco,Math.PI/2.-phihco);
        Line[] lines_lnyoke_11 = RoxieGeometryInferface.Notch(kphhole_2, kphhole_4, Math.PI / 2. + phihco, Math.PI / 2. - phihco);
        Line lnyoke_11a = lines_lnyoke_11[0];
        Line lnyoke_11b = lines_lnyoke_11[1];

        Arc lnyoke_13 = Arc.ofEndPointsRadius(kphhole_4, kphhole_5, lhhr);

        Line lnyoke_14 = Line.ofEndPoints(kphhole_5, kpins_1);
        Line lnyoke_15 = Line.ofEndPoints(kpins_1, kpins_7);
        Line lnyoke_15a = Line.ofEndPoints(kpins_7, kphoud_0);
        Line lnyoke_16 = Line.ofEndPoints(kphoud_0, kphoud_1);
        Line lnyoke_17 = Line.ofEndPoints(kphoud_1, kphoud_2);

        EllipticArc lnyoke_18 = EllipticArc.ofEndPointsAxes(kphoud_2, kpyok_col1, alel1, blel1);
        EllipticArc lnyoke_19 = EllipticArc.ofEndPointsAxes(kpyok_col1, kpyok_col2, alel1, blel1);
        EllipticArc lnyoke_20 = EllipticArc.ofEndPointsAxes(kpyok_col2, kpyok_col3, alel1, blel1);
        EllipticArc lnyoke_21 = EllipticArc.ofEndPointsAxes(kpyok_col3, kpyoke_1, alel1, blel1);


        // AREAS FIRST QUADRANT
        Area ar1_1 = Area.ofHyperLines(new HyperLine[]{lnyoke_1, lnyoke_2, lnyoke_3a, lnyoke_3b, lnyoke_4, lnyoke_5a, lnyoke_5b, lnyoke_5c, lnyoke_6, lnyoke_7a, lnyoke_7b, lnyoke_8a, lnyoke_8b, lnyoke_9, lnyoke_10, lnyoke_11a, lnyoke_11b, lnyoke_13, lnyoke_14, lnyoke_15, lnyoke_15a, lnyoke_16, lnyoke_17, lnyoke_18, lnyoke_19, lnyoke_20, lnyoke_21});

        // AREAS OTHER QUADRANTS
        Area ar1_2 = ar1_1.mirrorY();
        Area ar1_3 = ar1_2.mirrorX();
        Area ar1_4 = ar1_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element el1_1 = new Element("IY_El1", ar1_1);

        // ELEMENTS OTHER QUADRANTS
        Element el1_2 = new Element("IY_El2", ar1_2);
        Element el1_3 = new Element("IY_El3", ar1_3);
        Element el1_4 = new Element("IY_El4", ar1_4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1};
        Element[] quad2 = {el1_2};
        Element[] quad3 = {el1_3};
        Element[] quad4 = {el1_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{el1_1, el1_4};


//        return new Element[]{el1_1, el1_2, el1_3, el1_4};
//        return new Element[]{el1_1, el1_4};
//         return new Element[] { el1_1 };
    }

    public Element[] iron_insert() {

        // include-file of the modified insert for modified MB dipole/Christine Voellinger,Martin Aleksa - 31.08.2000
        // referring to: LHCMB__A0148|1|E and insert modification sheet
        // Checked by Bernhard Auchmann and Per Hagen 31.03.2011

        // POINTS

        Point kpins_1 = Point.ofCartesian(0, blel1 + houdh + (beamd - houdw) * Math.tan(linsphi));
        Point kpins_2 = Point.ofCartesian(0, linsh);
        Point kpins_3 = Point.ofCartesian(linsw, linsh);
        Point kpins_4 = Point.ofCartesian(linsw, linsh + linsnh);
        Point kpins_4a = Point.ofCartesian(lh5x - dxvar, lh5y - dyvar);
        Point kpins_4b = Point.ofCartesian(lh5x + dxvar, lh5y + dyvar);
        Point kpins_5 = Point.ofCartesian(beamd - d2 - Math.sqrt((1 - (cush1 * cush1) / (blel1 * blel1))) * (alel2), cush1);
        Point kpins_6 = Point.ofCartesian(linscut, yinscut1);
        Point kpins_7 = Point.ofCartesian(linscut, yinscut2);

        // LINES

        Line lnins_1 = Line.ofEndPoints(kpins_1, kpins_2);
        Line lnins_2 = Line.ofEndPoints(kpins_2, kpins_3);
        Line lnins_3 = Line.ofEndPoints(kpins_3, kpins_4);
        // lnins_4 = HyperLine(kpins_4,kpins_5,"EllipticArc",alel2,blel1,0.5);
        EllipticArc lnins_4 = EllipticArc.ofEndPointsAxes(kpins_4, kpins_4a, alel2, blel1);

        // Arc lnins_4a = new Arc(new Point[] {kpins_4b, kpins_4a}, -lh5r);
        Point kpc = Point.ofCartesian(lh5x, lh5y);
        double angle = -Math.PI;
        Arc lnins_4a = Arc.ofPointCenterAngle(kpins_4a, kpc, angle);

        EllipticArc lnins_4b = EllipticArc.ofEndPointsAxes(kpins_4b, kpins_5, alel2, blel1);
        EllipticArc lnins_5 = EllipticArc.ofEndPointsAxes(kpins_5, kpins_6, alel2, blel1);
        Line lnins_6 = Line.ofEndPoints(kpins_6, kpins_7);
        Line lnyoke_15 = Line.ofEndPoints(kpins_1, kpins_7);

        // AREAS FIRST QUADRANT
        Area arinsert_1 = Area.ofHyperLines(new HyperLine[]{lnins_1, lnins_2, lnins_3, lnins_4, lnins_4a, lnins_4b, lnins_5, lnins_6, lnyoke_15});

        // AREAS OTHER QUADRANTS
        Area arinsert_2 = arinsert_1.mirrorY();
        Area arinsert_3 = arinsert_2.mirrorX();
        Area arinsert_4 = arinsert_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element el_1 = new Element("II_El1_1", arinsert_1);

        // ELEMENTS OTHER QUADRANTS
        Element el_2 = new Element("II_El1_2", arinsert_2);
        Element el_3 = new Element("II_El1_3", arinsert_3);
        Element el_4 = new Element("II_El1_4", arinsert_4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el_1};
        Element[] quad2 = {el_2};
        Element[] quad3 = {el_3};
        Element[] quad4 = {el_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{el_1, el_4};


//        return new Element[]{el_1, el_2, el_3, el_4};
//        return new Element[]{el_1, el_4};
//        return new Element[]{el_1};
    }

    public Element[] holes_yoke() {

        // POINTS
        Point kph2_1 = Point.ofCartesian(lh2x, lh2y + lh2r);
        Point kph2_2 = Point.ofCartesian(lh2x, lh2y - lh2r);
        Point kph3_1 = Point.ofCartesian(lh3x, lh3y + lh3r);
        Point kph3_2 = Point.ofCartesian(lh3x, lh3y - lh3r);
        Point kph4_1 = Point.ofCartesian(lh4x, lh4y + lh4r);
        Point kph4_2 = Point.ofCartesian(lh4x, lh4y - lh4r);

        // LINES
        Circumference lnh2 = Circumference.ofDiameterEndPoints(kph2_1, kph2_2);
        Circumference lnh3 = Circumference.ofDiameterEndPoints(kph3_1, kph3_2);
        Circumference lnh4 = Circumference.ofDiameterEndPoints(kph4_1, kph4_2);

        // AREAS FIRST QUADRANT
        Area arh1_1 = Area.ofHyperLines(new HyperLine[]{lnh2});
        Area arh2_1 = Area.ofHyperLines(new HyperLine[]{lnh3});
        Area arh3_1 = Area.ofHyperLines(new HyperLine[]{lnh4});

        // AREAS OTHER QUADRANTS
        Area arh1_2 = arh1_1.mirrorY();
        Area arh1_3 = arh1_2.mirrorX();
        Area arh1_4 = arh1_1.mirrorX();

        Area arh2_2 = arh2_1.mirrorY();
        Area arh2_3 = arh2_2.mirrorX();
        Area arh2_4 = arh2_1.mirrorX();

        Area arh3_2 = arh3_1.mirrorY();
        Area arh3_3 = arh3_2.mirrorX();
        Area arh3_4 = arh3_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element ho1_1 = new Element("IY_HOLE1_1", arh1_1);
        Element ho2_1 = new Element("IY_HOLE2_1", arh2_1);
        Element ho3_1 = new Element("IY_HOLE3_1", arh3_1);

        // ELEMENTS OTHER QUADRANTS
        Element ho1_2 = new Element("IY_HOLE1_2", arh1_2);
        Element ho1_3 = new Element("IY_HOLE1_3", arh1_3);
        Element ho1_4 = new Element("IY_HOLE1_4", arh1_4);

        Element ho2_2 = new Element("IY_HOLE2_2", arh2_2);
        Element ho2_3 = new Element("IY_HOLE2_3", arh2_3);
        Element ho2_4 = new Element("IY_HOLE2_4", arh2_4);

        Element ho3_2 = new Element("IY_HOLE3_2", arh3_2);
        Element ho3_3 = new Element("IY_HOLE3_3", arh3_3);
        Element ho3_4 = new Element("IY_HOLE3_4", arh3_4);

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_2, ho2_2, ho3_2,
//                ho1_3, ho2_3, ho3_3,
//                ho1_4, ho2_4, ho3_4,
//        };

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_4, ho2_4, ho3_4,
//        };

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho1_1, ho2_1, ho3_1};
        Element[] quad2 = {ho1_2, ho2_2, ho3_2};
        Element[] quad3 = {ho1_3, ho2_3, ho3_3};
        Element[] quad4 = {ho1_4, ho2_4, ho3_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{ho1_1, ho2_1, ho3_1, ho1_4, ho2_4, ho3_4};


//         return new Element[] {ho1_1, ho2_1, ho3_1};
    }

    public Element[] holes_insert() {

        // POINTS
        Point kph1_1 = Point.ofCartesian(lh1x, lh1y + lh1r);
        Point kph1_2 = Point.ofCartesian(lh1x, lh1y - lh1r);

        // LINES
        Circumference lnh1 = Circumference.ofDiameterEndPoints(kph1_1, kph1_2);

        // AREAS FIRST QUADRANT
        Area arh_1 = Area.ofHyperLines(new HyperLine[]{lnh1});

        // AREAS OTHER QUADRANTS
        Area arh_2 = arh_1.mirrorY();
        Area arh_3 = arh_2.mirrorX();
        Area arh_4 = arh_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element ho_1 = new Element("II_HOLE1_1", arh_1);

        // ELEMENTS OTHER QUADRANTS
        Element ho_2 = new Element("II_HOLE1_2", arh_2);
        Element ho_3 = new Element("II_HOLE1_3", arh_3);
        Element ho_4 = new Element("II_HOLE1_4", arh_4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho_1};
        Element[] quad2 = {ho_2};
        Element[] quad3 = {ho_3};
        Element[] quad4 = {ho_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{ho_1, ho_4};


//        return new Element[]{ho_1, ho_2, ho_3, ho_4};
//        return new Element[]{ho_1, ho_4};
//        return new Element[]{ho_1};
    }

    public Element[] BoundaryConditions(){
        double length = 10;
        double eps  = 1e-4;

        //POINTS
        Point kpx1 = Point.ofCartesian(-length, 0-eps);
        Point kpx2 = Point.ofCartesian(length, 0-eps);
        Point kpx3 = Point.ofCartesian(length, 0+eps);
        Point kpx4 = Point.ofCartesian(-length, 0+eps);

        Point kpy1 = Point.ofCartesian(0-eps, -length);
        Point kpy2 = Point.ofCartesian(0+eps, -length);
        Point kpy3 = Point.ofCartesian(0+eps, length);
        Point kpy4 = Point.ofCartesian(0-eps, length);

        // LINES
        Line lnx1 = Line.ofEndPoints(kpx2, kpx1);
        Line lnx2 = Line.ofEndPoints(kpx2, kpx3);
        Line lnx3 = Line.ofEndPoints(kpx3, kpx4);
        Line lnx4 = Line.ofEndPoints(kpx1, kpx4);

        Line lny1 = Line.ofEndPoints(kpy2, kpy1);
        Line lny2 = Line.ofEndPoints(kpy2, kpy3);
        Line lny3 = Line.ofEndPoints(kpy3, kpy4);
        Line lny4 = Line.ofEndPoints(kpy1, kpy4);

        // AREAS
        Area xBC_Sel = Area.ofHyperLines(new HyperLine[]{lnx1, lnx2, lnx3, lnx4});
        Area yBC_Sel = Area.ofHyperLines(new HyperLine[]{lny1, lny2, lny3, lny4});

        // ELEMENTS
        Element xBC = new Element("xBC", xBC_Sel);
        Element yBC = new Element("yBC", yBC_Sel);

        return new Element[]{xBC, yBC};
    }

}
