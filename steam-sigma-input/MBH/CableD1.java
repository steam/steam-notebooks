package input.HL_LHC.D1;


import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableD1 extends Cable {

    public CableD1() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_D1";

        //Insulation
        this.wInsulNarrow = 1.55e-4; // [m];
        this.wInsulWide = 1.35e-4; // [m];
        //Filament
        this.dFilament = 6.0e-6; // [m];
        //Strand
        this.dstrand = 0.825e-3; // [m];
        this.fracCu = 1.95 /(1+1.95);
        this.fracSc = 1/(1+1.95);
        this.RRR = 150;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 15e-3; // [m];
        //Cable
        this.wBare = 15e-3; // [m];
        this.hInBare = 1.5e-3; // [m];
        this.hOutBare = 1.5e-3; // [m];
        this.noOfStrands = 36;
        this.noOfStrandsPerLayer = 18;
        this.noOfLayers = 2;
        this.lTpStrand = 0.1; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 65821.9; // [A];
        this.C2 = -5042.6; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_NbTi_GSI;
        this.insulationMaterial = MatDatabase.MAT_KAPTON;
        this.materialInnerVoids = MatDatabase.MAT_VOID;
        this.materialOuterVoids = MatDatabase.MAT_VOID;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_CUDI;
    }


}
