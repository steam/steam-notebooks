package input.HL_LHC.M11T;


import input.HL_LHC.D1.CableD1;
import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.domains.database.WedgeDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

public class Magnet_11T extends UtilsUserInput {

    // VARIABLES
    double deg = Math.PI / 180;
    double shrink = 0.998;
    double cshrink = 0.9973;
    double csshrink = 0.996;
    double BEAMD = 97.00;
    double beamd = BEAMD * 1e-3;
    double RYOKE =275.00;
    double ryoke = RYOKE * 1e-3 * shrink;
    double HX1   =18.00;
    double hx1 = HX1 * 1e-3 * shrink;
    double HX2   =21.00;
    double hx2 = HX2 * 1e-3 * shrink;
    double HY    =13.00;
    double hy = HY * 1e-3 * shrink;
    double phinos = Math.atan(hy / (hx2 - hx1));
    double phinoseu = Math.PI / 2. - Math.asin(hx1 / ryoke);
    double rnose = ryoke * Math.sin(phinoseu) - hy;
    double XNICK = 200.00;
    double xnick = XNICK * 1e-3 * shrink;
    double YNICK = 168.00;
    double ynick = YNICK * 1e-3 * shrink;
    double phinicku =Math.acos(xnick / ryoke);
    double phinickd = Math.asin(ynick / ryoke);
    double BBR = 244.00;
    double bbr = BBR * 1e-3;
    double BBW = 51.00;
    double bbw = BBW * 1e-3;
    double PHIM = 53.6;
    double phim = PHIM * deg;
    double bush = Math.sqrt(ryoke * ryoke - bbw * bbw / 4) - bbr;
    double gammah = Math.atan(bbw / (2 * ryoke));
    double LH1R  =4.0;
    double lh1r = LH1R * 1e-3 * shrink;
    double LH2PHI =48.5432;
    double lh2phi = LH2PHI * deg;
    double LH2RP =210.0654;
    double lh2rp = LH2RP * 1e-3 * shrink;
    double LH2R  =19.3819;
    double lh2r = LH2R * 1e-3 * shrink;
    double LH3PHI =47.1698;
    double lh3phi = LH3PHI * deg;
    double LH3RP =100.058;
    double lh3rp = LH3RP * 1e-3 * shrink;
    double LH3R  =5.4957;
    double lh3r = LH3R * 1e-3 * shrink;
    double LH4PHI =35.513;
    double lh4phi = LH4PHI * deg;
    double LH4RP =101.70;
    double lh4rp = LH4RP * 1e-3 * shrink;
    double LH4R  =4.8273;
    double lh4r = LH4R * 1e-3 * shrink;
    double LH5PHI = 26.1098;
    double lh5phi = LH5PHI * deg;
    double LH5RP =101.226;
    double lh5rp = LH5RP * 1e-3 * shrink;
    double LH5R  =3.6252;
    double lh5r = LH5R * 1e-3 * shrink;
    double lh2x = lh2rp * Math.cos(lh2phi);
    double lh2y = lh2rp * Math.sin(lh2phi);
    double lh3x = lh3rp * Math.cos(lh3phi) + beamd;
    double lh3y = lh3rp * Math.sin(lh3phi);
    double lh4x = lh4rp * Math.cos(lh4phi) + beamd;
    double lh4y = lh4rp * Math.sin(lh4phi);
    double lh5x = lh5rp * Math.cos(lh5phi) + beamd;
    double lh5y = lh5rp * Math.sin(lh5phi);
    double LHHX  =0.00;
    double lhhx = LHHX * 1e-3;
    double LHHY  =179.6;
    double lhhy = LHHY * 1e-3;
    double LHHR  =30.00;
    double lhhr = LHHR * 1e-3 * shrink;
    double PHIHCO =30.00;
    double phihco = PHIHCO * deg;
    double HOODH =97.3350;
    double hoodh = HOODH * 1e-3 * shrink;
    double HOODR =79.9009;
    double hoodr = HOODR * 1e-3 * shrink;
    double YOKRIN =91.5;
    double yokrin = YOKRIN * 1e-3 * shrink;
    double INSH1 =118.7;
    double insh1 = INSH1 * 1e-3 * shrink;
    double INSH2 =112.9790;
    double insh2 = INSH2 * 1e-3 * shrink;
    double INSALP =15.0;
    double insalpha = INSALP * deg;
    double INSRIN =YOKRIN;
    double insrin = INSRIN * 1e-3 * shrink;
    double INSBET =68.8128;
    double insbeta = INSBET * deg;
    double INSH1Y =32.2170;
    double insh1y = INSH1Y * 1e-3 * shrink;
    double INSH2Y =70.1225;
    double insh2y = INSH2Y * 1e-3 * shrink;
    double INSH1R =5.9751;
    double insh1r = INSH1R * 1e-3 * shrink;
    double INSH2R =16.1775;
    double insh2r = INSH2R * 1e-3 * shrink;
    double insw1 = (insh1 - hoodh) / Math.tan(insalpha);
    double insw2 = (insh1 - insh2) / Math.tan(insalpha);
    double hoodw = beamd - insw1;


    private final Domain[] domains;

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Magnet_11T() {

        setQuadrantsToBeConstructed(new int[]{1,2,3,4});

        domains = new Domain[] {
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, elem_ironYoke()),
                new AirDomain("OuterShell", MatDatabase.MAT_STEEL, elem_outerShell()),
                new AirDomain("SteelCollar", MatDatabase.MAT_STEEL, elem_steelCollar()),
                new AirDomain("nose", MatDatabase.MAT_STEEL, elem_nose()),
//                new AirDomain("airDomain", MatDatabase.MAT_AIR, elem_air()),
//                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, elem_airFarField()),
//                new BoundaryConditionsDomain("BC", MatDatabase.MAT_NULL, this.xAxisBC, this.yAxisBC, DomainBoundaryCondition()),
                new CoilDomain("cR", MatDatabase.MAT_COIL, elem_coil().translate(beamd,0)),
                new CoilDomain("cL", MatDatabase.MAT_COIL, elem_coil().translate(-beamd,0)),
                new WedgeDomain("elem_wedge", MatDatabase.MAT_COPPER, elem_wedge()),
//                new InsulationDomain("insulation", MatDatabase.MAT_KAPTON, insulation(coilC,"coil_OLD")),
//                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, elem_ironYoke()),
//                new QuenchHeaterDomain("QH_lowField", "poly1", quenchHeaters()),
                new AirDomain("holesYoke", MatDatabase.MAT_AIR, elem_yokeHole()),
        };

    }

    private Coil elem_coil() {
        double deg2rad = Math.PI/180;
        double r1 = 0.03;
        double deltar = 0.0001;
        double r2a = 0.04525-deltar;
        double r2b = 0.04525+deltar;
        double r3 = 0.0605;

        Point kp101 = Point.ofPolar(r1, 0*deg2rad);
        Point kp102 = Point.ofPolar(r2a, 0*deg2rad);
        Point kp103 = Point.ofPolar(r2a, 18.65934*deg2rad);
        Point kp104 = Point.ofPolar(r1, 24.18836*deg2rad);

        Point kp105 = Point.ofPolar(r1, 26.69416*deg2rad);
        Point kp106 = Point.ofPolar(r2a, 27.12536*deg2rad);
        Point kp109 = Point.ofPolar(r2a, 43.35745*deg2rad);
        Point kp110 = Point.ofPolar(r1, 47.69625*deg2rad);

        Point kp111 = Point.ofPolar(r1, 55.95846*deg2rad);
        Point kp112 = Point.ofPolar(r2a, 56.96191*deg2rad);
        Point kp114 = Point.ofPolar(r2a, 62.86518*deg2rad);
        Point kp115 = Point.ofPolar(r1, 63.577431*deg2rad);

        Point kp116 = Point.ofPolar(r1, 70.5744*deg2rad);
        Point kp117 = Point.ofPolar(r2a, 70.38467*deg2rad);
        Point kp118 = Point.ofPolar(r2a, 74.23456*deg2rad);
        Point kp119 = Point.ofPolar(r1, 75.52425*deg2rad);

        Point kp120 = Point.ofPolar(r2b, 0*deg2rad);
        Point kp121 = Point.ofPolar(r3, 0*deg2rad);
        Point kp122 = Point.ofPolar(r3, 24.62702*deg2rad);
        Point kp123 = Point.ofPolar(r2b, 28.388*deg2rad);

        Point kp126 = Point.ofPolar(r2b, 30.24952*deg2rad);
        Point kp127 = Point.ofPolar(r3, 30.92383*deg2rad);
        Point kp128 = Point.ofPolar(r3, 58.34695*deg2rad);
        Point kp129 = Point.ofPolar(r2b, 61.82651*deg2rad);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp101, kp104, kp0);
        Line ln12 = Line.ofEndPoints(kp101, kp102);
        Arc ln13 = Arc.ofEndPointsCenter(kp102, kp103, kp0);
        Line ln14 = Line.ofEndPoints(kp104, kp103);

        Arc ln21 = Arc.ofEndPointsCenter(kp105, kp110, kp0);
        Line ln22 = Line.ofEndPoints(kp105, kp106);
        Arc ln23 = Arc.ofEndPointsCenter(kp106, kp109, kp0);
        Line ln24 = Line.ofEndPoints(kp110, kp101);

        Arc ln31 = Arc.ofEndPointsCenter(kp111, kp115, kp0);
        Line ln32 = Line.ofEndPoints(kp111, kp112);
        Arc ln33 = Arc.ofEndPointsCenter(kp112, kp114, kp0);
        Line ln34 = Line.ofEndPoints(kp115, kp114);

        Arc ln41 = Arc.ofEndPointsCenter(kp116, kp119, kp0);
        Line ln42 = Line.ofEndPoints(kp116, kp117);
        Arc ln43 = Arc.ofEndPointsCenter(kp117, kp118, kp0);
        Line ln44 = Line.ofEndPoints(kp119, kp118);

        Arc ln51 = Arc.ofEndPointsCenter(kp120, kp123, kp0);
        Line ln52 = Line.ofEndPoints(kp120, kp121);
        Arc ln53 = Arc.ofEndPointsCenter(kp121, kp122, kp0);
        Line ln54 = Line.ofEndPoints(kp123, kp122);

        Arc ln61 = Arc.ofEndPointsCenter(kp126, kp129, kp0);
        Line ln62 = Line.ofEndPoints(kp126, kp127);
        Arc ln63 = Arc.ofEndPointsCenter(kp127, kp128, kp0);
        Line ln64 = Line.ofEndPoints(kp129, kp128);

        Area ha1p = Area.ofHyperLines(new HyperLine[] {ln11, ln12, ln13, ln14});
        Area ha2p = Area.ofHyperLines(new HyperLine[] {ln21, ln22, ln23, ln24});
        Area ha3p = Area.ofHyperLines(new HyperLine[] {ln31, ln32, ln33, ln34});
        Area ha4p = Area.ofHyperLines(new HyperLine[] {ln41, ln42, ln43, ln44});
        Area ha5p = Area.ofHyperLines(new HyperLine[] {ln51, ln52, ln53, ln54});
        Area ha6p = Area.ofHyperLines(new HyperLine[] {ln61, ln62, ln63, ln64});

        // negative winding cross-section, pole 1:
        Area ha1n = ha1p.mirrorY();
        Area ha2n = ha2p.mirrorY();
        Area ha3n = ha3p.mirrorY();
        Area ha4n = ha4p.mirrorY();
        Area ha5n = ha5p.mirrorY();
        Area ha6n = ha6p.mirrorY();

        Winding w0 = Winding.ofAreas(new Area[] {ha1p, ha1n}, new int[]{+1,-1}, 9, 9,new CableD1());
        Winding w1 = Winding.ofAreas(new Area[] {ha2p, ha2n}, new int[]{+1,-1}, 8, 8,new CableD1());
        Winding w2 = Winding.ofAreas(new Area[] {ha3p, ha3n}, new int[]{+1,-1}, 3, 3,new CableD1());
        Winding w3 = Winding.ofAreas(new Area[] {ha4p, ha4n}, new int[]{+1,-1}, 2, 2,new CableD1());
        Winding w4 = Winding.ofAreas(new Area[] {ha5p, ha5n}, new int[]{+1,-1}, 16, 16,new CableD1());
        Winding w5 = Winding.ofAreas(new Area[] {ha6p, ha6n}, new int[]{+1,-1}, 18, 18,new CableD1());

        Pole p1 = Pole.ofWindings(new Winding[]{w0,w1,w2,w3,w4,w5});
        Pole p2 = p1.mirrorX();

        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1,p2});
        return c1;
    }

    private Element[] elem_outerShell(){
        double thickShell = 10 * 1e-3;

        Point kpins8    = Point.ofCartesian(0,0);
        Point kpShell_1_in  = Point.ofCartesian(ryoke,0);
        Point kpShell_1_out = Point.ofCartesian(ryoke+thickShell,0);
        Point kpShell_2_in  = Point.ofCartesian(0,ryoke);
        Point kpShell_2_out = Point.ofCartesian(0,ryoke+thickShell);

        Line lnShell1 = Line.ofEndPoints(kpShell_1_in,kpShell_1_out);
        Arc lnShell2 = Arc.ofEndPointsCenter(kpShell_1_out,kpShell_2_out,kpins8);
        Line lnShell3 = Line.ofEndPoints(kpShell_2_out,kpShell_2_in);
        Arc lnShell4 = Arc.ofEndPointsCenter(kpShell_2_in,kpShell_1_in,kpins8);

        Area arShell = Area.ofHyperLines(new HyperLine[]{lnShell1,lnShell2,lnShell3,lnShell4});

        // Quadrant 1
        Element el1_1 = new Element("OS_El1_1", arShell);
        // Quadrant 2
        Element el2_1 = new Element("OS_El2_1", arShell.mirrorY());
        // Quadrant 3
        Element el3_1 = new Element("OS_El3_1", arShell.mirrorY().mirrorX());
        // Quadrant 4
        Element el4_1 = new Element("OS_El4_1", arShell.mirrorX());

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1};
        Element[] quad2 = {el2_1};
        Element[] quad3 = {el3_1};
        Element[] quad4 = {el4_1};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;

    }

    private Element[] elem_nose(){
        Point kpTW1 = Point.ofCartesian(0.0927227, 0.02996438);
        Point kpTW2 = Point.ofCartesian(0.1016, 0.02996438);
        Point kpTW3 = Point.ofCartesian(0.11153394, 0.05945378);
        Point kpTW4 = Point.ofCartesian(0.0829183, 0.05945378);
        Point kpCenterTW = Point.ofCartesian(beamd,0);
        double radiusInTW = 0.0303;
        double radiusOutTW = 0.06115;
        Arc lnTW1 = Arc.ofEndPointsRadius(kpTW1,kpTW2,radiusInTW);
        Line lnTW2 = Line.ofEndPoints(kpTW2,kpTW3);
        Arc lnTW3 = Arc.ofEndPointsRadius(kpTW4,kpTW3,radiusOutTW);
        Line lnTW4 = Line.ofEndPoints(kpTW4,kpTW1);

        Area arTW = Area.ofHyperLines(new HyperLine[]{lnTW1,lnTW2,lnTW3,lnTW4});
        // Quadrant 1
        Element el1_1 = new Element("NS_El1_1", arTW);
        // Quadrant 2
        Element el2_1 = new Element("NS_El2_1", arTW.mirrorY());
        // Quadrant 3
        Element el3_1 = new Element("NS_El3_1", arTW.mirrorY().mirrorX());
        // Quadrant 4
        Element el4_1 = new Element("NS_El4_1", arTW.mirrorX());

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1};
        Element[] quad2 = {el2_1};
        Element[] quad3 = {el3_1};
        Element[] quad4 = {el4_1};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;
    }

    private Element[] elem_steelCollar(){
        Point kpCenterCollar = Point.ofCartesian(beamd,0);
        double thickCollar =  29e-3;
        double rInCollar = yokrin - thickCollar;
        double rOutCollar = yokrin;

        Point kpyoke_4  = Point.ofCartesian(beamd+hoodw,hoodh);
        Point kpyoke_5  = Point.ofCartesian(beamd+yokrin*Math.cos(insbeta),yokrin*Math.sin(insbeta));
        Point kpyoke_6  = Point.ofCartesian(beamd+yokrin,0);
        Point kpins9    = Point.ofCartesian(beamd-insrin,0);
        Point kpins10   = Point.ofCartesian(beamd-insrin*Math.cos(insbeta),insrin*Math.sin(insbeta));
        Point kpins11   = Point.ofCartesian(insw1,hoodh);

        Point kpColl_1_in  = Point.ofCartesian(beamd+yokrin-thickCollar,0);
        Point kpColl_1_out = Point.ofCartesian(beamd+yokrin,0);
        Point kpColl_3_in  = Point.ofCartesian(beamd-rInCollar,0);
        Point kpColl_3_out = Point.ofCartesian(beamd-rOutCollar,0);

        Arc lnins7 = Arc.ofEndPointsRadius(kpins9, kpins10, insrin);
        Line lnins8 = Line.ofEndPoints(kpins10, kpins11);
        Arc lnyoke_16 = Arc.ofEndPointsRadius(kpins11, kpyoke_4, hoodr);
        Line lnyoke_17 = Line.ofEndPoints(kpyoke_4, kpyoke_5);
        Arc lnyoke_18 = Arc.ofEndPointsRadius(kpyoke_5, kpyoke_6, yokrin);

        Line lnColl1 = Line.ofEndPoints(kpColl_1_in,kpColl_1_out);
        Line lnColl3 = Line.ofEndPoints(kpColl_3_out,kpColl_3_in);
        Arc lnColl4 = Arc.ofEndPointsCenter(kpColl_3_in,kpColl_1_in,kpCenterCollar);
        Area arColl = Area.ofHyperLines(new HyperLine[]{lnColl1,lnins7,lnins8,lnyoke_16,lnyoke_17,lnyoke_18,lnColl3,lnColl4});

        // Quadrant 1
        Element el1_1 = new Element("SC_El1_1", arColl);
        // Quadrant 2
        Element el2_1 = new Element("SC_El2_1", arColl.mirrorY());
        // Quadrant 3
        Element el3_1 = new Element("SC_El3_1", arColl.mirrorY().mirrorX());
        // Quadrant 4
        Element el4_1 = new Element("SC_El4_1", arColl.mirrorX());

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1};
        Element[] quad2 = {el2_1};
        Element[] quad3 = {el3_1};
        Element[] quad4 = {el4_1};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;
    }

    private Element[] elem_ironYoke() {

        //--------------------------------------------------------------------------------------------------------------
        // Iron Yoke
        //--------------------------------------------------------------------------------------------------------------
        Point kpyoke_2  = Point.ofCartesian(ryoke,0);
        Point kpnick_1  = Point.ofPolar(ryoke,phinickd);
        Point kpnick_2  = Point.ofPolar(ryoke, phinicku);
        Point kpbus_1   = Point.ofPolar(ryoke, (phim-gammah));
        Point kpbus_2   = Point.ofPolar(ryoke, (phim+gammah));
        Point kpnose_1  = Point.ofPolar(ryoke, phinoseu);
        Point kpnose_2  = Point.ofCartesian(0,rnose);
        Point kphhole_1 = Point.ofCartesian(lhhx,lhhy+lhhr);
        Point kphhole_2 = Point.ofCartesian(lhhx+lhhr*Math.cos(phihco),lhhy+lhhr*Math.sin(phihco));
        Point kphhole_4 = Point.ofCartesian(lhhx+lhhr*Math.cos(phihco),lhhy-lhhr*Math.sin(phihco));
        Point kphhole_5 = Point.ofCartesian(lhhx,lhhy-lhhr);
        Point kpyoke_3  = Point.ofCartesian(0,insh1);
        Point kpyoke_4  = Point.ofCartesian(beamd+hoodw,hoodh);
        Point kpyoke_5  = Point.ofCartesian(beamd+yokrin*Math.cos(insbeta),yokrin*Math.sin(insbeta));
        Point kpyoke_6  = Point.ofCartesian(beamd+yokrin,0);
        Point kpins1    = Point.ofCartesian(0,insh2);
        Point kpins2    = Point.ofCartesian(0,insh2y+insh2r);
        Point kpins3    = Point.ofCartesian(insh2r,insh2y);
        Point kpins4    = Point.ofCartesian(0,insh2y-insh2r);
        Point kpins5    = Point.ofCartesian(0,insh1y+insh1r);
        Point kpins6    = Point.ofCartesian(insh1r,insh1y);
        Point kpins7    = Point.ofCartesian(0,insh1y-insh1r);
        Point kpins8    = Point.ofCartesian(0,0);
        Point kpins9    = Point.ofCartesian(beamd-insrin,0);
        Point kpins10   = Point.ofCartesian(beamd-insrin*Math.cos(insbeta),insrin*Math.sin(insbeta));
        Point kpins11   = Point.ofCartesian(insw1,hoodh);
        Point kpins12   = Point.ofCartesian(insw2,insh2);

        Arc lnyoke_2 = Arc.ofEndPointsRadius(kpnick_1, kpyoke_2, ryoke);
        Line[] lnyoke_3 = RoxieGeometryInferface.CornerIn(kpnick_2,kpnick_1);
        Line lnyoke_3a = lnyoke_3[0];
        Line lnyoke_3b = lnyoke_3[1];
        Arc lnyoke_4 = Arc.ofEndPointsRadius(kpbus_1, kpnick_2, ryoke);
        Line[] lnyoke_5 = RoxieGeometryInferface.Bar(kpbus_1, kpbus_2, bush);
        Line lnyoke_5a = lnyoke_5[0];
        Line lnyoke_5b = lnyoke_5[1];
        Line lnyoke_5c = lnyoke_5[2];
        Arc lnyoke_6 = Arc.ofEndPointsRadius(kpnose_1, kpbus_2,ryoke);
        Line[] lnyoke_7 = RoxieGeometryInferface.Notch(kpnose_1, kpnose_2, -phinos, 0.);
        Line lnyoke_7a = lnyoke_7[0];
        Line lnyoke_7b = lnyoke_7[1];
        Line lnyoke_9 = Line.ofEndPoints(kpnose_2, kphhole_1);
        Arc lnyoke_10 = Arc.ofEndPointsRadius(kphhole_1, kphhole_2, lhhr);
        Line[] lnyoke_11 = RoxieGeometryInferface.Notch(kphhole_2, kphhole_4, Math.PI / 2. + phihco, Math.PI / 2. - phihco);
        Line lnyoke_11a = lnyoke_11[0];
        Line lnyoke_11b = lnyoke_11[1];
        Arc lnyoke_13 = Arc.ofEndPointsRadius(kphhole_4, kphhole_5, lhhr);
        Line lnyoke_14 = Line.ofEndPoints(kphhole_5, kpyoke_3);
        Line lnyoke_15 = Line.ofEndPoints(kpyoke_3, kpins12);
        Arc lnyoke_16 = Arc.ofEndPointsRadius(kpins11, kpyoke_4, hoodr);
        Line lnyoke_17 = Line.ofEndPoints(kpyoke_4, kpyoke_5);
        Arc lnyoke_18 = Arc.ofEndPointsRadius(kpyoke_5, kpyoke_6, yokrin);
        Line lnyoke_19 = Line.ofEndPoints(kpyoke_6, kpyoke_2);
        Line lnins1 = Line.ofEndPoints(kpins1, kpins2);
        Arc lnins2 = Arc.ofThreePoints(kpins2, kpins3, kpins4);
        Line lnins3 = Line.ofEndPoints(kpins4, kpins5);
        Arc lnins4 = Arc.ofThreePoints(kpins5, kpins6, kpins7);
        Line lnins5 = Line.ofEndPoints(kpins7, kpins8);
        Line lnins6 = Line.ofEndPoints(kpins8, kpins9);
        Arc lnins7 = Arc.ofEndPointsRadius(kpins9, kpins10, insrin);
        Line lnins8 = Line.ofEndPoints(kpins10, kpins11);
        Line lnins9 = Line.ofEndPoints(kpins11, kpins12);
        Line lnins10 = Line.ofEndPoints(kpins12, kpins1);


        Area aryoke = Area.ofHyperLines(new HyperLine[]{lnyoke_2,lnyoke_3a,lnyoke_3b, lnyoke_4,lnyoke_5a,lnyoke_5b,lnyoke_5c,lnyoke_6,lnyoke_7a,lnyoke_7b,lnyoke_9,lnyoke_10,lnyoke_11a,lnyoke_11b,lnyoke_13,lnyoke_14,lnyoke_15,lnins9,lnyoke_16,lnyoke_17,lnyoke_18,lnyoke_19});
        Area arins = Area.ofHyperLines(new HyperLine[]{lnins1,lnins2,lnins3,lnins4,lnins5,lnins6,lnins7,lnins8,lnins9,lnins10});

        // Quadrant 1
        Element el1_1 = new Element("IY_El1_1", aryoke);
        Element el1_2 = new Element("IY_El1_2", arins);
        // Quadrant 2
        Element el2_1 = new Element("IY_El2_1", aryoke.mirrorY());
        Element el2_2 = new Element("IY_El2_2", arins.mirrorY());
        // Quadrant 3
        Element el3_1 = new Element("IY_El3_1", aryoke.mirrorY().mirrorX());
        Element el3_2 = new Element("IY_El3_2", arins.mirrorY().mirrorX());
        // Quadrant 4
        Element el4_1 = new Element("IY_El4_1", aryoke.mirrorX());
        Element el4_2 = new Element("IY_El4_2", arins.mirrorX());

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1,el1_2};
        Element[] quad2 = {el2_1,el2_2};
        Element[] quad3 = {el3_1,el3_2};
        Element[] quad4 = {el4_1,el4_2};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;
    }

    private Element[] elem_yokeHole() {
    Point kph2_1    = Point.ofCartesian(lh2x,lh2y+lh2r);
        Point kph2_2    = Point.ofCartesian(lh2x,lh2y-lh2r);
        Point kph3_1    = Point.ofCartesian(lh3x,lh3y+lh3r);
        Point kph3_2    = Point.ofCartesian(lh3x,lh3y-lh3r);
        Point kph4_1    = Point.ofCartesian(lh4x,lh4y+lh4r);
        Point kph4_2    = Point.ofCartesian(lh4x,lh4y-lh4r);
        Point kph5_1    = Point.ofCartesian(lh5x,lh5y+lh5r);
        Point kph5_2    = Point.ofCartesian(lh5x,lh5y-lh5r);


//        Circumference lnh2 = Circumference.ofDiameterEndPoints(kph2_1, kph2_2);
        Circumference lnh3 = Circumference.ofDiameterEndPoints(kph3_1, kph3_2);
        Circumference lnh4 = Circumference.ofDiameterEndPoints(kph4_1, kph4_2);
        Circumference lnh5 = Circumference.ofDiameterEndPoints(kph5_1, kph5_2);

        // AREAS
//        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1});
//        Area ar1 = Area.ofHyperLines(new HyperLine[] {lnh2});
        Area ar2 = Area.ofHyperLines(new HyperLine[]{lnh3});
        Area ar3 = Area.ofHyperLines(new HyperLine[]{lnh4});
        Area ar4 = Area.ofHyperLines(new HyperLine[]{lnh5});

        // MIRRORED AREAS
//        Area ar1_2 = ar1.mirrorY();
//        Area ar1_3 = ar1_2.mirrorX();
//        Area ar1_4 = ar1.mirrorX();
//
//        Area ar2_2 = ar2.mirrorY();
//        Area ar2_3 = ar2_2.mirrorX();
//        Area ar2_4 = ar2.mirrorX();
//
//        Area ar3_2 = ar3.mirrorY();
//        Area ar3_3 = ar3_2.mirrorX();
//        Area ar3_4 = ar3.mirrorX();

        // ELEMENTS
        // Quadrant 1
        Element ho1_2 = new Element("IY_HOLE1_2", ar2);
        Element ho1_3 = new Element("IY_HOLE1_3", ar3);
        Element ho1_4 = new Element("IY_HOLE1_4", ar4);

        // Quadrant 2
        Element ho2_2 = new Element("IY_HOLE2_2", ar2.mirrorY());
        Element ho2_3 = new Element("IY_HOLE2_3", ar3.mirrorY());
        Element ho2_4 = new Element("IY_HOLE2_4", ar4.mirrorY());

        // Quadrant 3
        Element ho3_2 = new Element("IY_HOLE3_2", ar2.mirrorY().mirrorX());
        Element ho3_3 = new Element("IY_HOLE3_3", ar3.mirrorY().mirrorX());
        Element ho3_4 = new Element("IY_HOLE3_4", ar4.mirrorY().mirrorX());

        // Quadrant 4
        Element ho4_2 = new Element("IY_HOLE4_2", ar2.mirrorX());
        Element ho4_3 = new Element("IY_HOLE4_3", ar3.mirrorX());
        Element ho4_4 = new Element("IY_HOLE4_4", ar4.mirrorX());

        // MIRRORED ELEMENTS

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho1_2,ho1_3,ho1_4};
        Element[] quad2 = {ho2_2,ho2_3,ho2_4};
        Element[] quad3 = {ho3_2,ho3_3,ho3_4};
        Element[] quad4 = {ho4_2,ho4_3,ho4_4};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;


//        return new Element[]{ho1, ho2, ho3, ho1_1, ho2_2, ho2_3, ho2_4, ho3_2, ho3_3, ho3_4};
//        return new Element[] {ho1, ho2, ho3};
    }

    private Element[] elem_wedge(){

        double deg2rad = Math.PI/180;
        double r1 = 0.03;
        double deltar = 0.0001;
        double r2a = 0.04525-deltar;
        double r2b = 0.04525+deltar;
        double r3 = 0.0605;

        Point kp103 = Point.ofPolar(r2a, 18.65934*deg2rad);
        Point kp104 = Point.ofPolar(r1, 24.18836*deg2rad);
        Point kp105 = Point.ofPolar(r1, 26.69416*deg2rad);
        Point kp106 = Point.ofPolar(r2a, 27.12536*deg2rad);

        Point kp109 = Point.ofPolar(r2a, 43.35745*deg2rad);
        Point kp110 = Point.ofPolar(r1, 47.69625*deg2rad);
        Point kp111 = Point.ofPolar(r1, 55.95846*deg2rad);
        Point kp112 = Point.ofPolar(r2a, 56.96191*deg2rad);

        Point kp114 = Point.ofPolar(r2a, 62.86518*deg2rad);
        Point kp115 = Point.ofPolar(r1, 63.577431*deg2rad);
        Point kp116 = Point.ofPolar(r1, 70.5744*deg2rad);
        Point kp117 = Point.ofPolar(r2a, 70.38467*deg2rad);

        Point kp122 = Point.ofPolar(r3, 24.62702*deg2rad);
        Point kp123 = Point.ofPolar(r2b, 28.388*deg2rad);
        Point kp126 = Point.ofPolar(r2b, 30.24952*deg2rad);
        Point kp127 = Point.ofPolar(r3, 30.92383*deg2rad);

        Line lnWed1_1 = Line.ofEndPoints(kp103,kp104);
        Arc lnWed1_2 = Arc.ofEndPointsRadius(kp105,kp104,r1);
        Line lnWed1_3 = Line.ofEndPoints(kp105,kp106);
        Arc lnWed1_4 = Arc.ofEndPointsRadius(kp106,kp103,r2a);

        Line lnWed2_1 = Line.ofEndPoints(kp109,kp110);
        Arc lnWed2_2 = Arc.ofEndPointsRadius(kp111,kp110,r1);
        Line lnWed2_3 = Line.ofEndPoints(kp111,kp112);
        Arc lnWed2_4 = Arc.ofEndPointsRadius(kp112,kp109,r2a);

        Line lnWed3_1 = Line.ofEndPoints(kp114,kp115);
        Arc lnWed3_2 = Arc.ofEndPointsRadius(kp116,kp115,r1);
        Line lnWed3_3 = Line.ofEndPoints(kp116,kp117);
        Arc lnWed3_4 = Arc.ofEndPointsRadius(kp117,kp114,r2a);

        Line lnWed4_1 = Line.ofEndPoints(kp122,kp123);
        Arc lnWed4_2 = Arc.ofEndPointsRadius(kp126,kp123,r2b);
        Line lnWed4_3 = Line.ofEndPoints(kp126,kp127);
        Arc lnWed4_4 = Arc.ofEndPointsRadius(kp127,kp122,r3);

        Area arTW1 = Area.ofHyperLines(new HyperLine[]{lnWed1_1,lnWed1_2,lnWed1_3,lnWed1_4});
        Area arTW2 = Area.ofHyperLines(new HyperLine[]{lnWed2_1,lnWed2_2,lnWed2_3,lnWed2_4});
        Area arTW3 = Area.ofHyperLines(new HyperLine[]{lnWed3_1,lnWed3_2,lnWed3_3,lnWed3_4});
        Area arTW4 = Area.ofHyperLines(new HyperLine[]{lnWed4_1,lnWed4_2,lnWed4_3,lnWed4_4});


        Point kp128 = Point.ofPolar(r3, 58.34695*deg2rad);
        Point kp129 = Point.ofPolar(r2b, 61.82651*deg2rad);
        Point kp130 = Point.ofPolar(r2b, 74.35943*deg2rad);
        Point kp131 = Point.ofPolar(r3, 73.65119*deg2rad);

        Line lnWed5_1 = Line.ofEndPoints(kp128,kp129);
        Arc lnWed5_2 = Arc.ofEndPointsRadius(kp130,kp129,r2b);
        Line lnWed5_3 = Line.ofEndPoints(kp130,kp131);
        Arc lnWed5_4 = Arc.ofEndPointsRadius(kp131,kp128,r3);

        Area arTW5 = Area.ofHyperLines(new HyperLine[]{lnWed5_1,lnWed5_2,lnWed5_3,lnWed5_4});

        // Quadrant 1
        Element el1_1 = new Element("Wed_El1_1", arTW1.translate(beamd,0));
        Element el1_2 = new Element("Wed_El1_2", arTW2.translate(beamd,0));
        Element el1_3 = new Element("Wed_El1_3", arTW3.translate(beamd,0));
        Element el1_4 = new Element("Wed_El1_4", arTW4.translate(beamd,0));
        Element el1_5 = new Element("Wed_El1_5", arTW5.translate(beamd,0));
        Element el1_6 = new Element("Wed_El1_6", arTW1.mirrorY().translate(beamd,0));
        Element el1_7 = new Element("Wed_El1_7", arTW2.mirrorY().translate(beamd,0));
        Element el1_8 = new Element("Wed_El1_8", arTW3.mirrorY().translate(beamd,0));
        Element el1_9 = new Element("Wed_El1_9", arTW4.mirrorY().translate(beamd,0));
        Element el1_10 = new Element("Wed_El1_10", arTW5.mirrorY().translate(beamd,0));

        // Quadrant 2
        Element el2_1 = new Element("Wed_El2_1", arTW1.translate(beamd,0).mirrorY());
        Element el2_2 = new Element("Wed_El2_2", arTW2.translate(beamd,0).mirrorY());
        Element el2_3 = new Element("Wed_El2_3", arTW3.translate(beamd,0).mirrorY());
        Element el2_4 = new Element("Wed_El2_4", arTW4.translate(beamd,0).mirrorY());
        Element el2_5 = new Element("Wed_El2_5", arTW5.translate(beamd,0).mirrorY());
        Element el2_6 = new Element("Wed_El2_6", arTW1.mirrorY().translate(beamd,0).mirrorY());
        Element el2_7 = new Element("Wed_El2_7", arTW2.mirrorY().translate(beamd,0).mirrorY());
        Element el2_8 = new Element("Wed_El2_8", arTW3.mirrorY().translate(beamd,0).mirrorY());
        Element el2_9 = new Element("Wed_El2_9", arTW4.mirrorY().translate(beamd,0).mirrorY());
        Element el2_10 = new Element("Wed_El2_10", arTW5.mirrorY().translate(beamd,0).mirrorY());

        // Quadrant 3
        Element el3_1 = new Element("Wed_El3_1", arTW1.translate(beamd,0).mirrorY().mirrorX());
        Element el3_2 = new Element("Wed_El3_2", arTW2.translate(beamd,0).mirrorY().mirrorX());
        Element el3_3 = new Element("Wed_El3_3", arTW3.translate(beamd,0).mirrorY().mirrorX());
        Element el3_4 = new Element("Wed_El3_4", arTW4.translate(beamd,0).mirrorY().mirrorX());
        Element el3_5 = new Element("Wed_El3_5", arTW5.translate(beamd,0).mirrorY().mirrorX());
        Element el3_6 = new Element("Wed_El3_6", arTW1.mirrorY().translate(beamd,0).mirrorY().mirrorX());
        Element el3_7 = new Element("Wed_El3_7", arTW2.mirrorY().translate(beamd,0).mirrorY().mirrorX());
        Element el3_8 = new Element("Wed_El3_8", arTW3.mirrorY().translate(beamd,0).mirrorY().mirrorX());
        Element el3_9 = new Element("Wed_El3_9", arTW4.mirrorY().translate(beamd,0).mirrorY().mirrorX());
        Element el3_10 = new Element("Wed_El3_10", arTW5.mirrorY().translate(beamd,0).mirrorY().mirrorX());

        // Quadrant 4
        Element el4_1 = new Element("Wed_El4_1", arTW1.translate(beamd,0).mirrorX());
        Element el4_2 = new Element("Wed_El4_2", arTW2.translate(beamd,0).mirrorX());
        Element el4_3 = new Element("Wed_El4_3", arTW3.translate(beamd,0).mirrorX());
        Element el4_4 = new Element("Wed_El4_4", arTW4.translate(beamd,0).mirrorX());
        Element el4_5 = new Element("Wed_El4_5", arTW5.translate(beamd,0).mirrorX());
        Element el4_6 = new Element("Wed_El4_6", arTW1.mirrorY().translate(beamd,0).mirrorX());
        Element el4_7 = new Element("Wed_El4_7", arTW2.mirrorY().translate(beamd,0).mirrorX());
        Element el4_8 = new Element("Wed_El4_8", arTW3.mirrorY().translate(beamd,0).mirrorX());
        Element el4_9 = new Element("Wed_El4_9", arTW4.mirrorY().translate(beamd,0).mirrorX());
        Element el4_10 = new Element("Wed_El4_10", arTW5.mirrorY().translate(beamd,0).mirrorX());

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_1,el1_2,el1_3,el1_4,el1_5,el1_6,el1_7,el1_8,el1_9,el1_10};
        Element[] quad2 = {el2_1,el2_2,el2_3,el2_4,el2_5,el2_6,el2_7,el2_8,el2_9,el2_10};
        Element[] quad3 = {el3_1,el3_2,el3_3,el3_4,el3_5,el3_6,el3_7,el3_8,el3_9,el3_10};
        Element[] quad4 = {el4_1,el4_2,el4_3,el4_4,el4_5,el4_6,el4_7,el4_8,el4_9,el4_10};
        Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return elementsToBuild;
    }



}
