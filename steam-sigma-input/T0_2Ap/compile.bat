echo Compiling java file into a class file
"C:\Program Files\COMSOL\COMSOL53a\Multiphysics\bin\win64\comsolcompile.exe" -jdkroot "C:\Program Files\Java\jdk1.8.0_91" "D:\CERNbox\SWAN_projects\steam-notebooks\steam-sigma-input\T0_2Ap\T0_2ApMagnetModel.java"
echo Saving the class file as an mph file
"C:\Program Files\COMSOL\COMSOL53a\Multiphysics\bin\win64\comsolbatch.exe" -inputfile "D:\CERNbox\SWAN_projects\steam-notebooks\steam-sigma-input\T0_2Ap\T0_2ApMagnetModel.class" -outputfile "D:\CERNbox\SWAN_projects\steam-notebooks\steam-sigma-input\T0_2Ap\T0_2ApMagnetModel.mph"
timeout /t -1