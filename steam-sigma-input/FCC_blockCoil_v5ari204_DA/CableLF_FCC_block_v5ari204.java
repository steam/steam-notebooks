package input.FCC.BlockCoil_v5ari204;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableLF_FCC_block_v5ari204 extends Cable {

    public CableLF_FCC_block_v5ari204() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        // ROXIE cable ECC111HF

        this.label = "cable_LF_FCC";

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];
        //Filament
        this.dFilament = 50e-6; // [m];
        //Strand
        this.dstrand = 0.7e-3; // [m]; previous 0.705e-3
        double CuScRatio = 2; // previous 2.3
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];
        //Cable
        this.wBare = 12.6e-3; // [m]; previous 13.05e-3
        this.hInBare = 1.27e-3; // [m]; previous 1.25e-3
        this.hOutBare = 1.27e-3; // [m]; previous 1.25e-3
        this.noOfStrands = 34;  // previous 35
        this.noOfStrandsPerLayer = 17;
        this.noOfLayers = 2;
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];

        this.thetaTpStrand = 15*Math.PI/180;
        this.lTpStrand = 2*(wBare-dstrand)/Math.tan(thetaTpStrand);

        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }


}
