package input.FCC.BlockCoil_v5ari204;

import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by mprioli on 07/05/2018.
 */
public class Magnet_FCC_blockCoil_v5ari204_DA {

    private final Domain[] domains;

    public Magnet_FCC_blockCoil_v5ari204_DA() {

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, iron_yoke()),
                new HoleDomain("holes_yoke", MatDatabase.MAT_AIR, holes_yoke())
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r1 = 1.5;
        double angle = Math.PI/2;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r1, 0);
        Point kp2 = Point.ofCartesian(0, r1);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofPointCenterAngle(kp1, kpc, angle);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);

        return new Element[]{el1};
    }

    public Element[] airFarField() {
        double r1 = 1.5;
        double r2 = r1 * 1.05;
        double angle = Math.PI/2;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r1, 0);
        Point kp2 = Point.ofCartesian(0, r1);

        Point kp1_far = Point.ofCartesian(r2, 0);
        Point kp2_far = Point.ofCartesian(0, r2);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofPointCenterAngle(kp1_far, kpc, angle);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofPointCenterAngle(kp2, kpc, -angle);

        // AREAS
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        // ELEMENTS
        Element el1_far = new Element("FAR_El1", ar1_far);

        return new Element[]{el1_far};
    }

    public Coil coil() {

//        double dyMechanical = 0.5e-3; // No gaps among blocks for the COMSOL - ANSYS coupling
        double dyMechanical = 0;      // Original ROXIE model with gaps among the blocks

        double dyReducedShim = 70e-6; // Magnet version v5ari204 has a midplane shim reduced by 70 um with respect to v2ari192
//        double dyMechanical = 0;      // The same coil of version v2ari192 is obtained (inter-beam distance is different)

        double interBeamDistance = 204e-3; // Magnet version v5ari204
//        double interBeamDistance = 194e-3; // Magnet version v2ari192



        Point kp11 = Point.ofCartesian(38.40e-3, 15.25e-3);
        Point kp12 = Point.ofCartesian(26.90e-3, 15.25e-3);
        Point kp13 = Point.ofCartesian(26.90e-3, 02.35e-3 - dyMechanical);
        Point kp14 = Point.ofCartesian(38.40e-3, 02.35e-3 - dyMechanical);

        Point kp21 = Point.ofCartesian(38.40e-3, 28.65e-3);
        Point kp22 = Point.ofCartesian(26.90e-3, 28.65e-3);
        Point kp23 = Point.ofCartesian(26.90e-3, 15.75e-3 - dyMechanical);
        Point kp24 = Point.ofCartesian(38.40e-3, 15.75e-3 - dyMechanical);

        Point kp31 = Point.ofCartesian(36.83e-3, 42.05e-3);
        Point kp32 = Point.ofCartesian(13.83e-3, 42.05e-3);
        Point kp33 = Point.ofCartesian(13.83e-3, 29.15e-3 - dyMechanical);
        Point kp34 = Point.ofCartesian(36.83e-3, 29.15e-3 - dyMechanical);

        Point kp41 = Point.ofCartesian(36.83e-3, 55.45e-3);
        Point kp42 = Point.ofCartesian(13.83e-3, 55.45e-3);
        Point kp43 = Point.ofCartesian(13.83e-3, 42.55e-3 - dyMechanical);
        Point kp44 = Point.ofCartesian(36.83e-3, 42.55e-3 - dyMechanical);

        Point kp51 = Point.ofCartesian(71.37e-3, 15.25e-3);
        Point kp52 = Point.ofCartesian(38.40e-3, 15.25e-3);
        Point kp53 = Point.ofCartesian(38.40e-3, 02.35e-3 - dyMechanical);
        Point kp54 = Point.ofCartesian(71.37e-3, 02.35e-3 - dyMechanical);

        Point kp61 = Point.ofCartesian(71.37e-3, 28.65e-3);
        Point kp62 = Point.ofCartesian(38.40e-3, 28.65e-3);
        Point kp63 = Point.ofCartesian(38.40e-3, 15.75e-3 - dyMechanical);
        Point kp64 = Point.ofCartesian(71.37e-3, 15.75e-3 - dyMechanical);

        Point kp71 = Point.ofCartesian(71.37e-3, 42.05e-3);
        Point kp72 = Point.ofCartesian(36.83e-3, 42.05e-3);
        Point kp73 = Point.ofCartesian(36.83e-3, 29.15e-3 - dyMechanical);
        Point kp74 = Point.ofCartesian(71.37e-3, 29.15e-3 - dyMechanical);

        Point kp81 = Point.ofCartesian(71.37e-3, 55.45e-3);
        Point kp82 = Point.ofCartesian(36.83e-3, 55.45e-3);
        Point kp83 = Point.ofCartesian(36.83e-3, 42.55e-3 - dyMechanical);
        Point kp84 = Point.ofCartesian(71.37e-3, 42.55e-3 - dyMechanical);


        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Line ln31 = Line.ofEndPoints(kp32, kp31);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Line ln33 = Line.ofEndPoints(kp33, kp34);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Line ln41 = Line.ofEndPoints(kp42, kp41);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Line ln43 = Line.ofEndPoints(kp43, kp44);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Line ln51 = Line.ofEndPoints(kp52, kp51);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Line ln53 = Line.ofEndPoints(kp53, kp54);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Line ln61 = Line.ofEndPoints(kp62, kp61);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Line ln63 = Line.ofEndPoints(kp63, kp64);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Line ln71 = Line.ofEndPoints(kp72, kp71);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Line ln73 = Line.ofEndPoints(kp73, kp74);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Line ln81 = Line.ofEndPoints(kp82, kp81);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Line ln83 = Line.ofEndPoints(kp83, kp84);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});

        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();

        Cable cableHF = new CableHF_FCC_block_v5ari204();
        Cable cableLF = new CableLF_FCC_block_v5ari204();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 5, 5, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1, -1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1, -1}, 10, 10, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1, -1}, 10, 10, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n}, new int[]{+1, -1}, 21, 21, cableLF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n}, new int[]{+1, -1}, 21, 21, cableLF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n}, new int[]{+1, -1}, 22, 22, cableLF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n}, new int[]{+1, -1}, 22, 22, cableLF);

//        Winding w11 = Winding.ofAreas(new Area[]{ha11p}, new int[]{+1}, 5, 5, cableHF);
//        Winding w12 = Winding.ofAreas(new Area[]{ha12p}, new int[]{+1}, 5, 5, cableHF);
//        Winding w13 = Winding.ofAreas(new Area[]{ha13p}, new int[]{+1}, 10, 10, cableHF);
//        Winding w14 = Winding.ofAreas(new Area[]{ha14p}, new int[]{+1}, 10, 10, cableHF);
//        Winding w15 = Winding.ofAreas(new Area[]{ha15p}, new int[]{+1}, 18, 18, cableLF);
//        Winding w16 = Winding.ofAreas(new Area[]{ha16p}, new int[]{+1}, 18, 18, cableLF);
//        Winding w17 = Winding.ofAreas(new Area[]{ha17p}, new int[]{+1}, 19, 19, cableLF);
//        Winding w18 = Winding.ofAreas(new Area[]{ha18p}, new int[]{+1}, 19, 19, cableLF);

        w11 = w11.reverseWindingDirection();
        w14 = w14.reverseWindingDirection();
        w15 = w15.reverseWindingDirection();
        w18 = w18.reverseWindingDirection();

        Winding[] windings_p1 = {w15, w11, w12, w16, w18, w14, w13, w17};

        Pole p1 = Pole.ofWindings(windings_p1).translate(0, -dyReducedShim);
        Pole[] poles = {p1};

        Coil coil = Coil.ofPoles(poles);
        coil = coil.translate(interBeamDistance/2.0, 0);

        return coil;
    }

    public Element[] iron_yoke() {


        //file generated by Xhermes 10.0
        //HyperMesh;
        //COMMENTS
        //''

        //VARIABLES
        double xbeam          = 102e-3;
        double xinhpad        = 187e-3;
        double xinhpad_left   = 2*xbeam-xinhpad;
        double hpad_th        = 40e-3;
        double xouhpad        = xinhpad+hpad_th;
        double xthhpad        = xouhpad-xinhpad;
        double slot           = 3e-3;
        double yhpadslot      = 75e-3;
        double rhpad          = xouhpad-xinhpad;
        double yinvpad        = 59e-3;
        double youvpad        = 112e-3;
        double ythvpad        = youvpad-yinvpad;
        double xthvpad        = 5e-3;
        double xnickpad1      = xbeam+20e-3;
        double xnickpad1_left = 2*xbeam-xnickpad1;
        double xnickpad2      = 25e-3;
        double ynickpad       = 25e-3;
        double xvshim         = xinhpad-2.5e-3;
        double xvshim_left    = 2*xbeam-xvshim;
        double xpatcent       = xvshim_left-2.5e-3;
        double xinyoke        = xouhpad+5e-3;
        double routyoke       = 570e-3/2;
        double yinyoke        = youvpad+5e-3;
        double rhole1         = 53e-3;
        double yhole1         = 225e-3;
//        double rhole2         = 16e-3;
//        double xhole2         = 120e-3;
//        double yhole2         = 200e-3;

        //NODES
        Point kp1  = Point.ofCartesian(xinhpad,0);
        Point kp2  = Point.ofCartesian(xouhpad-slot,0);
        Point kp3  = Point.ofCartesian(xouhpad-slot,yhpadslot);
        Point kp4  = Point.ofCartesian(xouhpad,yhpadslot);
        Point kp5  = Point.ofCartesian(xouhpad,youvpad);
        Point kp6  = Point.ofCartesian(xinhpad,youvpad);
        Point kp7  = Point.ofCartesian(xvshim-xthvpad,youvpad-slot);
        Point kp8  = Point.ofCartesian(xvshim-xthvpad,youvpad);
        Point kp9  = Point.ofCartesian(xvshim,youvpad);
        Point kp10 = Point.ofCartesian(xvshim,yinvpad);
        Point kp11 = Point.ofCartesian(xnickpad1+xnickpad2,yinvpad);
        Point kp12 = Point.ofCartesian(xnickpad1,yinvpad+ynickpad);
        Point kp13 = Point.ofCartesian(xnickpad1_left,yinvpad+ynickpad);
        Point kp14 = Point.ofCartesian(xnickpad1_left-xnickpad2,yinvpad);
        Point kp15 = Point.ofCartesian(xvshim_left,yinvpad);
        Point kp16 = Point.ofCartesian(xvshim_left,youvpad);
        Point kp17 = Point.ofCartesian(xvshim_left+xthvpad,youvpad);
        Point kp18 = Point.ofCartesian(xvshim_left+xthvpad,youvpad-slot);
        Point kp19 = Point.ofCartesian(xinyoke,0);
        Point kp20 = Point.ofCartesian(xinyoke,Math.sqrt(routyoke*routyoke-xinyoke*xinyoke));
        Point kp21 = Point.ofCartesian(routyoke,0);
        Point kp22 = Point.ofCartesian(0.95*routyoke,Math.sqrt(routyoke*routyoke-0.95*0.95*routyoke*routyoke));
        Point kp23 = Point.ofCartesian(xouhpad,yinyoke);
        Point kp24 = Point.ofCartesian(xouhpad,Math.sqrt(routyoke*routyoke-xouhpad*xouhpad));
        Point kp25 = Point.ofCartesian(0,routyoke);
        Point kp26 = Point.ofCartesian(0.05*routyoke,Math.sqrt(routyoke*routyoke-0.05*0.05*routyoke*routyoke));
        Point kp27 = Point.ofCartesian(0,yhole1+rhole1);
        Point kp28 = Point.ofCartesian(0,yhole1-rhole1);
        Point kp29 = Point.ofCartesian(rhole1,yhole1);
        Point kp30 = Point.ofCartesian(0,yinyoke);
//        Point kp31 = Point.ofCartesian(xhole2-rhole2,yhole2);
//        Point kp32 = Point.ofCartesian(xhole2,yhole2-rhole2);
//        Point kp33 = Point.ofCartesian(xhole2+rhole2,yhole2);
//        Point kp34 = Point.ofCartesian(xhole2,yhole2+rhole2);

        //LINES
        Line ln1  = Line.ofEndPoints(kp1,kp2);
        Line ln2  = Line.ofEndPoints(kp2,kp3);
        Line ln3  = Line.ofEndPoints(kp3,kp4);
        Line ln4  = Line.ofEndPoints(kp4,kp5);
        Line ln5  = Line.ofEndPoints(kp5,kp6);
        Line ln6  = Line.ofEndPoints(kp6,kp1);
        Line ln7  = Line.ofEndPoints(kp7,kp8);
        Line ln8  = Line.ofEndPoints(kp8,kp9);
        Line ln9  = Line.ofEndPoints(kp9,kp10);
        Line ln10 = Line.ofEndPoints(kp10,kp11);
        Line ln11 = Line.ofEndPoints(kp11,kp12);
        Line ln12 = Line.ofEndPoints(kp12,kp13);
        Line ln13 = Line.ofEndPoints(kp13,kp14);
        Line ln14 = Line.ofEndPoints(kp14,kp15);
        Line ln15 = Line.ofEndPoints(kp15,kp16);
        Line ln16 = Line.ofEndPoints(kp16,kp17);
        Line ln17 = Line.ofEndPoints(kp17,kp18);
        Line ln18 = Line.ofEndPoints(kp18,kp7);
        Line ln19 = Line.ofEndPoints(kp19,kp21);
        Arc ln20 = Arc.ofThreePoints(kp21,kp22,kp20);
        Line ln21 = Line.ofEndPoints(kp20,kp19);
        Line ln22 = Line.ofEndPoints(kp23,kp24);
        Arc ln23 = Arc.ofThreePoints(kp24,kp26,kp25);
        Line ln24 = Line.ofEndPoints(kp25,kp27);
        Arc ln25 = Arc.ofThreePoints(kp27,kp29,kp28);
        Line ln26 = Line.ofEndPoints(kp28,kp30);
        Line ln27 = Line.ofEndPoints(kp30,kp23);
//        Arc ln28 = Arc.ofThreePoints(kp31,kp33,kp32);
//        Arc ln29 = Arc.ofThreePoints(kp33,kp31,kp34);

        //AREAS
//        HyperArea ar5 = Area.ofHyperLines(new HyperLine[]{ln29,ln28});
        HyperArea ar1 = Area.ofHyperLines(new HyperLine[]{ln1,ln2,ln3,ln4,ln5,ln6});
        HyperArea ar2 = Area.ofHyperLines(new HyperLine[]{ln14,ln13,ln12,ln11,ln10,ln9,ln8,ln7,ln18,ln17,ln16,ln15});
        HyperArea ar3 = Area.ofHyperLines(new HyperLine[]{ln19,ln20,ln21});
        HyperArea ar4 = Area.ofHyperLines(new HyperLine[]{ln27,ln22,ln23,ln24,ln25,ln26});


        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);
//        Element el5 = new Element("IY_El5", ar5); // Moved to holes!


        return new Element[]{el1, el2, el3, el4};

        //HOLES
//        HyperHoleOf(ar5,ar4);
    }

    public Element[] holes_yoke() {

        double rhole2 = 16e-3;
        double xhole2 = 120e-3;
        double yhole2 = 200e-3;

        Point kpc = Point.ofCartesian(xhole2, yhole2);

        Circumference hole2 = Circumference.ofCenterRadius(kpc, rhole2);

        HyperArea ar5 = Area.ofHyperLines(new HyperLine[]{hole2});

        Element el5 = new Element("IY_El4_HOLE1", ar5);

        return new Element[]{el5};
    }
}

