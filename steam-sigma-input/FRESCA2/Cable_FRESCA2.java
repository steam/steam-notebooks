package input.Others.FRESCA2;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by Alejandro Fernandez on 05/09/2016. TALES #492 and roxie_vs10.cadata as references
 */
public class Cable_FRESCA2 extends Cable {

    public Cable_FRESCA2() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_FRESCA2";

        //Insulation
        this.wInsulNarrow = 0.16e-3; // [m];
        this.wInsulWide = 0.16e-3; // [m];
        //Filament
        this.dFilament = 44.0e-6; // [m];   ???
        //Strand
        this.dstrand = 1.0e-3; // [m];
        this.fracCu = 1.28 /(1+1.28);
        this.fracSc = 1/(1+1.28);
        this.RRR = 320;
        this.TupRRR = 293; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 10e-6; // [ohm];
        this.Ra = 10e-6; // ; // [ohm];
        this.fRhoEff = 1.0; // [1];  ???
        this.lTp = 25e-3; // [m];   ???
        //Cable insulated
        this.wBare = 21.48e-3; // [m];
        this.hInBare = 1.9e-3; // [m];
        this.hOutBare = 1.9e-3; // [m];
        this.noOfStrands = 40;
        this.noOfStrandsPerLayer = 20;
        this.noOfLayers = 2;
        this.lTpStrand = 0.12; // [m];
        this.wCore = 0; // [m];                                                 Added by ES
        this.hCore = 0; // [m];                                                 Added by ES
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2)); //      Added by ES
        this.C1 = 0; // [A]; from Roxie. TALES 1e40
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]                                      Added by ES
        this.fractFillOuterVoids = 1; //[-]                                     Added by ES

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_NIST;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID; //                            Added by ES
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;


//        this.tauIFCCFit = TauIFCCFitEnum.tauIFCCBortot;
//        this.rho0IFCC = 1.13338E-10; // [ohm/T];
//        this.rho1IFCC = 4.2e-11; // [ohm/T];
    }


}