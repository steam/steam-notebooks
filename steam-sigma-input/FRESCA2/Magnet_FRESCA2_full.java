package input.Others.FRESCA2;

import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 21/02/2018.
 */

public class Magnet_FRESCA2_full {

    private final Domain[] domains;

    // VARIABLES

    // GENERAL VARIABLES

    // Design variables added by Per HAGEN (for easier to deal with warm measurements)(Taken from Magnet_MB.java)
    double mm = 0.001;
    double deg = Math.PI / 180;
//    //shrink  = 0.998;			// yoke/insert shrink
//    double DCONT = 0.0;
//    double shrink = 1.0 * (1 - DCONT);
//    //cshrink = 0.9973;			// collar shrink
//    double DCCONT = 0.0;
//    double cshrink = 1.0 * (1 - DCCONT);
//    //csshrink = 0.996;			// coil sheet shrink
//    double DCSCONT = 0.0;
//    double csshrink = 1.0 * (1 - DCSCONT);
//    double BEAMD = 97.00;
//    double beamd = BEAMD * mm;

    // YOKE SPECIFIC VARIABLES
    double t_cbl = 2.22*mm;
    double n1 = 36;
    double n3 = 42;
    double x1 = 137.92*mm;
    double y1 = 3.5*mm;
    double y5 = 93.20*mm;
    double y3 = 0.5*(y1+y5);
    double x1l = x1-n1*t_cbl;
    double x3l = x1-n3*t_cbl;
    double r_fillet_top_pole = 8*mm;
    double t_top_pole_nose = 6*mm;
    double x_top_pole_corner = 12.5*mm;
    double y_top_pole_bottom = 62.5*mm;
    double angle_poles = Math.atan((y_top_pole_bottom-y3)/(x3l-t_top_pole_nose-x_top_pole_corner));
    double w_vpad = 296*mm;
    double t_top_plate = 18*mm;
    double t_vpad = 141.8*mm;
    double r_yoke = 450*mm;
    double yoke_gap = 5.*mm;
    double r_fillet_yoke = 20*mm;
    double x_yoke_hole = 62.5*mm;
    double y_yoke_hole = 300*mm;
    double dia_yoke_hole = 60*mm;
    double t_key = 10*mm;
    double w_key = 35*mm;
    double w_bladd = 75*mm;

    // extra geometrical parameters
    double gap_coil_iron_top_pole = 0.75*mm;
    double gap_coil_iron_vpad = 0.75*mm;

    public Magnet_FRESCA2_full() {

        domains = new Domain[]{

                new AirDomain("AIR1", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("AIR2", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("COIL", MatDatabase.MAT_COIL, coil()),
                new IronDomain("IY1", MatDatabase.MAT_IRON1, ironYoke1()),
                new IronDomain("IY2", MatDatabase.MAT_IRON2, ironYoke2()),
                new HoleDomain("HY1", MatDatabase.MAT_AIR, holesYoke()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    // Cryostat model
    public Element[] air() {
        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        Area ar2 = ar1.mirrorX();
        Area ar2_far = ar1_far.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);

        return new Element[]{el1, el2};
    }

    private Element[] airFarField() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_out = Point.ofCartesian(r * 1.05, 0);
        Point kp4_out = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1 = Line.ofEndPoints(kp1, kp1_out);
        Arc ln2 = Arc.ofEndPointsCenter(kp1_out, kp4_out, kpc);
        Line ln3 = Line.ofEndPoints(kp4_out, kp2);
        Arc ln4 = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("FAR_El1", ar1);
        Element el2 = new Element("FAR_El2", ar2);
        Element el3 = new Element("FAR_El3", ar3);
        Element el4 = new Element("FAR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
//        Element[] quad1 = {el1};
//        Element[] quad2 = {el2};
//        Element[] quad3 = {el3};
//        Element[] quad4 = {el4};
        Element[] elementsToBuild = new Element[]{el1,el4};
        return elementsToBuild;
    }

    public Coil coil() {
        Point kp11 = Point.ofCartesian(138.42e-3, 25.3e-3);
        Point kp12 = Point.ofCartesian(58.5e-3, 25.3e-3);
        Point kp13 = Point.ofCartesian(58.5e-3, 3.5e-3);
        Point kp14 = Point.ofCartesian(138.42e-3, 3.5e-3);

        Point kp21 = Point.ofCartesian(138.42e-3, 47.6e-3);
        Point kp22 = Point.ofCartesian(58.5e-3, 47.6e-3);
        Point kp23 = Point.ofCartesian(58.5e-3, 25.8e-3);
        Point kp24 = Point.ofCartesian(138.42e-3, 25.8e-3);

        Point kp31 = Point.ofCartesian(138.42e-3, 70.9e-3);
        Point kp32 = Point.ofCartesian(45.18e-3, 70.9e-3);
        Point kp33 = Point.ofCartesian(45.18e-3, 49.1e-3);
        Point kp34 = Point.ofCartesian(138.42e-3, 49.1e-3);

        Point kp41 = Point.ofCartesian(138.42e-3, 93.2e-3);
        Point kp42 = Point.ofCartesian(45.18e-3, 93.2e-3);
        Point kp43 = Point.ofCartesian(45.18e-3, 71.4e-3);
        Point kp44 = Point.ofCartesian(138.42e-3, 71.4e-3);

        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Line ln31 = Line.ofEndPoints(kp32, kp31);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Line ln33 = Line.ofEndPoints(kp33, kp34);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Line ln41 = Line.ofEndPoints(kp42, kp41);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Line ln43 = Line.ofEndPoints(kp43, kp44);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});

        Winding w11_R = Winding.ofAreas(new Area[]{ha11p}, new int[]{+1,-1}, 36, 36, new Cable_FRESCA2());
        Winding w12_R = Winding.ofAreas(new Area[]{ha12p}, new int[]{+1,-1}, 36, 36, new Cable_FRESCA2());
        Winding w13_R = Winding.ofAreas(new Area[]{ha13p}, new int[]{+1,-1}, 42, 42, new Cable_FRESCA2());
        Winding w14_R = Winding.ofAreas(new Area[]{ha14p}, new int[]{+1,-1}, 42, 42, new Cable_FRESCA2());

        w11_R = w11_R.reverseWindingDirection();
        w12_R = w12_R.reverseWindingDirection();
        w13_R = w13_R.reverseWindingDirection();
        w14_R = w14_R.reverseWindingDirection();

        Pole p1 = Pole.ofWindings(new Winding[]{w11_R, w12_R, w13_R, w14_R});
        Pole p2 = p1.mirrorX();

        return Coil.ofPoles(new Pole[]{p1, p2});
    }

    public Element[] ironYoke1() {

        // top pole
        Point kp_251 = Point.ofCartesian(0.0,y_top_pole_bottom);
        Point kp_252 = Point.ofCartesian(x_top_pole_corner,y_top_pole_bottom);
        Point kp_253 = Point.ofCartesian(x_top_pole_corner+r_fillet_top_pole*Math.sin(2*angle_poles),y_top_pole_bottom-r_fillet_top_pole+r_fillet_top_pole*Math.cos(2*angle_poles));
        Point kp_254 = Point.ofCartesian(x3l-t_top_pole_nose,y3);
        Point kp_255 = Point.ofCartesian(x3l-gap_coil_iron_top_pole,y3);
        Point kp_256 = Point.ofCartesian(x3l-gap_coil_iron_top_pole,y5);
        Point kp_257 = Point.ofCartesian(0.0,y5);

        Line ln_251 = Line.ofEndPoints(kp_251,kp_252);
        Arc ln_252 = Arc.ofEndPointsRadius(kp_252,kp_253,r_fillet_top_pole);
        Line ln_253 = Line.ofEndPoints(kp_253,kp_254);
        Line ln_254 = Line.ofEndPoints(kp_254,kp_255);
        Line ln_255 = Line.ofEndPoints(kp_255,kp_256);
        Line ln_256 = Line.ofEndPoints(kp_256,kp_257);
        Line ln_257 = Line.ofEndPoints(kp_257,kp_251);

        Area ar_251 = Area.ofHyperLines(new HyperLine[]{ln_251,ln_252,ln_253,ln_254,ln_255,ln_256,ln_257});
        Area ar_251_mirror_x = ar_251.mirrorX();

        Element el1_251 = new Element("IY1_El1", ar_251);
        Element el2_251 = new Element("IY1_El2", ar_251_mirror_x);
        return new Element[]{el1_251, el2_251};

    }

    public Element[] ironYoke2() {

        // vertical pad
        Point kp_701 = Point.ofCartesian(0.0,y5+t_top_plate);
        Point kp_702 = Point.ofCartesian(0.5*w_vpad,y5+t_top_plate);
        Point kp_703 = Point.ofCartesian(0.5*w_vpad,y5+t_vpad);
        Point kp_704 = Point.ofCartesian(0.0,y5+t_vpad);

        Line ln_701 = Line.ofEndPoints(kp_701,kp_702);
        Line ln_702 = Line.ofEndPoints(kp_702,kp_703);
        Line ln_703 = Line.ofEndPoints(kp_703,kp_704);
        Line ln_704 = Line.ofEndPoints(kp_704,kp_701);

        Area ar_701 = Area.ofHyperLines(new HyperLine[]{ln_701,ln_702,ln_703,ln_704});
        Area ar_701_mirror_x = ar_701.mirrorX();

        Element el1_701 = new Element("IY2_El1", ar_701);
        Element el2_701 = new Element("IY2_El2", ar_701_mirror_x);

        // yoke
        Point kp_1001 = Point.ofCartesian(216.0*mm,0.0);
        Point kp_1002 = Point.ofCartesian(r_yoke,0.);
        Point kp_1003 = Point.ofCartesian(0.5*yoke_gap,Math.sqrt(r_yoke*r_yoke-0.25*yoke_gap*yoke_gap));
        Point kp_1004 = Point.ofCartesian(0.5*yoke_gap,y5+t_vpad+0.5*t_key);
        Point kp_1005 = Point.ofCartesian(216.0*mm-r_fillet_yoke,y5+t_vpad+0.5*t_key);
        Point kp_1006 = Point.ofCartesian(216.0*mm,y5+t_vpad+0.5*t_key-r_fillet_yoke);

        Line ln_1001 = Line.ofEndPoints(kp_1001,kp_1002);
        Arc ln_1002 = Arc.ofEndPointsRadius(kp_1003,kp_1002,r_yoke);
        Line ln_1003 = Line.ofEndPoints(kp_1003,kp_1004);
        Line ln_1004 = Line.ofEndPoints(kp_1004,kp_1005);
        Arc ln_1005 = Arc.ofEndPointsRadius(kp_1005,kp_1006,r_fillet_yoke);
        Line ln_1006 = Line.ofEndPoints(kp_1006,kp_1001);

        Area ar_1001 = Area.ofHyperLines(new HyperLine[]{ln_1001,ln_1002,ln_1003,ln_1004,ln_1005,ln_1006});
        Area ar_1001_mirror_x = ar_1001.mirrorX();

        Element el1_1001 = new Element("IY2_El3", ar_1001);
        Element el2_1001 = new Element("IY2_El4", ar_1001_mirror_x);

        return new Element[]{el1_701,el1_1001,el2_701,el2_1001};

    }
    // HOLES

    public Element[] holesYoke() {
        Point kpyokehole = Point.ofCartesian(x_yoke_hole, y_yoke_hole);
        Circumference lnyokehole  = Circumference.ofCenterRadius(kpyokehole,0.5*dia_yoke_hole);
        Area aryokehole  = Area.ofHyperLines(new HyperLine[]{lnyokehole});
        Area aryokehole_mirror_x  = aryokehole.mirrorX();
        Element ho1_1 = new Element("IY1_HOLE1_1", aryokehole);
        Element ho1_2 = new Element("IY1_HOLE1_2", aryokehole_mirror_x);
        return new Element[]{ho1_1, ho1_2};
    }
}
