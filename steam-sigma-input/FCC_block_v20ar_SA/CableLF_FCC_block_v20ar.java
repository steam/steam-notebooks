package input.FCC.BlockCoil_v20ar;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableLF_FCC_block_v20ar extends Cable {

    public CableLF_FCC_block_v20ar() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_LF_FCC";

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];
        //Filament
        this.dFilament = 50e-6; // [m];
        //Strand
        this.dstrand = 0.705e-3; // [m]; previous 0.7
        double CuScRatio = 2.3; // prev 1.05
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = (1/CuScRatio)/(1+(1/CuScRatio));
        this.RRR = 150;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K]; prev 4.2
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];
        //Cable
        this.wBare = 13.05e-3; // [m]; previous 13.85e-3
        this.hInBare = 1.25e-3; // [m];
        this.hOutBare = 1.25e-3; // [m];
        this.noOfStrands = 35;  // previous 37
        this.noOfStrandsPerLayer = 17;
        this.noOfLayers = 2;
        this.lTpStrand = 0.180; // [m]; strand transposition pitch. prev 0.127
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }


}

