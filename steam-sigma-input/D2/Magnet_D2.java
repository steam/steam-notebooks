package input.HL_LHC.D2;

import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

public class Magnet_D2 {


    private final Domain[] domains;

    // VARIABLES
    private double x0 = 0.0e-3;
    private double xaxisel = 307e-3;
    private double yaxisel = 275e-3;
    private double xcollint = 199.00e-3;
    private double ycollint = 105.5e-3;
    private double rcryo = 307e-3;
    private double gap = 0.0e-3;
    private double rhole1 = 30e-3;
    private double rhole2 = 11.5e-3;
    private double rhole3 = 9e-3;
    private double rhole4 = 13e-3;
    private double rhole5 = 15.5e-3;
    private double deltayIron = 0.0e-3;
    private double Rcurv = 56e-3;
    private double deltaXsensHole4 = 0e-3;
    private double deltaYsensHole4 = 0e-3;
    private double xhole5 = 230e-3;
    private double yhole5 = 50.0e-3;
    private double xhole4 = 210e-3;
    private double yhole4 = 117.0e-3;
    private double xhole3 = 180e-3;
    private double yhole3 = 200e-3;
    private double xhole2 = 95e-3;
    private double yhole2 = 210e-3;
    private double xtooth = 3e-3;
    private double ytooth = 2.5e-3;
    private double stooth = 0.5e-3;
    private double xcoll = 94.35e-3;
    private double ycoll = 94.35e-3;
    private double rcoll = 69e-3;
    private double rcurvedge = 50e-3;
    private double rcurvel = 6.5e-3;
    private double dbeam = 94e-3;
    private double thetadx = 72.8439e-3;
    private double thetasx = 73.4685e-3;


    public Magnet_D2() {

        domains = new Domain[]{

                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new AirDomain("collar", MatDatabase.MAT_AIR, iron_collar()),
                new AirDomain("nose", MatDatabase.MAT_AIR, iron_nose()),
                new IronDomain("iron", MatDatabase.MAT_IRON2, iron_outer()),
                new HoleDomain("holes", MatDatabase.MAT_AIR, holes()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1};
        Element[] quad2 = {el2};
        Element[] quad3 = {el3};
        Element[] quad4 = {el4};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
        return new Element[]{el1, el4};
    }

    public Element[] airFarField() {
        // POINTS

        double r = 1;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
        Area ar2_far = ar1_far.mirrorY();
        Area ar3_far = ar2_far.mirrorX();
        Area ar4_far = ar1_far.mirrorX();

        Element el1_far = new Element("FAR_El1", ar1_far);
        Element el2_far = new Element("FAR_El2", ar2_far);
        Element el3_far = new Element("FAR_El3", ar3_far);
        Element el4_far = new Element("FAR_El4", ar4_far);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_far};
        Element[] quad2 = {el2_far};
        Element[] quad3 = {el3_far};
        Element[] quad4 = {el4_far};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
        return new Element[]{el1_far,el4_far};
    }

    public Coil coil() {
        Point kp0 = Point.ofCartesian(94e-3, 0);

        Point kp11_r = Point.ofCartesian(140.8790e-3, 24.4426e-3);
        Point kp12_r = Point.ofCartesian(146.4694e-3, 1.7928e-3);
        Point kp13_r = Point.ofCartesian(161.8198e-3, 1.7928e-3);
        Point kp14_r = Point.ofCartesian(155.8096e-3, 28.0083e-3);

        Point kp21_r = Point.ofCartesian(133.6651e-3, 34.3937e-3);
        Point kp22_r = Point.ofCartesian(139.2261e-3, 26.9245e-3);
        Point kp23_r = Point.ofCartesian(151.6274e-3, 35.9713e-3);
        Point kp24_r = Point.ofCartesian(145.1648e-3, 44.5620e-3);

        Point kp31_r = Point.ofCartesian(128.0399e-3, 40.1142e-3);
        Point kp32_r = Point.ofCartesian(132.4523e-3, 35.7446e-3);
        Point kp33_r = Point.ofCartesian(143.9272e-3, 45.9407e-3);
        Point kp34_r = Point.ofCartesian(138.8555e-3, 51.0073e-3);

        Point kp41_r = Point.ofCartesian(116.8620e-3, 47.4505e-3);
        Point kp42_r = Point.ofCartesian(122.2098e-3, 44.2771e-3);
        Point kp43_r = Point.ofCartesian(131.2367e-3, 56.6929e-3);
        Point kp44_r = Point.ofCartesian(125.0956e-3, 60.4060e-3);

        Point kp51_r = Point.ofCartesian(105.2790e-3, 51.3956e-3);
        Point kp52_r = Point.ofCartesian(108.2603e-3, 50.5262e-3);
        Point kp53_r = Point.ofCartesian(113.2445e-3, 65.0449e-3);
        Point kp54_r = Point.ofCartesian(109.8070e-3, 66.0630e-3);


        Arc ln11_r = Arc.ofEndPointsCenter(kp12_r, kp11_r, kp0);
        Line ln12_r = Line.ofEndPoints(kp12_r, kp13_r);
        Arc ln13_r = Arc.ofEndPointsCenter(kp13_r, kp14_r, kp0);
        Line ln14_r = Line.ofEndPoints(kp11_r, kp14_r);
//
        Arc ln21_r = Arc.ofEndPointsCenter(kp22_r, kp21_r, kp0);
        Line ln22_r = Line.ofEndPoints(kp22_r, kp23_r);
        Arc ln23_r = Arc.ofEndPointsCenter(kp23_r, kp24_r, kp0);
        Line ln24_r = Line.ofEndPoints(kp21_r, kp24_r);

        Arc ln31_r = Arc.ofEndPointsCenter(kp32_r, kp31_r, kp0);
        Line ln32_r = Line.ofEndPoints(kp32_r, kp33_r);
        Arc ln33_r = Arc.ofEndPointsCenter(kp33_r, kp34_r, kp0);
        Line ln34_r = Line.ofEndPoints(kp31_r, kp34_r);

        Arc ln41_r = Arc.ofEndPointsCenter(kp42_r, kp41_r, kp0);
        Line ln42_r = Line.ofEndPoints(kp42_r, kp43_r);
        Arc ln43_r = Arc.ofEndPointsCenter(kp43_r, kp44_r, kp0);
        Line ln44_r = Line.ofEndPoints(kp41_r, kp44_r);

        Arc ln51_r = Arc.ofEndPointsCenter(kp52_r, kp51_r, kp0);
        Line ln52_r = Line.ofEndPoints(kp52_r, kp53_r);
        Arc ln53_r = Arc.ofEndPointsCenter(kp53_r, kp54_r, kp0);
        Line ln54_r = Line.ofEndPoints(kp51_r, kp54_r);


        Area ha11_r = Area.ofHyperLines(new HyperLine[]{ln11_r, ln12_r, ln13_r, ln14_r});
        Area ha12_r = Area.ofHyperLines(new HyperLine[]{ln21_r, ln22_r, ln23_r, ln24_r});
        Area ha13_r = Area.ofHyperLines(new HyperLine[]{ln31_r, ln32_r, ln33_r, ln34_r});
        Area ha14_r = Area.ofHyperLines(new HyperLine[]{ln41_r, ln42_r, ln43_r, ln44_r});
        Area ha15_r = Area.ofHyperLines(new HyperLine[]{ln51_r, ln52_r, ln53_r, ln54_r});
//
        // left winding cross section, pole 1:
        Point kp11_l = Point.ofCartesian(46.5110e-3, 23.1411e-3);
        Point kp12_l = Point.ofCartesian(41.5017e-3, 0.4220e-3);
        Point kp13_l = Point.ofCartesian(26.1512e-3, 0.4220e-3);
        Point kp14_l = Point.ofCartesian(31.5804e-3, 26.7069e-3);

        Point kp21_l = Point.ofCartesian(52.1191e-3, 31.6582e-3);
        Point kp22_l = Point.ofCartesian(47.0281e-3, 23.8477e-3);
        Point kp23_l = Point.ofCartesian(34.2914e-3, 32.4158e-3);
        Point kp24_l = Point.ofCartesian(40.2406e-3, 41.3813e-3);

        Point kp31_l = Point.ofCartesian(58.1997e-3, 38.4004e-3);
        Point kp32_l = Point.ofCartesian(53.8169e-3, 33.9970e-3);
        Point kp33_l = Point.ofCartesian(43.0825e-3, 44.9701e-3);
        Point kp34_l = Point.ofCartesian(48.1718e-3, 50.0227e-3);

        Point kp41_l = Point.ofCartesian(68.1252e-3, 45.8816e-3);
        Point kp42_l = Point.ofCartesian(62.9947e-3, 42.3665e-3);
        Point kp43_l = Point.ofCartesian(53.1566e-3, 54.1499e-3);
        Point kp44_l = Point.ofCartesian(59.9425e-3, 58.2566e-3);

        Point kp51_l = Point.ofCartesian(80.2727e-3, 50.6807e-3);
        Point kp52_l = Point.ofCartesian(77.2983e-3, 49.7931e-3);
        Point kp53_l = Point.ofCartesian(72.7010e-3, 64.4390e-3);
        Point kp54_l = Point.ofCartesian(76.1353e-3, 65.4631e-3);


        Arc ln11_l = Arc.ofEndPointsCenter(kp12_l, kp11_l, kp0);
        Line ln12_l = Line.ofEndPoints(kp12_l, kp13_l);
        Arc ln13_l = Arc.ofEndPointsCenter(kp13_l, kp14_l, kp0);
        Line ln14_l = Line.ofEndPoints(kp11_l, kp14_l);
//
        Arc ln21_l = Arc.ofEndPointsCenter(kp22_l, kp21_l, kp0);
        Line ln22_l = Line.ofEndPoints(kp22_l, kp23_l);
        Arc ln23_l = Arc.ofEndPointsCenter(kp23_l, kp24_l, kp0);
        Line ln24_l = Line.ofEndPoints(kp21_l, kp24_l);

        Arc ln31_l = Arc.ofEndPointsCenter(kp32_l, kp31_l, kp0);
        Line ln32_l = Line.ofEndPoints(kp32_l, kp33_l);
        Arc ln33_l = Arc.ofEndPointsCenter(kp33_l, kp34_l, kp0);
        Line ln34_l = Line.ofEndPoints(kp31_l, kp34_l);

        Arc ln41_l = Arc.ofEndPointsCenter(kp42_l, kp41_l, kp0);
        Line ln42_l = Line.ofEndPoints(kp42_l, kp43_l);
        Arc ln43_l = Arc.ofEndPointsCenter(kp43_l, kp44_l, kp0);
        Line ln44_l = Line.ofEndPoints(kp41_l, kp44_l);

        Arc ln51_l = Arc.ofEndPointsCenter(kp52_l, kp51_l, kp0);
        Line ln52_l = Line.ofEndPoints(kp52_l, kp53_l);
        Arc ln53_l = Arc.ofEndPointsCenter(kp53_l, kp54_l, kp0);
        Line ln54_l = Line.ofEndPoints(kp51_l, kp54_l);


        Area ha11_l = Area.ofHyperLines(new HyperLine[]{ln11_l, ln12_l, ln13_l, ln14_l});
        Area ha12_l = Area.ofHyperLines(new HyperLine[]{ln21_l, ln22_l, ln23_l, ln24_l});
        Area ha13_l = Area.ofHyperLines(new HyperLine[]{ln31_l, ln32_l, ln33_l, ln34_l});
        Area ha14_l = Area.ofHyperLines(new HyperLine[]{ln41_l, ln42_l, ln43_l, ln44_l});
        Area ha15_l = Area.ofHyperLines(new HyperLine[]{ln51_l, ln52_l, ln53_l, ln54_l});

        Cable cable_D2 = new Cable_D2();

        // Windigs, pole 1
        Winding w11_pole1 = Winding.ofAreas(new Area[]{ha11_l, ha11_r}, new int[]{-1, +1}, 15, 15, cable_D2);
        Winding w12_pole1 = Winding.ofAreas(new Area[]{ha12_l, ha12_r}, new int[]{-1, +1}, 6, 6, cable_D2);
        Winding w13_pole1 = Winding.ofAreas(new Area[]{ha13_l, ha13_r}, new int[]{-1, +1}, 4, 4, cable_D2);
        Winding w14_pole1 = Winding.ofAreas(new Area[]{ha14_l, ha14_r}, new int[]{-1, +1}, 4, 4, cable_D2);
        Winding w15_pole1 = Winding.ofAreas(new Area[]{ha15_l, ha15_r}, new int[]{-1, +1}, 2, 2, cable_D2);

        // Windigs, pole 2
        Winding w11_pole2 = Winding.ofAreas(new Area[]{ha11_r, ha11_l}, new int[]{+1, -1}, 15, 15, cable_D2);
        Winding w12_pole2 = Winding.ofAreas(new Area[]{ha12_r, ha12_l}, new int[]{+1, -1}, 6, 6, cable_D2);
        Winding w13_pole2 = Winding.ofAreas(new Area[]{ha13_r, ha13_l}, new int[]{+1, -1}, 4, 4, cable_D2);
        Winding w14_pole2 = Winding.ofAreas(new Area[]{ha14_r, ha14_l}, new int[]{+1, -1}, 4, 4, cable_D2);
        Winding w15_pole2 = Winding.ofAreas(new Area[]{ha15_r, ha15_l}, new int[]{+1, -1}, 2, 2, cable_D2);


        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w15_pole1, w14_pole1, w13_pole1, w12_pole1, w11_pole1}).reverseWindingDirection();

        Pole p2 = Pole.ofWindings(new Winding[]{w15_pole2, w14_pole2, w13_pole2, w12_pole2, w11_pole2}).reverseWindingDirection().mirrorX();

        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1, p2});
        return c1;
    }

    public Element[] iron_collar() {

        Point kpcoll_930 = Point.ofCartesian(dbeam,rcoll);
        Point kpcoll_931 = Point.ofCartesian(dbeam,ycoll);
        Point kpcoll_932 = Point.ofCartesian(dbeam+xcoll-rcurvedge,ycoll);
        Point kpcoll_933 = Point.ofCartesian(dbeam+77.4418e-3,81.8324e-3);
        Point kpcoll_934 = Point.ofCartesian(dbeam+73.9183e-3,68.7448e-3);
        Point kpcoll_935 = Point.ofCartesian(dbeam+80.1968e-3,60.5e-3);
        Point kpcoll_936 = Point.ofCartesian(dbeam+91.16699e-3,60.5e-3);
        Point kpcoll_937 = Point.ofCartesian(dbeam+xcoll,ycoll-rcurvedge);
        Point kpcoll_938 = Point.ofCartesian(dbeam+xcoll,0e-3);
        Point kpcoll_939 = Point.ofCartesian(dbeam+rcoll,0e-3);
        Point kpcoll_942 = Point.ofCartesian(dbeam-(xcoll-rcurvedge),ycoll);
        Point kpcoll_943 = Point.ofCartesian(dbeam-77.4418e-3,81.8324e-3);
        Point kpcoll_944 = Point.ofCartesian(dbeam-73.9183e-3,68.7448e-3);
        Point kpcoll_945 = Point.ofCartesian(dbeam-80.1968e-3,60.5e-3);
        Point kpcoll_946 = Point.ofCartesian(dbeam-91.16699e-3,60.5e-3);
        Point kpcoll_947 = Point.ofCartesian(0e-3,ycoll-rcurvedge);
        Point kpcoll_948 = Point.ofCartesian(0e-3,0e-3);
        Point kpcoll_949 = Point.ofCartesian(dbeam-rcoll,0e-3);


        Line lncoll_932 = Line.ofEndPoints(kpcoll_931,kpcoll_932);
        Line lncoll_934 = Line.ofEndPoints(kpcoll_933,kpcoll_934);
        Line lncoll_936 = Line.ofEndPoints(kpcoll_935,kpcoll_936);
        Line lncoll_938 = Line.ofEndPoints(kpcoll_937,kpcoll_938);
        Line lncoll_939 = Line.ofEndPoints(kpcoll_938,kpcoll_939);
        Line lncoll_941 = Line.ofEndPoints(kpcoll_931,kpcoll_942);
        Line lncoll_943 = Line.ofEndPoints(kpcoll_943,kpcoll_944);
        Line lncoll_945 = Line.ofEndPoints(kpcoll_945,kpcoll_946);
        Line lncoll_947 = Line.ofEndPoints(kpcoll_947,kpcoll_948);
        Line lncoll_948 = Line.ofEndPoints(kpcoll_948,kpcoll_949);

        Arc lncoll_933 = Arc.ofEndPointsRadius(kpcoll_932,kpcoll_933,rcurvedge);
        Arc lncoll_935 = Arc.ofEndPointsRadius(kpcoll_935,kpcoll_934,rcurvel);
        Arc lncoll_937 = Arc.ofEndPointsRadius(kpcoll_936,kpcoll_937,rcurvedge);
        Arc lncoll_940 = Arc.ofEndPointsRadius(kpcoll_930,kpcoll_939,rcoll);
        Arc lncoll_942 = Arc.ofEndPointsRadius(kpcoll_943,kpcoll_942,rcurvedge);
        Arc lncoll_944 = Arc.ofEndPointsRadius(kpcoll_944,kpcoll_945,rcurvel);
        Arc lncoll_946 = Arc.ofEndPointsRadius(kpcoll_947,kpcoll_946,rcurvedge);
        Arc lncoll_949 = Arc.ofEndPointsRadius(kpcoll_949,kpcoll_930,rcoll);

        // AREAS FIRST QUADRANT
        Area areaColl_1   = Area.ofHyperLines(new HyperLine[]{lncoll_940,lncoll_939,lncoll_938,lncoll_937,lncoll_936,lncoll_935,lncoll_934,lncoll_933,lncoll_932,lncoll_941,lncoll_942,lncoll_943,lncoll_944,lncoll_945,lncoll_946,lncoll_947,lncoll_948,lncoll_949});

        // AREAS OTHER QUADRANTS
        Area areaColl_2 = areaColl_1.mirrorY();
        Area areaColl_3 = areaColl_2.mirrorX();
        Area areaColl_4 = areaColl_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element elColl_1 = new Element("IC_El1", areaColl_1);

        // ELEMENTS OTHER QUADRANTS
        Element elColl_2 = new Element("IC_El2", areaColl_2);
        Element elColl_3 = new Element("IC_El3", areaColl_3);
        Element elColl_4 = new Element("IC_El4", areaColl_4);

        return new Element[]{elColl_1, elColl_4};
    }

    public Element[] iron_nose() {

        Point kpnose_950 = Point.ofCartesian(77.2e-3,66.8e-3);
        Point kpnose_951 = Point.ofCartesian(81.8e-3,50.6e-3);
        Point kpnose_952 = Point.ofCartesian(104e-3,51.2e-3);
        Point kpnose_953 = Point.ofCartesian(109e-3,67.3e-3);

        Line lnnose_950 = Line.ofEndPoints(kpnose_950,kpnose_951);
        Line lnnose_951 = Line.ofEndPoints(kpnose_951,kpnose_952);
        Line lnnose_952 = Line.ofEndPoints(kpnose_952,kpnose_953);
        Arc lnnose_953 = Arc.ofEndPointsRadius(kpnose_950,kpnose_953,rcoll);

        // AREAS FIRST QUADRANT
        Area areaNose_1   = Area.ofHyperLines(new HyperLine[]{lnnose_950,lnnose_951,lnnose_952,lnnose_953});
        // AREAS OTHER QUADRANTS
        Area areaNose_2 = areaNose_1.mirrorY();
        Area areaNose_3 = areaNose_2.mirrorX();
        Area areaNose_4 = areaNose_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element elNose_1 = new Element("IN_El1", areaNose_1);

        // ELEMENTS OTHER QUADRANTS
        Element elNose_2 = new Element("IN_El2", areaNose_2);
        Element elNose_3 = new Element("IN_El3", areaNose_3);
        Element elNose_4 = new Element("IN_El4", areaNose_4);

        return new Element[]{elNose_1, elNose_4};
    }

    public Element[] iron_outer() {

        Point kpouter_1 = Point.ofCartesian(0,ycollint);
        Point kpouter_2 = Point.ofCartesian(0,255.0e-3);
        Point kpouter_3 = Point.ofCartesian(15.0e-3,255.0e-3);
        Point kpouter_4 = Point.ofCartesian(15.0e-3,245.0e-3);
        Point kpouter_5 = Point.ofCartesian(26.0e-3,245.0e-3);
        Point kpouter_6 = Point.ofCartesian(26.0e-3,266.0e-3);
        Point kpouter_61 = Point.ofCartesian(36.0e-3,266.0e-3);
        Point kpouter_7 = Point.ofCartesian(36.0e-3,273.1027e-3);
        Point kpouter_81 = Point.ofCartesian(94.6e-3,261.6185e-3);
        Point kpouter_8 = Point.ofCartesian(94.6e-3,246.0e-3);
        Point kpouter_9 = Point.ofCartesian(137.2219e-3,246.0e-3);
        Point kpouter_10 = Point.ofCartesian(146.5138e-3,241.6620e-3);
        Point kpouter_11 = Point.ofCartesian(128.7e-3,217.5e-3);
        Point kpouter_12 = Point.ofCartesian(187.8636e-3,217.5e-3);
        Point kpouter_13 = Point.ofCartesian(286.5583e-3,98.66993e-3);
        Point kpouter_14 = Point.ofCartesian(290.2742e-3,99.94942e-3);
        Point kpouter_15 = Point.ofCartesian(306.8371e-3,10e-3);
        Point kpouter_16 = Point.ofCartesian(300e-3,10e-3);
        Point kpouter_17 = Point.ofCartesian(300.0e-3,0);
        Point kpouter_18 = Point.ofCartesian(xcollint,0);
        Point kpouter_180 = Point.ofCartesian(xcollint-xtooth,0);
        Point kpouter_181 = Point.ofCartesian(xcollint-xtooth,ytooth-stooth);
        Point kpouter_182 = Point.ofCartesian(xcollint-xtooth+stooth,ytooth);
        Point kpouter_183 = Point.ofCartesian(xcollint,ytooth);
        Point kpouter_19 = Point.ofCartesian(xcollint,ycollint-Rcurv);
        Point kpouter_20 = Point.ofCartesian(xcollint-Rcurv,ycollint);
        Point kpouter_51 = Point.ofCartesian(0,200e-3-rhole1);
        Point kpouter_50  = Point.ofCartesian(0,200e-3+rhole1);
        Point kpouter_52  = Point.ofCartesian(rhole1+23e-3,200e-3);

        Line lnouter_1   = Line.ofEndPoints(kpouter_1,kpouter_51);
        EllipticArc lnouter_111 = EllipticArc.ofEndPointsAxes(kpouter_52,kpouter_51,rhole1+23e-3,rhole1);
        EllipticArc lnouter_112 = EllipticArc.ofEndPointsAxes(kpouter_50,kpouter_52,rhole1+23e-3,rhole1);
        Line lnouter_113 = Line.ofEndPoints(kpouter_2,kpouter_50);
        Line lnouter_2   = Line.ofEndPoints(kpouter_2,kpouter_3);
        Line lnouter_3   = Line.ofEndPoints(kpouter_3,kpouter_4);
        Line lnouter_4   = Line.ofEndPoints(kpouter_4,kpouter_5);
        Line lnouter_5   = Line.ofEndPoints(kpouter_5,kpouter_6);
        Line lnouter_6   = Line.ofEndPoints(kpouter_6,kpouter_61);
        Line lnouter_7   = Line.ofEndPoints(kpouter_61,kpouter_7);
        EllipticArc lnouter_9   = EllipticArc.ofEndPointsAxes(kpouter_7,kpouter_81,xaxisel,yaxisel);
        Line lnouter_10  = Line.ofEndPoints(kpouter_81,kpouter_8);
        Line lnouter_11  = Line.ofEndPoints(kpouter_8,kpouter_9);
        EllipticArc lnouter_12  = EllipticArc.ofEndPointsAxes(kpouter_9,kpouter_10,xaxisel,yaxisel);
        Line lnouter_13  = Line.ofEndPoints(kpouter_10,kpouter_11);
        Line lnouter_14  = Line.ofEndPoints(kpouter_11,kpouter_12);
        EllipticArc lnouter_15  = EllipticArc.ofEndPointsAxes(kpouter_12,kpouter_13,xaxisel,yaxisel);
        Line lnouter_16  = Line.ofEndPoints(kpouter_13,kpouter_14);
        Arc lnouter_17  = Arc.ofEndPointsRadius(kpouter_14,kpouter_15,rcryo);
        Line lnouter_18  = Line.ofEndPoints(kpouter_15,kpouter_16);
        Line lnouter_19  = Line.ofEndPoints(kpouter_16,kpouter_17);
        Line lnouter_20  = Line.ofEndPoints(kpouter_17,kpouter_18);
        Line lnouter_21  = Line.ofEndPoints(kpouter_18,kpouter_180);
        Line lnouter_210 = Line.ofEndPoints(kpouter_180,kpouter_181);
        Line lnouter_211 = Line.ofEndPoints(kpouter_181,kpouter_182);
        Line lnouter_212 = Line.ofEndPoints(kpouter_182,kpouter_183);
        Line lnouter_213 = Line.ofEndPoints(kpouter_183,kpouter_19);
        Line lnouter_23  = Line.ofEndPoints(kpouter_1,kpouter_20);
        Arc lnouter_22  = Arc.ofEndPointsRadius(kpouter_20,kpouter_19,56e-3);

        // AREAS FIRST QUADRANT
        Area areaOuter_1   = Area.ofHyperLines(new HyperLine[]{lnouter_23,lnouter_22,lnouter_213,lnouter_212,lnouter_211,lnouter_210,lnouter_21,lnouter_20,lnouter_19,lnouter_18,lnouter_17,lnouter_16,lnouter_15,lnouter_14,lnouter_13,lnouter_12,lnouter_11,lnouter_10,lnouter_9,lnouter_7,lnouter_6,lnouter_5,lnouter_4,lnouter_3,lnouter_2,lnouter_113,lnouter_112,lnouter_111,lnouter_1});
        // AREAS OTHER QUADRANTS
        Area areaOuter_2 = areaOuter_1.mirrorY();
        Area areaOuter_3 = areaOuter_2.mirrorX();
        Area areaOuter_4 = areaOuter_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element elOuter_1 = new Element("IO_El1", areaOuter_1);

        // ELEMENTS OTHER QUADRANTS
        Element elOuter_2 = new Element("IO_El2", areaOuter_2);
        Element elOuter_3 = new Element("IO_El3", areaOuter_3);
        Element elOuter_4 = new Element("IO_El4", areaOuter_4);

        return new Element[]{elOuter_1, elOuter_4};
    }

    public Element[] holes() {

        // POINTS
        Point kph_600 = Point.ofCartesian(xhole2-rhole2,yhole2);
        Point kph_602 = Point.ofCartesian(xhole2+rhole2,yhole2);
        Point kph_700 = Point.ofCartesian(xhole3-rhole3,yhole3);
        Point kph_702 = Point.ofCartesian(xhole3+rhole3,yhole3);
        Point kph_800 = Point.ofCartesian(xhole4-rhole4,yhole4);
        Point kph_802 = Point.ofCartesian(xhole4+rhole4,yhole4);
        Point kph_900 = Point.ofCartesian(xhole5-rhole5,yhole5);
        Point kph_902 = Point.ofCartesian(xhole5+rhole5,yhole5);

        // LINES
        Circumference lnh1 = Circumference.ofDiameterEndPoints(kph_600, kph_602);
        Circumference lnh2 = Circumference.ofDiameterEndPoints(kph_700, kph_702);
        Circumference lnh3 = Circumference.ofDiameterEndPoints(kph_800, kph_802);
        Circumference lnh4 = Circumference.ofDiameterEndPoints(kph_900, kph_902);

        // AREAS FIRST QUADRANT
        Area arh1_1 = Area.ofHyperLines(new HyperLine[]{lnh1});
        Area arh2_1 = Area.ofHyperLines(new HyperLine[]{lnh2});
        Area arh3_1 = Area.ofHyperLines(new HyperLine[]{lnh3});
        Area arh4_1 = Area.ofHyperLines(new HyperLine[]{lnh4});

        // AREAS FOURTH QUADRANT
        Area arh1_4 = arh1_1.mirrorX();
        Area arh2_4 = arh2_1.mirrorX();
        Area arh3_4 = arh3_1.mirrorX();
        Area arh4_4 = arh4_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element hole1_1 = new Element("IY_HOLE1_Q1", arh1_1);
        Element hole2_1 = new Element("IY_HOLE2_Q1", arh2_1);
        Element hole3_1 = new Element("IY_HOLE3_Q1", arh3_1);
        Element hole4_1 = new Element("IY_HOLE4_Q1", arh4_1);

        // ELEMENTS FOURTH QUADRANT
        Element hole1_4 = new Element("IY_HOLE1_Q4", arh1_4);
        Element hole2_4 = new Element("IY_HOLE2_Q4", arh2_4);
        Element hole3_4 = new Element("IY_HOLE3_Q4", arh3_4);
        Element hole4_4 = new Element("IY_HOLE4_Q4", arh4_4);

        return new Element[]{hole1_1, hole2_1, hole3_1, hole4_1, hole1_4, hole2_4, hole3_4, hole4_4};

    }


}
