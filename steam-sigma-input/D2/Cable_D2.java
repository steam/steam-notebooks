package input.HL_LHC.D2;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

public class Cable_D2 extends Cable {

    public Cable_D2() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_D2";

        //Insulation
        this.wInsulNarrow = 0.125e-3; // [m];
        this.wInsulWide = 0.098e-3; // [m];
        //Filament
        this.dFilament = 6e-6; // [m];
        this.lTp = 15e-3; // [m];
        this.dstrand = 0.825e-3; // [m];
        double CuScRatio = 1.9;
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 80;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 1e-6; // [ohm]; // Only for inter-strand coupling losses
        this.Ra = 1e-7; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        //Cable
        this.wBare = 15.1e-3; // [m]; // Height of bare cable
        this.hInBare = 1.358e-3; // [m]; // Internal width of bare cable
        this.hOutBare = 1.594e-3; // [m]; // External width of bare cable
        this.noOfStrands = 36;
        this.noOfStrandsPerLayer = 18;
        this.noOfLayers = 2;
        this.lTpStrand = 0.100; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
//        this.C1 = 65821.9; // [A]; not used
//        this.C2 = -5042.6; // [A/T]; not used
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_NbTi_D2;
        this.insulationMaterial = MatDatabase.MAT_KAPTON;
        this.materialInnerVoids = MatDatabase.MAT_VOID;
        this.materialOuterVoids = MatDatabase.MAT_VOID;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }

}
