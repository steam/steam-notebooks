package input.FCC.CommonCoil_vh12_2ac6;

import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by mprioli on 04/06/2018.
 */
public class Magnet_FCC_commonCoil_vh12_2ac6 {

    private final Domain[] domains;

    public Magnet_FCC_commonCoil_vh12_2ac6() {

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, iron_yoke()),
//                new IronDomain("ironYoke", "","", iron_yoke())
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r1 = 1.5;
        double angle = Math.PI/2;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r1, 0);
        Point kp2 = Point.ofCartesian(0, r1);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofPointCenterAngle(kp1, kpc, angle);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);

        return new Element[]{el1};
    }

    public Element[] airFarField() {
        double r1 = 1.5;
        double r2 = r1 * 1.05;
        double angle = Math.PI/2;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r1, 0);
        Point kp2 = Point.ofCartesian(0, r1);

        Point kp1_far = Point.ofCartesian(r2, 0);
        Point kp2_far = Point.ofCartesian(0, r2);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofPointCenterAngle(kp1_far, kpc, angle);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofPointCenterAngle(kp2, kpc, -angle);

        // AREAS
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        // ELEMENTS
        Element el1_far = new Element("FAR_El1", ar1_far);

        return new Element[]{el1_far};
    }

    public Coil coil() {

        // Main coils 1-8
        
        Point kp11 = Point.ofCartesian(25.15e-3, 155.95e-3);
        Point kp12 = Point.ofCartesian(kp11.getX(), 106.25e-3);
        Point kp13 = Point.ofCartesian(44.35e-3, kp12.getY());
        Point kp14 = Point.ofCartesian(kp13.getX(), kp11.getY());

        Point kp21 = Point.ofCartesian(25.15e-3, 210.357e-3);
        Point kp22 = Point.ofCartesian(kp21.getX(), 163.157e-3);
        Point kp23 = Point.ofCartesian(44.35e-3, kp22.getY());
        Point kp24 = Point.ofCartesian(kp23.getX(), kp21.getY());

        Point kp31 = Point.ofCartesian(45.15e-3, 157.2964e-3);
        Point kp32 = Point.ofCartesian(kp31.getX(), 110.0964e-3);
        Point kp33 = Point.ofCartesian(57.15e-3, kp32.getY());
        Point kp34 = Point.ofCartesian(kp33.getX(), kp31.getY());

        Point kp41 = Point.ofCartesian(45.15e-3, 207.884e-3);
        Point kp42 = Point.ofCartesian(kp41.getX(), 163.184e-3);
        Point kp43 = Point.ofCartesian(57.15e-3, kp42.getY());
        Point kp44 = Point.ofCartesian(kp43.getX(), kp41.getY());

        Point kp51 = Point.ofCartesian(57.95e-3, 158.1091e-3);
        Point kp52 = Point.ofCartesian(kp51.getX(), 113.4091e-3);
        Point kp53 = Point.ofCartesian(69.95e-3, kp52.getY());
        Point kp54 = Point.ofCartesian(kp53.getX(), kp51.getY());

        Point kp61 = Point.ofCartesian(57.95e-3, 204.322e-3);
        Point kp62 = Point.ofCartesian(kp61.getX(), 162.122e-3);
        Point kp63 = Point.ofCartesian(69.95e-3, kp62.getY());
        Point kp64 = Point.ofCartesian(kp63.getX(), kp61.getY());

        Point kp71 = Point.ofCartesian(70.75e-3, 158.5317e-3);
        Point kp72 = Point.ofCartesian(kp71.getX(), 116.3317e-3);
        Point kp73 = Point.ofCartesian(82.75e-3, kp72.getY());
        Point kp74 = Point.ofCartesian(kp73.getX(), kp71.getY());

        Point kp81 = Point.ofCartesian(70.75e-3, 201.192e-3);
        Point kp82 = Point.ofCartesian(kp81.getX(), 161.492e-3);
        Point kp83 = Point.ofCartesian(82.75e-3, kp82.getY());
        Point kp84 = Point.ofCartesian(kp83.getX(), kp81.getY());

        // Ancillary coils 9-16
        
        Point kp91 = Point.ofCartesian(13.65e-3, 115.65e-3);
        Point kp92 = Point.ofCartesian(18.35e-3, kp91.getY());
        Point kp93 = Point.ofCartesian(kp92.getX(), 134.85e-3);
        Point kp94 = Point.ofCartesian(kp91.getX(), kp93.getY());

        Point kp101 = Point.ofCartesian(13.65e-3, 185.15e-3);
        Point kp102 = Point.ofCartesian(18.35e-3, kp101.getY());
        Point kp103 = Point.ofCartesian(kp102.getX(), 204.35e-3);
        Point kp104 = Point.ofCartesian(kp101.getX(), kp103.getY());

        Point kp111 = Point.ofCartesian(19.15e-3, 119.15e-3);
        Point kp112 = Point.ofCartesian(23.85e-3, kp111.getY());
        Point kp113 = Point.ofCartesian(kp112.getX(), 138.35e-3);
        Point kp114 = Point.ofCartesian(kp111.getX(), kp113.getY());

        Point kp121 = Point.ofCartesian(19.15e-3, 181.65e-3);
        Point kp122 = Point.ofCartesian(23.85e-3, kp121.getY());
        Point kp123 = Point.ofCartesian(kp122.getX(), 200.85e-3);
        Point kp124 = Point.ofCartesian(kp121.getX(), kp123.getY());

        Point kp131 = Point.ofCartesian(13.65e-3, 95.0745e-3);
        Point kp132 = Point.ofCartesian(18.35e-3, kp131.getY());
        Point kp133 = Point.ofCartesian(kp132.getX(), 114.2745e-3);
        Point kp134 = Point.ofCartesian(kp131.getX(), kp133.getY());

        Point kp141 = Point.ofCartesian(13.65e-3, 205.671e-3);
        Point kp142 = Point.ofCartesian(18.35e-3, kp141.getY());
        Point kp143 = Point.ofCartesian(kp142.getX(), 224.871e-3);
        Point kp144 = Point.ofCartesian(kp141.getX(), kp143.getY());

        Point kp151 = Point.ofCartesian(19.15e-3, 99.1445e-3);
        Point kp152 = Point.ofCartesian(23.85e-3, kp151.getY());
        Point kp153 = Point.ofCartesian(kp152.getX(), 118.3445e-3);
        Point kp154 = Point.ofCartesian(kp151.getX(), kp153.getY());

        Point kp161 = Point.ofCartesian(19.15e-3, 201.67e-3);
        Point kp162 = Point.ofCartesian(23.85e-3, kp161.getY());
        Point kp163 = Point.ofCartesian(kp162.getX(), 220.87e-3);
        Point kp164 = Point.ofCartesian(kp161.getX(), kp163.getY());
        
        
        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Line ln31 = Line.ofEndPoints(kp32, kp31);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Line ln33 = Line.ofEndPoints(kp33, kp34);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Line ln41 = Line.ofEndPoints(kp42, kp41);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Line ln43 = Line.ofEndPoints(kp43, kp44);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Line ln51 = Line.ofEndPoints(kp52, kp51);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Line ln53 = Line.ofEndPoints(kp53, kp54);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Line ln61 = Line.ofEndPoints(kp62, kp61);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Line ln63 = Line.ofEndPoints(kp63, kp64);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Line ln71 = Line.ofEndPoints(kp72, kp71);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Line ln73 = Line.ofEndPoints(kp73, kp74);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Line ln81 = Line.ofEndPoints(kp82, kp81);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Line ln83 = Line.ofEndPoints(kp83, kp84);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Line ln91 = Line.ofEndPoints(kp92, kp91);
        Line ln92 = Line.ofEndPoints(kp92, kp93);
        Line ln93 = Line.ofEndPoints(kp93, kp94);
        Line ln94 = Line.ofEndPoints(kp91, kp94);

        Line ln101 = Line.ofEndPoints(kp102, kp101);
        Line ln102 = Line.ofEndPoints(kp102, kp103);
        Line ln103 = Line.ofEndPoints(kp103, kp104);
        Line ln104 = Line.ofEndPoints(kp101, kp104);

        Line ln111 = Line.ofEndPoints(kp112, kp111);
        Line ln112 = Line.ofEndPoints(kp112, kp113);
        Line ln113 = Line.ofEndPoints(kp113, kp114);
        Line ln114 = Line.ofEndPoints(kp111, kp114);

        Line ln121 = Line.ofEndPoints(kp122, kp121);
        Line ln122 = Line.ofEndPoints(kp122, kp123);
        Line ln123 = Line.ofEndPoints(kp123, kp124);
        Line ln124 = Line.ofEndPoints(kp121, kp124);

        Line ln131 = Line.ofEndPoints(kp132, kp131);
        Line ln132 = Line.ofEndPoints(kp132, kp133);
        Line ln133 = Line.ofEndPoints(kp133, kp134);
        Line ln134 = Line.ofEndPoints(kp131, kp134);

        Line ln141 = Line.ofEndPoints(kp142, kp141);
        Line ln142 = Line.ofEndPoints(kp142, kp143);
        Line ln143 = Line.ofEndPoints(kp143, kp144);
        Line ln144 = Line.ofEndPoints(kp141, kp144);

        Line ln151 = Line.ofEndPoints(kp152, kp151);
        Line ln152 = Line.ofEndPoints(kp152, kp153);
        Line ln153 = Line.ofEndPoints(kp153, kp154);
        Line ln154 = Line.ofEndPoints(kp151, kp154);

        Line ln161 = Line.ofEndPoints(kp162, kp161);
        Line ln162 = Line.ofEndPoints(kp162, kp163);
        Line ln163 = Line.ofEndPoints(kp163, kp164);
        Line ln164 = Line.ofEndPoints(kp161, kp164);
        

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});
        Area ha19p = Area.ofHyperLines(new HyperLine[]{ln91, ln92, ln93, ln94});
        Area ha110p = Area.ofHyperLines(new HyperLine[]{ln101, ln102, ln103, ln104});
        Area ha111p = Area.ofHyperLines(new HyperLine[]{ln111, ln112, ln113, ln114});
        Area ha112p = Area.ofHyperLines(new HyperLine[]{ln121, ln122, ln123, ln124});
        Area ha113p = Area.ofHyperLines(new HyperLine[]{ln131, ln132, ln133, ln134});
        Area ha114p = Area.ofHyperLines(new HyperLine[]{ln141, ln142, ln143, ln144});
        Area ha115p = Area.ofHyperLines(new HyperLine[]{ln151, ln152, ln153, ln154});
        Area ha116p = Area.ofHyperLines(new HyperLine[]{ln161, ln162, ln163, ln164});


        Cable cableHF = new Cable_EUHF1();
        Cable cableLF = new Cable_EULF1();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p}, new int[]{+1}, 20, 20, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p}, new int[]{+1}, 19, 19, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p}, new int[]{+1}, 19, 19, cableLF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p}, new int[]{+1}, 18, 18, cableLF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p}, new int[]{+1}, 18, 18, cableLF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p}, new int[]{+1}, 17, 17, cableLF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p}, new int[]{+1}, 17, 17, cableLF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p}, new int[]{+1}, 16, 16, cableLF);

        Winding w19 = Winding.ofAreas(new Area[]{ha19p}, new int[]{+1}, 2, 2, cableHF);
        Winding w110 = Winding.ofAreas(new Area[]{ha110p}, new int[]{+1}, 2, 2, cableHF);
        Winding w111 = Winding.ofAreas(new Area[]{ha111p}, new int[]{+1}, 2, 2, cableHF);
        Winding w112 = Winding.ofAreas(new Area[]{ha112p}, new int[]{+1}, 2, 2, cableHF);
        Winding w113 = Winding.ofAreas(new Area[]{ha113p}, new int[]{+1}, 2, 2, cableHF);
        Winding w114 = Winding.ofAreas(new Area[]{ha114p}, new int[]{+1}, 2, 2, cableHF);
        Winding w115 = Winding.ofAreas(new Area[]{ha115p}, new int[]{+1}, 2, 2, cableHF);
        Winding w116 = Winding.ofAreas(new Area[]{ha116p}, new int[]{+1}, 2, 2, cableHF);

        w13 = w13.reverseWindingDirection();
        w14 = w14.reverseWindingDirection();
        w17 = w17.reverseWindingDirection();
        w18 = w18.reverseWindingDirection();
        w19 = w19.reverseWindingDirection();
        w111 = w111.reverseWindingDirection();
        w114 = w114.reverseWindingDirection();
        w116 = w116.reverseWindingDirection();

        Winding[] windings_p1 = {w18, w17, w15, w16, w14, w13, w11, w12, w115, w113, w19, w111, w112, w110, w114, w116};

        Pole p1 = Pole.ofWindings(windings_p1);

        return Coil.ofPoles(new Pole[]{p1});
    }

    public Element[] iron_yoke() {
        
        double mm=1e-3;
        double d2r=Math.PI/180;

        double rout=375*mm;
        double rhole=15*mm;

        double XCSUP=20;
        double xcsup=XCSUP*mm;

        double YCSUP=250;
        double ycsup=YCSUP*mm;

        double DYCSUP=20;
        double dycsup=DYCSUP*mm;

        double XCMAX=150;
        double xcmax=XCMAX*mm;

        double S1X=51.6242;
        double s1x=S1X*mm;

        double S1Y=0.4666;
        double s1y=S1Y*mm;

        double S1TX=68.9352;
        double s1tx=S1TX*mm;

        double S1TY=78.6876;
        double s1ty=S1TY*mm;

        double S2X=0;
        double s2x=S2X*mm;

        double S2Y=29.4948;
        double s2y=S2Y*mm;

        double S2TX=24.4614;
        double s2tx=S2TX*mm;

        double S2TY=24.7406;
        double s2ty=S2TY*mm;


        Point kp1  = Point.ofCartesian(xcmax,0);
        Point kp2 = Point.ofCartesian(rout,0);
        Point kp3 = Point.ofPolar(rout, 86*d2r);
        Point kp3a = Point.ofCartesian(26*mm,rout-22*mm);
        Point kp3b = Point.ofCartesian(0,rout-22*mm);
        Point kp4 = Point.ofCartesian(0,ycsup+dycsup);
        Point kp4a = Point.ofCartesian(xcsup,ycsup+dycsup);
        Point kp4b = Point.ofCartesian(xcsup,ycsup);
        Point kp5 = Point.ofCartesian(xcmax,ycsup);

        Point kps1 = Point.ofCartesian(s1x,s1y);
        Point kps2 = Point.ofCartesian(s1x+s1tx,s1y);
        Point kps3 = Point.ofCartesian(s1x+s1tx,s1y+s1ty);
        Point kps4 = Point.ofCartesian(s1x,s1y+s1ty);

        Point kps21 = Point.ofCartesian(s2x,s2y);
        Point kps22 = Point.ofCartesian(s2x+s2tx,s2y);
        Point kps23 = Point.ofCartesian(s2x+s2tx,s2y+s2ty);
        Point kps24 = Point.ofCartesian(s2x,s2y+s2ty);
        
        
        Line ln1 = Line.ofEndPoints(kp1,kp2);
        Arc ln2 = Arc.ofEndPointsRadius(kp3,kp2,rout);
        Line ln3 = Line.ofEndPoints(kp3,kp3a);
        Line ln3a = Line.ofEndPoints(kp3a,kp3b);
        Line ln3b = Line.ofEndPoints(kp3b,kp4);
        Line ln4 = Line.ofEndPoints(kp4,kp4a);
        Line ln4a = Line.ofEndPoints(kp4a,kp4b);
        Line ln4b = Line.ofEndPoints(kp4b,kp5);
        Line ln5 = Line.ofEndPoints(kp5,kp1);

        Line lns1 = Line.ofEndPoints(kps1,kps2);
        Line lns2 = Line.ofEndPoints(kps2,kps3);
        Line lns3 = Line.ofEndPoints(kps3,kps4);
        Line lns4 = Line.ofEndPoints(kps4,kps1);

        Line lns21 = Line.ofEndPoints(kps21,kps22);
        Line lns22 = Line.ofEndPoints(kps22,kps23);
        Line lns23 = Line.ofEndPoints(kps23,kps24);
        Line lns24 = Line.ofEndPoints(kps24,kps21);

        HyperArea ar1= Area.ofHyperLines(new HyperLine[]{ln1,ln2,ln3,ln3a,ln3b,ln4,ln4a,ln4b,ln5}); //,BHiron2);
        HyperArea ars1= Area.ofHyperLines(new HyperLine[]{lns1,lns2,lns3,lns4}); //BHiron2);
        HyperArea ars2= Area.ofHyperLines(new HyperLine[]{lns21,lns22,lns23,lns24}); //BHiron2);

//        Lmesh(ln1,5);
//        Lmesh(lns2,3);
//        Lmesh(lns21,3);

        Element el1 = new Element("IY_El1", ar1);
        Element els1 = new Element("IY_Els1", ars1);
        Element els2 = new Element("IY_Els2", ars2);

        return new Element[]{el1, els1, els2};
    }

}

