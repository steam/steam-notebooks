package input.FCC.CommonCoil_vh12_2ac6;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by mprioli on 04/06/2018.
 */
public class Cable_EULF1 extends Cable {

    public Cable_EULF1() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        // ROXIE cable EUHF1
        this.label = "cable_EULF1";

        // Cable geometry
        this.wBare = 12e-3; // [m];
        this.hInBare = 2.2e-3; // [m];
        this.hOutBare = 2.2e-3; // [m];
        this.noOfStrands = 18;
        this.noOfStrandsPerLayer = 9;
        this.noOfLayers = 2;
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];

        //Strand
        this.dstrand = 1.2e-3; // [m];
        double CuScRatio = 2.5;
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        this.thetaTpStrand = 15*Math.PI/180;
        this.lTpStrand = 2*(wBare-dstrand)/Math.tan(thetaTpStrand);

        //Filament
        this.dFilament = 30e-6; // [m];

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];

        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];

        // Others
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }


}