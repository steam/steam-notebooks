package input.FCC.CosTheta_v22b_37_optd7f8;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableLF_FCC_cosTheta_v22b_37_optd7f8 extends Cable {

    public CableLF_FCC_cosTheta_v22b_37_optd7f8() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_LF_FCC";

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];
        //Filament
        this.dFilament = 50e-6; // [m];
        //Strand
        this.dstrand = 0.7e-3; // [m];
        double CuScRatio = 2.2;
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];
        //Cable
        this.wBare = 13.65e-3; // [m];
        this.hInBare = 1.204e-3; // [m];
        this.hOutBare = 1.3231e-3; // [m];
        this.noOfStrands = 37;
        this.noOfStrandsPerLayer = 19;
        this.noOfLayers = 2;
        this.lTpStrand = 0.100; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 65821.9; // [A]; not used
        this.C2 = -5042.6; // [A/T]; not used
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }


}
