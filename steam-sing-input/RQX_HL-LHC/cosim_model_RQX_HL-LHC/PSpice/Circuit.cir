

* PSPICE Netlist Simulation File
* Generated on 2022/02/28 12:36:23 at CERN
* Authors: STEAM
.LIB "C:\GitLabRepository\steam-pspice-library\diodes\Items\RQX_HL-LHC_Diodes.lib"
.LIB "C:\GitLabRepository\steam-pspice-library\power_supply\Items\RQX_HL-LHC_PCs.lib"
.LIB "C:\GitLabRepository\steam-pspice-library\CLIQ\CLIQ_unit.lib"
.LIB "C:\GitLabRepository\steam-pspice-library\RQX_HL-LHC\magnets\magnets_cosimulation_MQXF.lib"
**** Global parameters ****
.PARAM
+ R_trimA={0.00E+00} 
+ R_trimB={0.00E+00} 
+ R_trimC={0.00E+00} 
+ R_trimD={0.00E+00} 
+ R_trimE={0.00E+00} 
+ length_MQXFA={4.2} 
+ length_MQXFB={7.15} 
+ L_pole_perMeter={0.001497249} 
+ k12_adjacent_poles={0.230094704} 
+ k13_opposite_poles={-0.092494511} 
+ C_ground_perMeter={3.71E-08} 

**** Main circuit ****
* Main power converter and its crowbar
I_PC_RQX (401 402) STIMULUS = I_pc
xD_PC_RQX (401 401a) TripletDiode_5V
R_PC_RQX (401a 402) {560e-6}
C_PC_RQX (401 402) {3.0mF}
*
* Connection to ground
R_ground (401 0) {100}
*
* Diode string across the main power supply
x_diodePC (402 401) TripletDiode_12V
*
* Warm resistances at the two sides of the main power supply
R_PCwarmleft (401 101) {150e-6/2}
R_PCwarmright (105 402) {150e-6/2}
*
* Q1 trim power converter and its crowbar
I_PC_RTQX1 (201 202) STIMULUS = I_pc_trimQ1
xD_direct_PC_RTQX1 (201 201a) TripletDiode_3600mV
xD_reverse_PC_RTQX1 (201a 201) TripletDiode_3600mV
R_PC_RTQX1 (201a 202) {160e-6}
C_PC_RTQX1 (201 202) {4.16mF}
*
* Q3 trim power converter and its crowbar
I_PC_RTQX3 (204 205) STIMULUS = I_pc_trimQ3
xD_direct_PC_RTQX3 (204 204a) TripletDiode_3600mV
xD_reverse_PC_RTQX3 (204a 204) TripletDiode_3600mV
R_PC_RTQX3 (204a 205) {160e-6}
C_PC_RTQX3 (204 205) {4.16mF}
*
* Warm leads from trim converters to DFHX boxes
R_trimA (201 101) {1.35e-3/2}
R_trimB (202 102) {1.35e-3/2}
R_trimD (204 104) {1.20e-3/2}
R_trimE (205 105) {1.20e-3/2}
*
* Spare lead (floating)
R_trimC (203 103) {2.4e-3}
R_trimC_Floating (0 203) {1e6}
*
* Superconducting links
R_clE (105 MAGQ3b_5) {1e-6}
R_clD (104 MAGQ2b_5) {1e-6}
R_clC (103 MAGQ2a_1) {1e-6}
R_clB (102 MAGQ1b_5) {1e-6}
R_clA (101 MAGQ1a_6) {1e-6}
*
* Connections between magnets
V_Q3b_Q3a (MAGQ3b_2 MAGQ3a_1) {0V}
V_Q3a_Q2b (MAGQ3a_6 MAGQ2b_5) {0V}
V_Q2b_Q2a (MAGQ2b_2 MAGQ2a_1) {0V}
V_Q2a_Q1b (MAGQ2a_6 MAGQ1b_5) {0V}
V_Q1b_Q1a (MAGQ1b_2 MAGQ1a_1) {0V}
*
* Q1a trim power converter and its crowbar
xPC_minitrim (107 108) power_converter_minitrim
+ PARAMS: R_crow={1e-4} Cfilter={6.6uF} 
*
* Q1a trim power converter current leads
R_mt_left (107 106) {227e-3/2}
R_mt_right (109 108) {227e-3/2}
* Q1a trim superconducting links
R_clF (106 MAGQ1a_6) {1e-6}
R_clG (109 MAGQ1a_1) {1e-6}
*
* Cold Diodes
x_diodeQ3 (105 104a) TripletDiode_12V
R_diodeQ3 (104a 104) {1e-6}
x_diodeQ2b (104 103a) TripletDiode_5V
R_diodeQ2b (103a 103) {1e-6}
x_diodeQ2a (103 102a) TripletDiode_5V
R_diodeQ2a (102a 102) {1e-6}
x_diodeQ1 (102 101a) TripletDiode_12V
R_diodeQ1 (101a 101) {1e-6}
*
* Warm Diodes across Q3a
R_warmDiodeQ1a_1 (113 112) {227e-3/2}
x_warmDiodeQ1a_dir (112 111) TripletDiode_12V
x_warmDiodeQ1a_rev (111 112) TripletDiode_12V
R_warmDiodeQ1a_2 (111 110) {227e-3/2}
*
* Q3a superconducting links
R_clH (113 MAGQ3a_1) {1e-6}
R_clI (110 MAGQ2b_5) {1e-6}
* Magnets
x_mag_1 (MAGQ1a_1 MAGQ1a_2 MAGQ1a_3 MAGQ1a_4 MAGQ1a_5 MAGQ1a_6 MAGQ1a_7 MAGQ1a_8) MAGNET_EQ_4_M1
+ PARAMS: L_1={L_pole_perMeter*length_MQXFA} L_2={L_pole_perMeter*length_MQXFA} L_3={L_pole_perMeter*length_MQXFA} 
+ L_4={L_pole_perMeter*length_MQXFA} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
x_mag_2 (MAGQ1b_1 MAGQ1b_2 MAGQ1b_3 MAGQ1b_4 MAGQ1b_5 MAGQ1b_6 MAGQ1b_7 MAGQ1b_8) MAGNET_EQ_4_M2
+ PARAMS: L_1={L_pole_perMeter*length_MQXFA} L_2={L_pole_perMeter*length_MQXFA} L_3={L_pole_perMeter*length_MQXFA} 
+ L_4={L_pole_perMeter*length_MQXFA} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
x_mag_3 (MAGQ2a_1 MAGQ2a_2 MAGQ2a_3 MAGQ2a_4 MAGQ2a_5 MAGQ2a_6 MAGQ2a_7 MAGQ2a_8) MAGNET_EQ_4_M3
+ PARAMS: L_1={L_pole_perMeter*length_MQXFB} L_2={L_pole_perMeter*length_MQXFB} L_3={L_pole_perMeter*length_MQXFB} 
+ L_4={L_pole_perMeter*length_MQXFB} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
x_mag_4 (MAGQ2b_1 MAGQ2b_2 MAGQ2b_3 MAGQ2b_4 MAGQ2b_5 MAGQ2b_6 MAGQ2b_7 MAGQ2b_8) MAGNET_EQ_4_M4
+ PARAMS: L_1={L_pole_perMeter*length_MQXFB} L_2={L_pole_perMeter*length_MQXFB} L_3={L_pole_perMeter*length_MQXFB} 
+ L_4={L_pole_perMeter*length_MQXFB} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
x_mag_5 (MAGQ3a_1 MAGQ3a_2 MAGQ3a_3 MAGQ3a_4 MAGQ3a_5 MAGQ3a_6 MAGQ3a_7 MAGQ3a_8) MAGNET_EQ_4_M5
+ PARAMS: L_1={L_pole_perMeter*length_MQXFA} L_2={L_pole_perMeter*length_MQXFA} L_3={L_pole_perMeter*length_MQXFA} 
+ L_4={L_pole_perMeter*length_MQXFA} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
x_mag_6 (MAGQ3b_1 MAGQ3b_2 MAGQ3b_3 MAGQ3b_4 MAGQ3b_5 MAGQ3b_6 MAGQ3b_7 MAGQ3b_8) MAGNET_EQ_4_M6
+ PARAMS: L_1={L_pole_perMeter*length_MQXFA} L_2={L_pole_perMeter*length_MQXFA} L_3={L_pole_perMeter*length_MQXFA} 
+ L_4={L_pole_perMeter*length_MQXFA} k_1_2={{k12_adjacent_poles}} k_1_3={{k13_opposite_poles}} 
+ k_1_4={{k12_adjacent_poles}} k_2_3={{k12_adjacent_poles}} k_2_4={{k13_opposite_poles}} k_3_4={{k12_adjacent_poles}} k_I={1} 
+ R_par={1e3} 
*
* Internal connections in the magnets - Q1a
V_Q1a_int_1 (MAGQ1a_2 MAGQ1a_7) {0V}
V_Q1a_int_2 (MAGQ1a_8 MAGQ1a_3) {0V}
V_Q1a_int_3 (MAGQ1a_4 MAGQ1a_5) {0V}
* Internal connections in the magnets - Q1b
V_Q1b_int_1 (MAGQ1b_6 MAGQ1b_3) {0V}
V_Q1b_int_2 (MAGQ1b_4 MAGQ1b_7) {0V}
V_Q1b_int_3 (MAGQ1b_8 MAGQ1b_1) {0V}
* Internal connections in the magnets - Q2a
V_Q2a_int_1 (MAGQ2a_2 MAGQ2a_7) {0V}
V_Q2a_int_2 (MAGQ2a_8 MAGQ2a_3) {0V}
V_Q2a_int_3 (MAGQ2a_4 MAGQ2a_5) {0V}
* Internal connections in the magnets - Q2b
V_Q2b_int_1 (MAGQ2b_6 MAGQ2b_3) {0V}
V_Q2b_int_2 (MAGQ2b_4 MAGQ2b_7) {0V}
V_Q2b_int_3 (MAGQ2b_8 MAGQ2b_1) {0V}
* Internal connections in the magnets - Q3a
V_Q3a_int_1 (MAGQ3a_2 MAGQ3a_7) {0V}
V_Q3a_int_2 (MAGQ3a_8 MAGQ3a_3) {0V}
V_Q3a_int_3 (MAGQ3a_4 MAGQ3a_5) {0V}
* Internal connections in the magnets - Q3b
V_Q3b_int_1 (MAGQ3b_6 MAGQ3b_3) {0V}
V_Q3b_int_2 (MAGQ3b_4 MAGQ3b_7) {0V}
V_Q3b_int_3 (MAGQ3b_8 MAGQ3b_1) {0V}
*
* CLIQ unit (units). Here the first port is negative
* Six different library components are used for the six CLIQ units,
* to allow setting six different triggering times
xcliqQ1a (MAGQ1a_4 MAGQ1a_2) cliq_unit_1_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={38m} L_cliq={0.42m} 
*
xcliqQ1b (MAGQ1b_1 MAGQ1b_6) cliq_unit_2_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={32m} L_cliq={0.41m} 
*
xcliqQ2a (MAGQ2a_4 MAGQ2a_2) cliq_unit_3_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={29m} L_cliq={0.39m} 
*
xcliqQ2b (MAGQ2b_1 MAGQ2b_6) cliq_unit_4_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={28m} L_cliq={0.38m} 
*
xcliqQ3a (MAGQ3a_4 MAGQ3a_2) cliq_unit_5_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={27m} L_cliq={0.36m} 
*
xcliqQ3b (MAGQ3b_1 MAGQ3b_6) cliq_unit_6_withInductance
+ PARAMS: C_cliq={0.04} R_cliq={26m} L_cliq={0.35m} 
*
* Simulation options (uncomment before running the simulation) 
* .OPTION
* + RELTOL=0.0001
* + VNTOL=0.00001
* + ABSTOL=0.0001
* + CHGTOL=0.000000000000001
* + GMIN=0.000000000001
* + ITL1=150
* + ITL2=20
* + ITL4=10
* + TNOM=27
* + NUMDGT=8
* .AUTOCONVERGE
* + RELTOL=0.05
* + VNTOL=0.0001
* + ABSTOL=0.0001
* + ITL1=1000
* + ITL2=1000
* + ITL4=1000
* + PIVTOL=0.0000000001
*
* Configuration file
.INC Conf.cir
.END

