HEL_Cosim_1Mag_1Sec.cir 				1 magnet with 1 section in circuit.
HEL_Cosim_1Mag_2Sec_ser.cir			 	1 magnet with 2 sections connected in series.
HEL_Cosim_1Mag_2Sec_ind.cir			 	1 magnet with 2 sections connected in series. In this case A1 is the first magnet and B1 is the second magnet.
HEL_Cosim_2Mag_3Sec_ser_ind.cir			2 magnets with 3 sections. First magnet with 2 sections connected in series. The second magnet with 1 section. Magnets independently powered and inducitvely coupled.
HEL_Cosim_2Mag_4Sec_ser_ind.cir			2 magnets with 4 sections. Eeach magnet with 2 sections connected in series. Magnets independently powered and inducitvely coupled.

2021.06.02 - Updated by M.Wozniak