import numpy as np
import sys

###### START OF FUNCTION DEFINITIONS
def getValueType(value):
    """
    
        **Define the type of input variable**
        
        Function returns an an integer defining the type of input variable:
        - float: 0
        - int: 0
        - list: 1
        - numpy.ndarray: 2

        :param value: a variable
        :type value: float, int, list, or numpy.ndarray
        :return: int

        - Example :

        import numpy as np
        
        print( utils.getValueType(2) ) # expected: 0
        # >>> 					0
        
        print( utils.getValueType(2.31) ) # expected: 0
        # >>> 					0
        
        print( utils.getValueType([2, 3, 5]) ) # expected: 1
        # >>> 					1
        
        print( utils.getValueType(np.array([2, 3, 5])) ) # expected: 2
        # >>> 					2
        
    """
    
    if isinstance(value, (float, int)):
        return 0
    if isinstance(value, list):
        return 1
    if isinstance(value, np.ndarray):
        return 2
    raise Exception('Input parameter type {} not supported!'.format(type(value)))

def printVariableDescNameValue(variableGroup, variableLabels):
    """
    
       **Print variable description, variable name, and variable value**
       
       Function prints variable description, variable name, and variable value
       
       :param variableGroup: list of tuples; each tuple has two elements: the first element is a string defining the variable name, and the second element is either an integer, a float, a list, or a numpy.ndarray defining the variable value
        :type variableGroup: list
        :param variableLabels: dictionary assigning a description to each variable name
        :type variableLabels: dict
        :return: 
       
       - Example :
       
       import numpy as np

        variableGroup = []
        variableGroup.append( ('x1', 12) )
        variableGroup.append( ('x2', 23.42) )
        variableGroup.append( ('x3', [2, 4, 6]) )
        variableGroup.append( ('x3', np.array([2, 4, 6])) )

        variableLabels = {'x1': '1st variable', 'x2': '2nd variable', 'x3': '3rd variable'}

        utils.printVariableDescNameValue(variableGroup, variableLabels)
        # >>> 					1st variable x1 12
        # >>> 					2nd variable x2 23.42
        # >>> 					3rd variable x3 [2, 4, 6]
        # >>> 					3rd variable x3 [2 4 6]
       
    """
    
    for vg in variableGroup:
        if vg != '':
            varName = vg[0]
            varValue = vg[1]
            varDesc = variableLabels.get(str(varName))
            print(varDesc + " " + varName + " " + str(varValue))
            
def writeLEDETInputsNew(book, sheet, variableGroup, variableLabels):
    """
    
        **Write one sheet of a LEDET input file**
        
        Function writes one sheet of a LEDET input file

        :param book: workbook object to write
        :type book: xlsxwriter.Workbook
        :param sheet: name of the sheet to write (first sheet = 0)
        :type sheet: string
        :param variableGroup: list of tuples; each tuple has two elements: the first element is a string defining the variable name, and the second element is either an integer, a float, a list, or a numpy.ndarray defining the variable value
        :type variableGroup: list
        :param variableLabels: dictionary assigning a description to each variable name
        :type variableLabels: dict
        :return: 

    """
    
    worksheet = book.add_worksheet(sheet) 

    cell_format = book.add_format({'bold': False, 'font_name': 'Calibri', 'font_size': 11})

    # Write to the sheet of the workbook
    currentRow=0
    for i in range(len(variableGroup)):
        vg = variableGroup[i]
        if vg != '':
            varName = vg[0]
            varValue = vg[1]
            varDesc = variableLabels.get(str(varName))

            worksheet.write(currentRow, 0, varDesc, cell_format)
            worksheet.write(currentRow, 1, varName, cell_format)

            print('i=' + str(i) + ', currentRow=' + str(currentRow) + ' - ' + str(varName))
            
            # get variable type
            # 0 - scalar, 1 - vector, 2 - matrix
            varType = getValueType(varValue)
            if varType == 0:
                worksheet.write(currentRow, 2, varValue, cell_format) 
                currentRow = currentRow + 1
            if varType == 1:
                worksheet.write_row(currentRow, 2, varValue, cell_format)
                currentRow = currentRow + 1
            if varType == 2:
                for row, data in enumerate(varValue):
                    worksheet.write_row(currentRow, 2, data, cell_format)
                    currentRow = currentRow + 1
        if vg == '':
            print('i=' + str(i) + ', currentRow=' + str(currentRow) + ' - ' + 'BLANK LINE')
            currentRow = currentRow + 1
            
    worksheet.set_column(0, 0, 80)
    worksheet.set_column(1, 1, 40)
    worksheet.set_column(2, 1000, 20)
    
def progressBar(value, endvalue, bar_length=20):
    """
    
        **Show a simple progress bar during calculation**
        
        Function updates the last printed line and visualizes a progress bar

        :param value: Value indicating the current progress
        :type value: float or int
        :param value: Value indicating the end of the process
        :type value: float or int
        :param bar_length: Length of characters used in the display window
        :type value: int
        :return: 

        - Example :

        utils.progressBar(value=15, endvalue=50, bar_length=20) # 15/50=30%
        # >>> 					Status: [----->              ] 30%
    
    """
    
    percent = float(value) / endvalue
    arrow = '-' * int(round(percent * bar_length)-1) + '>'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\rStatus: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100))))
    sys.stdout.flush()
    
# Function to calculate self-mutual inductance
def calculateInductanceOneForLoop(x, y, polarities, Ns, Ds, strandToHalfTurn, strandToCoilSection, flag_strandCorrection=0, flag_sumTurnToTurn=1, flag_writeOutput=0):
    # Check inputs
    if np.size(x)!=np.size(y):
        raise Exception('Variables x and y must have the same length.')
    if np.size(x)!=np.size(polarities):
        raise Exception('Variables x and polarities must have the same length.')
    if np.size(x)!=np.size(Ds):
        raise Exception('Variables x and Ds must have the same length.')
    if np.size(x)!=np.size(strandToHalfTurn):
        raise Exception('Variables x and strandToHalfTurn must have the same length.')
    if np.size(x)!=np.sum(Ns):
        raise Exception('The sum of the elements of vector Ns must be equal to the number of elements in vector x.')
    if np.size(Ns)<np.max(strandToHalfTurn):
        raise Exception('The variable strandToHalfTurn cannot contain elements higher than the number of half-turns, i.e. the length of Ns.')
    if np.size(Ns)>np.size(np.unique(strandToHalfTurn)):
        print('Warning - ' + 'The variable strandToHalfTurn must assign strands to all half-turns.')
    # If needed, assign default values
#     if isfield(functionOptions,'strandToCoilSection')
#         strandToCoilSection=functionOptions.strandToCoilSection;
#         flag_inductanceCoilSections=1;
#         if size(x)==size(strandToCoilSection')
#             strandToCoilSection=strandToCoilSection';
#         end
#         % Check input    
#         if size(x)~=size(strandToCoilSection)
#             error('Variables x and strandToCoilSection must have the same length.')
#         end
#     else
#         strandToCoilSection=NaN;
#         flag_inductanceCoilSections=0;
#     end

    # Define constants
    veryLow = 1e-12;
    mu0 = 4*np.pi/10**7;

    # Calculate number of strands and half-turns
    nStrands=np.size(x);
    nHalfTurns=np.size(Ns);
    nTurns=int(np.size(Ns)/2);
    nCoilSections=np.max(strandToCoilSection);

    # Only consider the sign of the vector with current polarities
    polarities = np.sign(polarities);

    # Calculate self-mutual inductance between strands    
    oneTwentiethStrands = np.round(nStrands/20) # This is used only to show progress during the calculation
    print('Self-mutual inductance between strands - Calculation started')
    if flag_strandCorrection==0:
        print('Calculation does not include correction for strand diameter.')
        M_strands = np.zeros((nStrands,nStrands))
        # Calculate mutual inductances between strands
        for p1 in range(nStrands):
            if p1 % oneTwentiethStrands == 0:
#                 print('Progress: ' + str(p1) + '/' + str(nStrands))
                progressBar(p1, nStrands, bar_length=20)
            M_strands[p1,:]=-np.sign(polarities[p1])/Ns[strandToHalfTurn[p1]-1]*np.sign(polarities[:])/Ns[strandToHalfTurn[:]-1]*mu0/2/np.pi*np.log((((x[p1]-x[:])**2+(y[p1]-y[:])**2)**.5+veryLow)/Ds[:])
        # Correct the elements on the diagonal
        idxDiagStrands = np.diag_indices(nStrands)
        M_strands[idxDiagStrands]=mu0/np.pi/8/Ns[strandToHalfTurn[:]-1]
    else:
        raise Exception('Calculation including correction for strand diameter is currently not supported.')
#         print('Calculation includes correction for strand diameter.')
#         # Initialization
#         M_strands = np.zeros((nStrands,nStrands))
#         # Calculate mutual inductances between strands
#         for p1 in range(nStrands):
#             if p1 % oneTwentiethStrands == 0:
#                 print('Progress: ' + str(p1) + '/' + str(nStrands))
#             M_strands[p1,:]=-np.sign(polarities[p1])/Ns[strandToHalfTurn[p1]-1]*np.sign(polarities[:])/Ns[strandToHalfTurn[:]-1]*mu0/2/np.pi*np.log((((x[p1]-x[:])**2+(y[p1]-y[:])**2)**.5-Ds[:]+veryLow)/Ds[:])
#         # Correct the elements on the diagonal
#         idxDiagStrands = np.diag_indices(nStrands)
#         M_strands[idxDiagStrands]=mu0/np.pi/8/Ns[strandToHalfTurn[:]-1]
    progressBar(1,1, bar_length=20)
    print('')
    print('Self-mutual inductance between strands - Calculation finished')

    # Calculate total magnet self-inductance
    L_mag0 = np.sum(M_strands)

    # Calculate self-mutual inductance between half-turns
    M_halfTurns = np.empty((nHalfTurns,nHalfTurns))
    oneTwentiethHalfTurns = np.round(nHalfTurns/20) # This is used only to show progress during the calculation
    print('Self-mutual inductance between half-turns - Calculation started')
    for ht1 in range(nHalfTurns):
        idxHT1 = np.where(strandToHalfTurn==ht1+1)
        if ht1 % oneTwentiethHalfTurns == 0:
            progressBar(ht1, nHalfTurns, bar_length=20)
    #          print('Progress: ' + str(ht1) + '/' + str(nHalfTurns))
        for ht2 in range(nHalfTurns):
            idxHT2 = np.where(strandToHalfTurn==ht2+1)
            M_halfTurns[ht1,ht2] = np.sum(M_strands[idxHT1[0][:,None],idxHT2[0]])
    progressBar(1,1, bar_length=20)
    print('')
    print('Self-mutual inductance between half-turns - Calculation finished')
    
    # Calculate self-mutual inductance between turns
    # THIS ASSUMES THAT THE SECOND HALF OF THE HALF-TURNS ARE THE RETURN LINES OF THE FIRST HALF
    if flag_sumTurnToTurn==1:
        halfTurnToTurn=np.tile(np.arange(1,nTurns+1),2);
        M_turns = np.empty((nTurns,nTurns))
#         print('Self-mutual inductance between turns - Calculation started')
        for t1 in range(nTurns):
            idxT1 = np.where(halfTurnToTurn==t1+1)
            for t2 in range(nTurns):
                idxT2 = np.where(halfTurnToTurn==t2+1)
                M_turns[t1,t2] = np.sum(M_halfTurns[idxT1[0][:,None],idxT2[0]])
        print('Self-mutual inductance between turns - Calculation finished')
    else:
        raise Exception('The only supported option is to flag_sumTurnToTurn to 1: Consider that the second half of the half-turns are the return lines of the first half.')
        
    # Calculate self-mutual inductance between defined coil sections
    M_coilSections = np.empty((nCoilSections,nCoilSections))
#     print('Self-mutual inductance between coil sections - Calculation started')
    for cs1 in range(nCoilSections):
        idxCs1 = np.where(strandToCoilSection==cs1+1)
        for cs2 in range(nCoilSections):
            idxCs2 = np.where(strandToCoilSection==cs2+1)
            M_coilSections[cs1,cs2] = np.sum(M_strands[idxCs1[0][:,None],idxCs2[0]])
    print('Self-mutual inductance between coil sections - Calculation finished')

    # Write output file (optional)
    if flag_writeOutput==1:
        outputFileName = 'SMIC_outputCalculatedInductances.txt'
        file_out = open(outputFileName, "w") ## 'w' overwrites file
        file_out.write("%s\n" %('Self- and mutual inductances per unit length between each turn [H/m]'))
        for t1 in range(nTurns):
            for t2 in range(nTurns):
                file_out.write("%12.9e, " %(M_turns[t1,t2]))
            file_out.write("%s\n" %(''))

        file_out.write("%s\n" %(''))
        file_out.write("%s\n" %('Self- and mutual inductances per unit length between each coil section [H/m]'))
        for cs1 in range(nCoilSections):
            for cs2 in range(nCoilSections):
                file_out.write("%12.9e, " %(M_coilSections[cs1,cs2]))
            file_out.write("%s\n" %(''))

        file_out.write("%s\n" %(''))
        file_out.write("%s\n" %('Total self-inductance per unit length [H/m]'))
        file_out.write("%12.9e" %(L_mag0))
        file_out.close()
        print('Output file written: ' + outputFileName + '.')

    return M_halfTurns, M_turns, M_coilSections, L_mag0
    
###### END OF FUNCTION DEFINITIONS