YOFFC = 89.54;		yoffc = YOFFC*mm;
DIAIN = 601.194;	diain = DIAIN*mm;
THICK = 6.35;		thick = THICK*mm;

kpc1 = [0,-diain/2 - yoffc];
kpc2 = [0,-diain/2 - yoffc - thick];
kpc3 = [diain/2+thick,-yoffc];
kpc4 = [diain/2,-yoffc];
kpc5 = [0,diain/2 - yoffc + thick];
kpc6 = [0,diain/2 - yoffc];

lnc1 = HyperLine(kpc1,kpc2,"Line");
lnc2 = HyperLine(kpc3,kpc2,"Arc",diain/2+thick);
lnc3 = HyperLine(kpc3,kpc4,"Line");
lnc4 = HyperLine(kpc4,kpc1,"Arc",diain/2);

arc1 = HyperArea(lnc1,lnc2,lnc3,lnc4,BHiron2);

lnc5 = HyperLine(kpc5,kpc3,"Arc",diain/2+thick);
lnc6 = HyperLine(kpc5,kpc6,"Line");
lnc7 = HyperLine(kpc6,kpc4,"Arc",diain/2);

arc2 = HyperArea(lnc3,lnc5,lnc6,lnc7,BHiron2);
