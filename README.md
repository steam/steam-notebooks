## **STEAM Notebooks** 
#### Notebooks to generate models of superconducting circuits
(Copyright © 2020, CERN, Switzerland. All rights reserved.)

## Abstract
The notebooks can be used to automatically generate electrical models of circuits, electro-thermal models of superconducting magnets, and cooperative-simulation models featuring field/circuit coupling.

## Prerequisites
- PSPICE© [commercial]
- STEAM-LEDET
- STEAM-COSIM
- To use STEAM-SIGMA: Refer to STEAM installation guide
- To use STEAM-SING: Java Runtime Environment (Version 8 update 201, on 16/01/2019)
- To use STEAM-LEDET: Matlab Compiler Runtime (MCR), version "MCR_R2018a_win64_installer"

## Documentation
Not available yet.

## Links
STEAM website: https://espace.cern.ch/steam/

## Contact
steam-team@cern.ch

## STEAM User Agreement
By using any software of the STEAM framework, users agree with this document:
https://edms.cern.ch/document/2024516
